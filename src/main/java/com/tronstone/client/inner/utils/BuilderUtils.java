package com.tronstone.client.inner.utils;

import lombok.Lombok;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@UtilityClass
public class BuilderUtils {
    @SneakyThrows
    public static <T> T fillWithBuilderFields(T fillableObject, Object builderObject) {
        Map<String, Field> builderFields = FieldUtils.getAllFieldsList(builderObject.getClass()).stream()
                .peek(field -> {
                    if (field.isSynthetic()) {
                        try {
                            fillWithBuilderFields(fillableObject, FieldUtils.readField(field, builderObject, true));
                        } catch (IllegalAccessException ex) {
                            throw Lombok.sneakyThrow(ex);
                        }
                    }
                }).collect(
                        HashMap::new,
                        (builderFieldsMap, field) -> builderFieldsMap.put(field.getName(), field),
                        HashMap::putAll
                );

        Field builderField;
        Class<?> fillableObjectClass = fillableObject.getClass();

        for (Field fillableObjectField : FieldUtils.getAllFieldsList(fillableObjectClass)) {
            builderField = builderFields.get(fillableObjectField.getName());

            if (Objects.nonNull(builderField)) {
                FieldUtils.writeField(
                        fillableObjectField,
                        fillableObject,
                        FieldUtils.readField(builderField, builderObject, true),
                        true
                );
            }
        }

        return fillableObject;
    }
}
