package com.tronstone.client.inner.utils;

import lombok.experimental.UtilityClass;

import java.util.Collections;
import java.util.List;

@UtilityClass
public class CollectionsUtils {
    public static boolean isEmptyList(List<?> list) {
        return Collections.EMPTY_LIST.getClass().isAssignableFrom(list.getClass());
    }

    public static boolean isNotEmptyList(List<?> list) {
        return !isEmptyList(list);
    }
}
