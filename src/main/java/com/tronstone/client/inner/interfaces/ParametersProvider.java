package com.tronstone.client.inner.interfaces;

public interface ParametersProvider {
    /**
     * Provide prepared request filter or parameters set
     * built URI into prepared request. For inner using.
     *
     * @param preparedURI already created URI from requester
     * @return String – constructed request URI, will be added to the end of prepared request
     */
    String provide(String preparedURI);
}
