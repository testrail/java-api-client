package com.tronstone.client.inner.annotations;

import com.tronstone.client.inner.service.HttpMethod;
import org.apache.commons.lang3.StringUtils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RequesterMapping {
    HttpMethod method();
    String path() default StringUtils.EMPTY;
}
