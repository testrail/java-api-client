package com.tronstone.client.inner.authentication;

import org.apache.http.client.fluent.Request;

public interface Authentication {
    void prepareAuthentication(Request request);
}
