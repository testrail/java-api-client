package com.tronstone.client.inner.authentication;

import lombok.AllArgsConstructor;
import org.apache.http.client.fluent.Request;

import java.util.Base64;

@AllArgsConstructor(staticName = "credentials")
public class BasicAuthentication implements Authentication {
    private String username;
    private String password;

    @Override
    public void prepareAuthentication(Request request) {
        request.setHeader(
                "Authorization",
                "Basic " + new String(Base64.getEncoder().encode((username + ":" + password).getBytes()))
        );
    }
}
