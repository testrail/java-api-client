package com.tronstone.client.inner.service;

import com.tronstone.client.inner.annotations.AutomaticAuthentication;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.annotations.ServiceURI;
import com.tronstone.client.inner.service.RequesterHandler.RequesterMappedData;
import lombok.SneakyThrows;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.http.client.fluent.Request;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;

public class ServiceRequester {
    private final BasicService basicService;
    private MethodInterceptor requestHandler;

    ServiceRequester(BasicService basicService) {
        this.basicService = basicService;
    }

    ServiceRequester(BasicService basicService, MethodInterceptor requestHandler) {
        this(basicService);
        this.requestHandler = requestHandler;
    }

    public Request prepareRequest(String additionalURI) {
        if (!Enhancer.isEnhanced(basicService.getClass())) {
            throw new UnsupportedOperationException("ServiceRequester can't use RequestMapping for objects that not created from ServiceFactory");
        }

        Class<?> basicServiceClass = basicService.getClass();
        RequesterMappedData mappedData = findLastInvokedRequesterMappedMethod();
        ServiceURI serviceURI = basicServiceClass.getAnnotation(ServiceURI.class);
        RequesterMapping requesterMapping = mappedData.getProcessedMethod().getAnnotation(RequesterMapping.class);

        StringBuilder requestURI = new StringBuilder(basicService.getBaseURI().toString());

        if (Objects.nonNull(serviceURI)) {
            requestURI.append(serviceURI.value());
        }

        requestURI.append(additionalURI).append(mappedData.getRequestPath());
        if (Objects.nonNull(mappedData.getParametersProvider())) {
            String builtRequestURI = mappedData.getParametersProvider().provide(requestURI.toString());
            requestURI.setLength(0);
            requestURI.append(builtRequestURI);
        }

        Request request;
        if (Objects.nonNull(requestHandler)) {
            request = requesterMapping.method().createRequest(requestURI.toString(), requestHandler);
        } else {
            request = requesterMapping.method().createRequest(requestURI.toString());
        }

        if (basicServiceClass.isAnnotationPresent(AutomaticAuthentication.class)) {
            if (Objects.nonNull(basicService.getAuthentication())) {
                basicService.getAuthentication().prepareAuthentication(request);
            } else {
                throw new UnsupportedOperationException("Automatic authentication is enabled but authentication object not initialized in basic service");
            }
        }

        return request;
    }

    public Request prepareRequest() {
        return prepareRequest(StringUtils.EMPTY);
    }

    @SneakyThrows
    public String prepareAndExecuteRequest() {
        return prepareRequest().execute().returnContent().asString();
    }

    private RequesterMappedData findLastInvokedRequesterMappedMethod() {
        Class<?> serviceClass = basicService.getClass().getSuperclass();
        String serviceClassName = serviceClass.getName();
        List<Method> requestMappingMethods = MethodUtils.getMethodsListWithAnnotation(serviceClass, RequesterMapping.class);

        Method referenceMethod;
        String traceMethodName;

        for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
            if (serviceClassName.equals(element.getClassName())) {
                traceMethodName = element.getMethodName();
                referenceMethod = RequesterHandler.getRequesterMappedData(traceMethodName).getProcessedMethod();

                if (Objects.nonNull(referenceMethod)) {
                    for (Method requestMappingMethod : requestMappingMethods) {
                        if (requestMappingMethod.equals(referenceMethod)) {
                            RequesterMappedData mappedData = RequesterHandler.getRequesterMappedData(traceMethodName);
                            RequesterHandler.removeRequesterMappedData(traceMethodName);
                            return mappedData;
                        }
                    }
                }
            }
        }

        throw new UnsupportedOperationException("Can't prepare request for methods that not marked by RequesterMapping annotation");
    }
}
