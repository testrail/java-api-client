package com.tronstone.client.inner.service;

import com.tronstone.client.inner.annotations.MapName;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.interfaces.ParametersProvider;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RequesterHandler implements MethodInterceptor {
    private static final ThreadLocal<Map<String, RequesterMappedData>> REQUESTER_MAPPED_DATA_STORAGE = new ThreadLocal<>();

    public static RequesterMappedData getRequesterMappedData(String methodName) {
        return REQUESTER_MAPPED_DATA_STORAGE.get().get(methodName);
    }

    public static RequesterMappedData removeRequesterMappedData(String methodName) {
        return REQUESTER_MAPPED_DATA_STORAGE.get().remove(methodName);
    }

    @Override
    public Object intercept(Object serviceObject, Method serviceMethod, Object[] methodArguments, MethodProxy proxy) throws Throwable {
        RequesterMapping requesterMapping = serviceMethod.getAnnotation(RequesterMapping.class);

        if (Objects.nonNull(requesterMapping)) {
            RequesterMappedData requesterMappedData = new RequesterMappedData();

            String methodParameterName;
            String requestPath = requesterMapping.path();
            Parameter[] serviceMethodParameters = serviceMethod.getParameters();
            Map<String, String> requestPathParameters = getRequestPathDynamicParameters(requesterMapping.path());

            for (int i = 0; i < methodArguments.length; i++) {
                if (serviceMethodParameters[i].isAnnotationPresent(MapName.class)) {
                    methodParameterName = "{" + serviceMethodParameters[i].getAnnotation(MapName.class).value() + "}";
                } else {
                    methodParameterName = "{" + serviceMethodParameters[i].getName() + "}";
                }

                if (requestPathParameters.containsKey(methodParameterName)) {
                    requestPath = StringUtils.replace(requestPath, methodParameterName, methodArguments[i].toString());
                }

                if (ParametersProvider.class.isAssignableFrom(serviceMethodParameters[i].getType())) {
                    requesterMappedData.setParametersProvider((ParametersProvider) methodArguments[i]);
                }
            }

            requesterMappedData.setRequestPath(requestPath);
            requesterMappedData.setProcessedMethod(serviceMethod);

            if (Objects.isNull(REQUESTER_MAPPED_DATA_STORAGE.get())) {
                REQUESTER_MAPPED_DATA_STORAGE.set(new HashMap<>());
            }

            REQUESTER_MAPPED_DATA_STORAGE.get().put(serviceMethod.getName(), requesterMappedData);
        }

        return proxy.invokeSuper(serviceObject, methodArguments);
    }

    private Map<String, String> getRequestPathDynamicParameters(String requestPath) {
        Map<String, String> pathParameters = new HashMap<>();
        Matcher parameterMatcher = Pattern.compile("\\{([\\w.-]+)\\}").matcher(requestPath);

        while (parameterMatcher.find()) {
            pathParameters.put(parameterMatcher.group(), null);
        }

        return pathParameters;
    }

    @Setter(AccessLevel.PRIVATE) @Getter
    public static class RequesterMappedData {
        private Method processedMethod;
        private String requestPath;
        private ParametersProvider parametersProvider;
    }
}
