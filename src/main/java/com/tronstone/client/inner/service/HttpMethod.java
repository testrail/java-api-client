package com.tronstone.client.inner.service;

import lombok.Lombok;
import lombok.SneakyThrows;
import net.sf.cglib.core.ReflectUtils;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.reflect.FastClass;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

import java.net.URI;

public enum HttpMethod {
    GET(HttpGet.METHOD_NAME, "org.apache.http.client.fluent.InternalHttpRequest"),
    POST(HttpPost.METHOD_NAME, "org.apache.http.client.fluent.InternalEntityEnclosingHttpRequest");

    private static final Class<?> IHR;

    static {
        try {
            IHR = Class.forName("org.apache.http.client.fluent.InternalHttpRequest");
        } catch (ClassNotFoundException ex) {
            throw Lombok.sneakyThrow(ex);
        }
    }

    private final String methodName;
    private final Class<?> requestTypeClass;

    @SneakyThrows
    HttpMethod(String methodName, String requestTypeClassName) {
        this.methodName = methodName;
        requestTypeClass = Class.forName(requestTypeClassName);
    }

    @SneakyThrows
    public Request createRequest(String uri) {
        return (Request) FastClass.create(Request.class)
                .getMethod(MethodUtils.getAccessibleMethod(Request.class, StringUtils.capitalize(name().toLowerCase()), String.class))
                .invoke(null, new Object[] {uri});
    }

    public Request createRequest(String uri, MethodInterceptor requestHandler) {
        Enhancer enhancer = new Enhancer();

        enhancer.setSuperclass(Request.class);
        enhancer.setCallback(requestHandler);

        return (Request) enhancer.create(new Class[] {IHR}, new Object[] {
                ReflectUtils.newInstance(
                        requestTypeClass,
                        new Class[] {String.class, URI.class},
                        new Object[] {methodName, URI.create(uri)}
                )
        });
    }
}
