package com.tronstone.client.inner.service;

import com.tronstone.client.inner.authentication.Authentication;
import lombok.Getter;
import net.sf.cglib.proxy.MethodInterceptor;

import java.net.URI;

@Getter
public abstract class BasicService {
    private final URI baseURI;
    private Authentication authentication;
    private final ServiceRequester requester;

    protected BasicService(URI baseURI) {
        this.baseURI = baseURI;
        requester = new ServiceRequester(this);
    }

    protected BasicService(URI baseURI, MethodInterceptor requestHandler) {
        this.baseURI = baseURI;
        requester = new ServiceRequester(this, requestHandler);
    }

    protected BasicService(URI baseURI, Authentication authentication) {
        this(baseURI);
        this.authentication = authentication;
    }

    protected BasicService(URI baseURI, Authentication authentication, MethodInterceptor requestHandler) {
        this(baseURI, requestHandler);
        this.authentication = authentication;
    }
}
