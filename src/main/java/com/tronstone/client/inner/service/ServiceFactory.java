package com.tronstone.client.inner.service;

import lombok.experimental.UtilityClass;
import net.sf.cglib.proxy.Enhancer;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ClassUtils;

import java.util.Arrays;

@UtilityClass
public class ServiceFactory {
    @SuppressWarnings("unchecked")
    public static <T> T createService(Class<? extends BasicService> serviceClass, Object... constructorArguments) {
        Enhancer enhancer = new Enhancer();

        enhancer.setSuperclass(serviceClass);
        enhancer.setCallback(new RequesterHandler());

        if (ArrayUtils.isEmpty(constructorArguments)) {
            return (T) enhancer.create();
        }

        return (T) enhancer.create(Arrays.stream(constructorArguments).map(argument -> {
            Class<?> argumentClass = argument.getClass();

            if (Enhancer.isEnhanced(argumentClass)) {
                return argumentClass.getSuperclass();
            } else if (ClassUtils.isPrimitiveOrWrapper(argumentClass)) {
                return ClassUtils.wrapperToPrimitive(argumentClass);
            }

            return argumentClass;
        }).toArray(Class[]::new), constructorArguments);
    }
}
