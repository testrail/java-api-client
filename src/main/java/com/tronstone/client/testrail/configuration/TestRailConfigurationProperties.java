package com.tronstone.client.testrail.configuration;

import lombok.SneakyThrows;
import org.aeonbits.owner.Config;
import org.aeonbits.owner.Converter;
import org.apache.http.client.utils.URIUtils;

import java.lang.reflect.Method;
import java.net.URI;

import static org.aeonbits.owner.Config.Sources;

@Sources("classpath:testrailclient.properties")
public interface TestRailConfigurationProperties extends Config {
    @Key("testrail.url")
    @ConverterClass(URLConverter.class)
    URI getInstanceURL();

    @Key("testrail.username")
    String getUsername();

    @Key("testrail.password")
    String getPassword();

    @Key("testrail.api-key")
    String getAPIKey();

    class URLConverter implements Converter<URI> {
        @Override
        @SneakyThrows
        public URI convert(Method method, String instanceURL) {
            return URIUtils.normalizeSyntax(URI.create(instanceURL));
        }
    }
}
