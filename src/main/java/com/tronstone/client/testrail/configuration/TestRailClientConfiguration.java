package com.tronstone.client.testrail.configuration;

import com.tronstone.client.inner.authentication.Authentication;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.net.URI;

@Getter
@AllArgsConstructor(staticName = "create")
public class TestRailClientConfiguration {
    private URI instanceURI;
    private Authentication authentication;
}
