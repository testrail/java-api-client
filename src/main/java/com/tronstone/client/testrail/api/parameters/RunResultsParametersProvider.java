package com.tronstone.client.testrail.api.parameters;

import com.google.common.base.Preconditions;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.concurrent.TimeUnit;

@NoArgsConstructor(staticName = "prepare")
public class RunResultsParametersProvider extends PaginationParametersProvider<ResultsParametersProvider> {
    private final StringBuilder parametersBuilder = getParametersBuilder();

    /**
     * Only return test cases created before this date.
     *
     * @param createdBefore time that will be converted to UNIX timestamp
     * @return RunResultsParametersProvider – provider object
     */
    public RunResultsParametersProvider createdBefore(DateTime createdBefore) {
        Preconditions.checkNotNull(createdBefore, "Created before should not be null");
        parametersBuilder.append("&created_before=").append(TimeUnit.MILLISECONDS.toSeconds(createdBefore.getMillis()));
        return this;
    }

    /**
     * Only return test cases created after this date.
     *
     * @param createdAfter time that will be converted to UNIX timestamp
     * @return RunResultsParametersProvider – provider object
     */
    public RunResultsParametersProvider createdAfter(DateTime createdAfter) {
        Preconditions.checkNotNull(createdAfter, "Created after should not be null");
        parametersBuilder.append("&created_after=").append(TimeUnit.MILLISECONDS.toSeconds(createdAfter.getMillis()));
        return this;
    }

    /**
     * A comma-separated array of creators (user ID's) to filter by.
     *
     * @param userIDs array of user ID's
     * @return RunResultsParametersProvider – provider object
     */
    public RunResultsParametersProvider createdBy(int... userIDs) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(userIDs), "User ID's should not be empty or null");
        parametersBuilder.append("&created_by=").append(StringUtils.join(userIDs, ','));
        return this;
    }

    /**
     * A single Defect ID (e.g. TR-1, 4291, etc).
     *
     * @param defect single defect
     * @return RunResultsParametersProvider – provider object
     */
    public RunResultsParametersProvider defectsFilter(String defect) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(defect), "Defect should not be empty or null");
        parametersBuilder.append("&defects_filter=").append(defect);
        return this;
    }

    /**
     * A comma-separated list of status IDs to filter by.
     *
     * @param statusIDs array of status ID's
     * @return RunResultsParametersProvider – provider object
     */
    public RunResultsParametersProvider statusID(int... statusIDs) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(statusIDs), "Status ID's should not be empty or null");
        parametersBuilder.append("&status_id=").append(StringUtils.join(statusIDs, ','));
        return this;
    }
}
