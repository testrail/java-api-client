package com.tronstone.client.testrail.api.models.beans.users;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.google.common.base.Preconditions;
import com.google.common.primitives.Ints;
import com.tronstone.client.inner.utils.BuilderUtils;
import com.tronstone.client.inner.utils.CollectionsUtils;
import com.tronstone.client.testrail.api.models.parsing.filters.EmptyListIncludeFilter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
@ToString
@EqualsAndHashCode
@JsonInclude(Include.NON_DEFAULT)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class User {
	private String name;
	private String email;
	private int roleID;
	private @Getter(AccessLevel.NONE) Boolean mfaRequired;
	private @Getter(AccessLevel.NONE) Boolean ssoEnabled;

	@JsonProperty("is_admin")
	private @Getter(AccessLevel.NONE) Boolean admin;

	@JsonProperty("email_notifications")
	private @Getter(AccessLevel.NONE) Boolean emailNotificationsEnabled;

	@JsonInclude(value = Include.CUSTOM, valueFilter = EmptyListIncludeFilter.class)
	private final List<Integer> groupIDs = new ArrayList<>();

	@JsonProperty("assigned_projects")
	@JsonInclude(value = Include.CUSTOM, valueFilter = EmptyListIncludeFilter.class)
	private final List<Integer> assignedProjectsIDs = new ArrayList<>();

	@JsonProperty(access = Access.WRITE_ONLY)
	private int id;

	@JsonProperty(value = "role", access = Access.WRITE_ONLY)
	private String roleName;

	@JsonProperty(value = "is_active", access = Access.WRITE_ONLY)
	private boolean active;

	/**
	 * Returns user object builder.
	 *
	 * @param name the name of the user
	 * @param email the email address of the user
	 * @return UserBuilder – user builder object
	 */
	public static UserBuilder construct(String name, String email) {
		Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
		Preconditions.checkArgument(StringUtils.isNotEmpty(email), "Email should not be empty or null");

		return new UserBuilder(name, email);
	}

	public int getID() {
		return id;
	}

	public boolean isMFARequired() {
		return mfaRequired;
	}

	public boolean isSSOEnabled() {
		return ssoEnabled;
	}

	public boolean isAdmin() {
		return admin;
	}

	public boolean isEmailNotificationsEnabled() {
		return emailNotificationsEnabled;
	}

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	public static class UserBuilder {
		private @NonNull String name;
		private @NonNull String email;
		private int roleID;
		private Boolean admin;
		private Boolean emailNotificationsEnabled;
		private Boolean mfaRequired;
		private Boolean ssoEnabled;
		private final List<Integer> groupIDs = new ArrayList<>();
		private final List<Integer> assignedProjectsIDs = new ArrayList<>();

		/**
		 * @param roleID the ID of the global role to assign to the user
		 * @return UserBuilder – user builder object
		 */
		public UserBuilder roleID(int roleID) {
			Preconditions.checkArgument(roleID > 0, "Role ID should be positive number");
			this.roleID = roleID;
			return this;
		}

		/**
		 * @param admin true to make the user a TestRail Administrator, default false
		 * @return UserBuilder – user builder object
		 */
		public UserBuilder admin(boolean admin) {
			this.admin = admin;
			return this;
		}

		/**
		 * @param emailNotificationsEnabled false to disable email notifications for the user, default true
		 * @return UserBuilder – user builder object
		 */
		public UserBuilder emailNotifications(boolean emailNotificationsEnabled) {
			this.emailNotificationsEnabled = emailNotificationsEnabled;
			return this;
		}

		/**
		 * @param mfaRequired true to require Multi-Factor Authentication for the user,
		 *                    default value matches the MFA setting of the instance
		 * @return UserBuilder – user builder object
		 */
		public UserBuilder mfaRequired(boolean mfaRequired) {
			this.mfaRequired = mfaRequired;
			return this;
		}

		/**
		 * @param ssoEnabled true to enable SSO for the user, default value matches the SSO setting of the instance
		 * @return UserBuilder – user builder object
		 */
		public UserBuilder ssoEnabled(boolean ssoEnabled) {
			this.ssoEnabled = ssoEnabled;
			return this;
		}

		/**
		 * @param groupIDs list of group ID's to assign the user to
		 * @return UserBuilder – user builder object
		 */
		public UserBuilder groupIDs(List<Integer> groupIDs) {
			Preconditions.checkArgument(CollectionUtils.isNotEmpty(groupIDs), "Group ID's should not be empty or null");
			this.groupIDs.addAll(groupIDs);
			return this;
		}

		/**
		 * @param groupIDs array of group ID's to assign the user to
		 * @return UserBuilder – user builder object
		 */
		public UserBuilder groupIDs(int... groupIDs) {
			Preconditions.checkArgument(ArrayUtils.isNotEmpty(groupIDs), "Group ID's should be not empty or null");
			this.groupIDs.addAll(Ints.asList(groupIDs));
			return this;
		}

		/**
		 * @param assignedProjectsIDs list of project ID's to assign to a Project Level Administrator,
		 *                            see Project Level Administration for more information
		 * @return UserBuilder – user builder object
		 */
		public UserBuilder assignedProjectsIDs(List<Integer> assignedProjectsIDs) {
			Preconditions.checkArgument(CollectionUtils.isNotEmpty(groupIDs), "Assigned projects ID's should not be empty or null");
			this.assignedProjectsIDs.addAll(assignedProjectsIDs);
			return this;
		}

		/**
		 * @param assignedProjectsIDs an array of project ID's to assign to a Project Level Administrator,
		 *                            see Project Level Administration for more information
		 * @return UserBuilder – user builder object
		 */
		public UserBuilder assignedProjectsIDs(int... assignedProjectsIDs) {
			Preconditions.checkArgument(ArrayUtils.isNotEmpty(assignedProjectsIDs), "Assigned projects ID's should be not empty or null");
			this.assignedProjectsIDs.addAll(Ints.asList(assignedProjectsIDs));
			return this;
		}

		/**
		 * Build ready user object.
		 *
		 * @return User – constructed user object
		 */
		public User build() {
			User user = new User();

			user.name = name;
			user.email = email;
			user.roleID = roleID;
			user.admin = admin;
			user.emailNotificationsEnabled = emailNotificationsEnabled;
			user.mfaRequired = mfaRequired;
			user.ssoEnabled = ssoEnabled;
			user.groupIDs.addAll(groupIDs);
			user.assignedProjectsIDs.addAll(assignedProjectsIDs);

			return user;
		}
	}

	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static class UserUpdate extends User {
		/**
		 * Returns user object builder for update operations.
		 *
		 * @return UserUpdateBuilder – user update builder object
		 */
		public static UserUpdateBuilder constructUpdate() {
			return new UserUpdateBuilder();
		}

		/**
		 * Due to group ID's and assigned projects ID's are completely overwritten,
		 * it will be useful to have a complete collection of these elements.
		 * When updating, these elements will already be present when building the query.
		 *
		 * @param user pre-filled user
		 * @return UserUpdateBuilder – user update builder object
		 */
		public static UserUpdateBuilder constructUpdate(User user) {
			return new UserUpdateBuilder(user.groupIDs, user.assignedProjectsIDs);
		}

		@SuppressWarnings("FieldCanBeLocal")
		@NoArgsConstructor(access = AccessLevel.PRIVATE)
		public static class UserUpdateBuilder {
			private String name;
			private String email;
			private int roleID;
			private Boolean admin;
			private Boolean emailNotificationsEnabled;
			private Boolean mfaRequired;
			private Boolean ssoEnabled;
			private List<Integer> groupIDs = new ArrayList<>();
			private List<Integer> assignedProjectsIDs = new ArrayList<>();

			private UserUpdateBuilder(List<Integer> groupIDs, List<Integer> assignedProjectsIDs) {
				this.groupIDs = groupIDs;
				this.assignedProjectsIDs = assignedProjectsIDs;
			}

			/**
			 * @param name the name of the user
			 * @return UserUpdateBuilder – user update builder object
			 */
			public UserUpdateBuilder name(String name) {
				Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
				this.name = name;
				return this;
			}

			/**
			 * @param email the email address of the user
			 * @return UserUpdateBuilder – user update builder object
			 */
			public UserUpdateBuilder email(String email) {
				Preconditions.checkArgument(StringUtils.isNotEmpty(email), "Name should not be empty or null");
				this.email = email;
				return this;
			}

			/**
			 * @param roleID the ID of the global role to assign to the user
			 * @return UserUpdateBuilder – user update builder object
			 */
			public UserUpdateBuilder roleID(int roleID) {
				Preconditions.checkArgument(roleID > 0, "Role ID should be positive number");
				this.roleID = roleID;
				return this;
			}

			/**
			 * @param admin true to make the user a TestRail Administrator, default false
			 * @return UserUpdateBuilder – user update builder object
			 */
			public UserUpdateBuilder admin(boolean admin) {
				this.admin = admin;
				return this;
			}

			/**
			 * @param emailNotificationsEnabled false to disable email notifications for the user, default true
			 * @return UserUpdateBuilder – user update builder object
			 */
			public UserUpdateBuilder emailNotifications(boolean emailNotificationsEnabled) {
				this.emailNotificationsEnabled = emailNotificationsEnabled;
				return this;
			}

			/**
			 * @param mfaRequired true to require Multi-Factor Authentication for the user,
			 *                   default value matches the MFA setting of the instance
			 * @return UserUpdateBuilder – user update builder object
			 */
			public UserUpdateBuilder mfaRequired(boolean mfaRequired) {
				this.mfaRequired = mfaRequired;
				return this;
			}

			/**
			 * @param ssoEnabled true to enable SSO for the user, default value matches the SSO setting of the instance
			 * @return UserUpdateBuilder – user update builder object
			 */
			public UserUpdateBuilder ssoEnabled(boolean ssoEnabled) {
				this.ssoEnabled = ssoEnabled;
				return this;
			}

			/**
			 * If the group ID's already establish, it will be replaced.
			 * Use {@link Collections#emptyList()} for removing group ID's.
			 *
			 * @param groupIDs list of group ID's to assign the user to
			 * @return UserUpdateBuilder – user update builder object
			 */
			public UserUpdateBuilder groupIDs(List<Integer> groupIDs) {
				Preconditions.checkNotNull(groupIDs, "Group ID's should not be null");

				if (CollectionsUtils.isEmptyList(groupIDs)) {
					this.groupIDs = groupIDs;
					return this;
				} else if (CollectionsUtils.isEmptyList(this.groupIDs)) {
					this.groupIDs = new ArrayList<>();
				}

				this.groupIDs.addAll(groupIDs);
				return this;
			}

			/**
			 * If the group ID's already establish, it will be replaced.
			 *
			 * @param groupIDs array of group ID's to assign the user to
			 * @return UserUpdateBuilder – user update builder object
			 */
			public UserUpdateBuilder groupIDs(int... groupIDs) {
				Preconditions.checkArgument(ArrayUtils.isNotEmpty(groupIDs), "Group ID's should be not empty or null");

				if (CollectionsUtils.isEmptyList(this.groupIDs)) {
					this.groupIDs = new ArrayList<>();
				}

				this.groupIDs.addAll(Ints.asList(groupIDs));
				return this;
			}

			/**
			 * If the assigned projects ID's already establish, it will be replaced.
			 * Use {@link Collections#emptyList()} for removing assigned projects ID's.
			 *
			 * @param assignedProjectsIDs list of project ID's to assign to a Project Level Administrator,
			 *                            see Project Level Administration for more information
			 * @return UserUpdateBuilder – user update builder object
			 */
			public UserUpdateBuilder assignedProjectsIDs(List<Integer> assignedProjectsIDs) {
				Preconditions.checkNotNull(assignedProjectsIDs, "Assigned projects ID's should not be null");

				if (CollectionsUtils.isEmptyList(assignedProjectsIDs)) {
					this.assignedProjectsIDs = assignedProjectsIDs;
					return this;
				} else if (CollectionsUtils.isEmptyList(this.assignedProjectsIDs)) {
					this.assignedProjectsIDs = new ArrayList<>();
				}

				this.assignedProjectsIDs.addAll(assignedProjectsIDs);
				return this;
			}

			/**
			 * If the assigned projects ID's already establish, it will be replaced.
			 *
			 * @param assignedProjectsIDs an array of project ID's to assign to a Project Level Administrator,
			 *                            see Project Level Administration for more information
			 * @return UserUpdateBuilder – user update builder object
			 */
			public UserUpdateBuilder assignedProjectsIDs(int... assignedProjectsIDs) {
				Preconditions.checkArgument(ArrayUtils.isNotEmpty(assignedProjectsIDs), "Assigned projects ID's should be not empty or null");

				if (CollectionsUtils.isEmptyList(this.assignedProjectsIDs)) {
					this.assignedProjectsIDs = new ArrayList<>();
				}

				this.assignedProjectsIDs.addAll(Ints.asList(assignedProjectsIDs));
				return this;
			}

			/**
			 * Build ready user update object.
			 *
			 * @return UserUpdate – constructed user update object
			 */
			@SneakyThrows
			public UserUpdate build() {
				return BuilderUtils.fillWithBuilderFields(new UserUpdate(), this);
			}
		}
	}
}