package com.tronstone.client.testrail.api.models.parsing.modules;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.tronstone.client.testrail.api.models.beans.fields.CustomField;
import com.tronstone.client.testrail.api.models.beans.fields.Field;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import java.io.IOException;
import java.util.Map;

@AllArgsConstructor
public class CustomFieldModule extends SimpleModule {
    private Map<String, Field> fields;

    @Override
    public void setupModule(SetupContext setupContext) {
        setupContext.addBeanDeserializerModifier(new CustomFieldDeserializerModifier(fields));
        super.setupModule(setupContext);
    }

    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    private static class CustomFieldDeserializerModifier extends BeanDeserializerModifier {
        private Map<String, Field> caseFields;

        @Override
        public JsonDeserializer<?> modifyDeserializer(DeserializationConfig config, BeanDescription beanDescription, JsonDeserializer<?> deserializer) {
            if (CustomField.class.isAssignableFrom(beanDescription.getBeanClass())) {
                return new CustomFieldDeserializer(caseFields);
            }

            return super.modifyDeserializer(config, beanDescription, deserializer);
        }
    }

    private static class CustomFieldDeserializer extends StdDeserializer<CustomField> {
        private final Map<String, Field> fields;

        private CustomFieldDeserializer(Map<String, Field> fields) {
            super(CustomField.class);
            this.fields = fields;
        }

        @Override
        @SneakyThrows
        public CustomField getNullValue(DeserializationContext context) {
            Field field = fields.get(context.getParser().getCurrentName());
            return new CustomField(
                    field.getName(),
                    field.getLabel(),
                    field.getFieldType(),
                    field.getFieldType().getJavaType().getRawClass().isAssignableFrom(Boolean.class) ? false : null
            );
        }

        @Override
        public CustomField deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException {
            Field field = fields.get(context.getParser().getCurrentName());
            return new CustomField(
                    field.getName(),
                    field.getLabel(),
                    field.getFieldType(),
                    context.readValue(jsonParser, field.getFieldType().getJavaType())
            );
        }
    }
}
