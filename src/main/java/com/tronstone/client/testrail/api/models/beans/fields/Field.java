package com.tronstone.client.testrail.api.models.beans.fields;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.google.common.base.Preconditions;
import com.google.common.primitives.Ints;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;
import static com.fasterxml.jackson.annotation.JsonProperty.Access;

@Getter
@ToString
@EqualsAndHashCode
@JsonInclude(Include.NON_DEFAULT)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@JsonIgnoreProperties({"entity_id", "location_id", "is_multi", "status_id", "is_system"})
public class Field {
    private String name;
    private String label;
    private String description;
    private boolean includeAll;
    private final List<Integer> templateIDs = new ArrayList<>();
    private final List<FieldConfig> configs = new ArrayList<>();

    @JsonProperty("type")
    @JsonAlias("type_id")
    @JsonSerialize(converter = FieldTypeSerializeConverter.class)
    @JsonDeserialize(converter = FieldTypeDeserializeConverter.class)
    private FieldType<?> fieldType;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int id;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int displayOrder;

    @JsonProperty(access = Access.WRITE_ONLY)
    private String systemName;

    @JsonProperty(value = "is_active", access = Access.WRITE_ONLY)
    private boolean active;

    /**
     * Returns field object builder.
     *
     * @param fieldType the type enumeration for the new custom field
     * @param name the name for new the custom field
     * @param label the label for the new custom field
     * @param configs an object wrapped in an array with two default keys, ‘context’ and ‘options’
     * @return FieldBuilder – field builder object
     */
    public static FieldBuilder construct(FieldType<?> fieldType, String name, String label, List<FieldConfig> configs) {
        Preconditions.checkNotNull(fieldType, "Field type should not be null");
        Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
        Preconditions.checkArgument(!name.matches(".*\\d+.*"), "Name should not contains digits");
        Preconditions.checkArgument(StringUtils.isNotEmpty(label), "Label should not be empty or null");
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(configs), "Configs should not be empty or null");

        return new FieldBuilder(fieldType, name, label, configs);
    }

    /**
     * Returns list of field configs.
     *
     * @param fieldConfigs field configs array
     * @return List<FieldConfig> – list of field configs
     */
    public static List<FieldConfig> createConfigs(FieldConfig... fieldConfigs) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(fieldConfigs), "Field configs should not be empty or null");
        return Arrays.asList(fieldConfigs);
    }

    public int getID() {
        return id;
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public static class FieldBuilder {
        private @NonNull FieldType<?> fieldType;
        private @NonNull String name;
        private @NonNull String label;
        private @NonNull List<FieldConfig> configs;
        private String description;
        private boolean includeAll;
        private final List<Integer> templateIDs = new ArrayList<>();

        /**
         * @param description the description for the new custom field
         * @return FieldBuilder – field builder object
         */
        public FieldBuilder description(String description) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(description), "Description should not be empty or null");
            this.description = description;
            return this;
        }

        /**
         * Set flag to true if you want the new custom field included for all templates.
         * Otherwise, (false) specify the ID’s of templates
         * to be included as the (templateIDs) parameter.
         *
         * @param includeAll custom field included all flag, false by default
         * @return FieldBuilder – field builder object
         */
        public FieldBuilder includeAll(boolean includeAll) {
            this.includeAll = includeAll;
            return this;
        }

        /**
         * ID’s of templates new custom field will apply to if includeAll is set to false.
         *
         * @param templateIDs list of templates ID's
         * @return FieldBuilder – field builder object
         */
        public FieldBuilder templateIDs(List<Integer> templateIDs) {
            Preconditions.checkArgument(CollectionUtils.isNotEmpty(templateIDs), "Template ID's should not be empty or null");
            this.templateIDs.addAll(templateIDs);
            return this;
        }

        /**
         * ID’s of templates new custom field will apply to if includeAll is set to false.
         *
         * @param templateIDs array of templates ID's
         * @return FieldBuilder – field builder object
         */
        public FieldBuilder templateIDs(int... templateIDs) {
            Preconditions.checkArgument(ArrayUtils.isNotEmpty(templateIDs), "Template ID's should not be empty or null");
            this.templateIDs.addAll(Ints.asList(templateIDs));
            return this;
        }

        /**
         * Build ready field object.
         *
         * @return Field – constructed field object
         */
        public Field build() {
            if (includeAll && CollectionUtils.isNotEmpty(templateIDs)) {
                throw new IllegalArgumentException("If field includeAll() is true, than field templateIDs() should be empty");
            } else if (!includeAll && CollectionUtils.isEmpty(templateIDs)) {
                throw new IllegalArgumentException("If field includeAll() is false, than field templateIDs() should not be empty");
            }

            if (fieldType.equals(FieldType.DROPDOWN) || fieldType.equals(FieldType.MULTI_SELECT)) {
                configs.forEach(config -> Preconditions.checkArgument(
                        MapUtils.isNotEmpty(config.getOptions().getItems()),
                        "For Dropdown and Multi-Select field types config option items() is necessary"
                ));
            } else {
                configs.forEach(config -> Preconditions.checkArgument(
                        MapUtils.isEmpty(config.getOptions().getItems()),
                        "Config option items() is necessary only for Dropdown and Multi-Select field types"
                ));
            }

            if (fieldType.equals(FieldType.STEPS) || fieldType.equals(FieldType.TEXT)) {
                configs.forEach(config -> {
                    Preconditions.checkNotNull(
                            config.getOptions().getFormat(),
                            "For Steps and Text field types config option format() is necessary"
                    );
                    Preconditions.checkArgument(
                            config.getOptions().getRows() != -1,
                            "For Steps and Text field types config option rows() is necessary"
                    );
                });
            } else {
                configs.forEach(config -> {
                    Preconditions.checkArgument(
                            Objects.isNull(config.getOptions().getFormat()),
                            "Config option format() is necessary only for Steps and Text field types"
                    );
                    Preconditions.checkArgument(
                            config.getOptions().getRows() == -1,
                            "Config option rows() is necessary only for Steps and Text field types"
                    );
                });
            }

            Field field = new Field();

            field.fieldType = fieldType;
            field.name = name;
            field.label = label;
            field.description = description;
            field.includeAll = includeAll;
            field.configs.addAll(configs);
            field.templateIDs.addAll(templateIDs);

            return field;
        }
    }

    private static class FieldTypeSerializeConverter extends StdConverter<FieldType<?>, String> {
        @Override
        public String convert(FieldType<?> fieldType) {
            return Integer.toString(fieldType.getId());
        }
    }

    private static class FieldTypeDeserializeConverter extends StdConverter<Integer, FieldType<?>> {
        @Override
        public FieldType<?> convert(Integer fieldTypeID) {
            return FieldType.valueOf(fieldTypeID);
        }
    }
}
