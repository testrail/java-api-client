package com.tronstone.client.testrail.api.models.beans.runs;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;
import com.google.common.primitives.Ints;
import com.tronstone.client.inner.utils.BuilderUtils;
import com.tronstone.client.inner.utils.CollectionsUtils;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.ReferencesDeserializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.TimeOnDeserializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.serialize.ReferencesSerializeConverter;
import com.tronstone.client.testrail.api.models.parsing.filters.EmptyListIncludeFilter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@ToString
@EqualsAndHashCode
@JsonInclude(Include.NON_DEFAULT)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Run {
    public static final String CUSTOM_STATUS_COUNT_POSTFIX = "_count";

    @JsonView({Run.class, RunUpdate.class})
    private String name;

    @JsonView({Run.class, RunUpdate.class})
    private String description;

    @JsonView({Run.class, RunUpdate.class})
    private int milestoneID;

    @JsonView(Run.class)
    private int suiteID;

    @JsonView({Run.class, RunUpdate.class})
    private @Getter(AccessLevel.NONE) Boolean includeAll;

    @JsonView(Run.class)
    @JsonProperty("assignedto_id")
    private int assigneeID;

    @JsonView({Run.class, RunUpdate.class})
    @JsonSerialize(converter = ReferencesSerializeConverter.class)
    @JsonDeserialize(converter = ReferencesDeserializeConverter.class)
    @JsonInclude(value = Include.CUSTOM, valueFilter = EmptyListIncludeFilter.class)
    private final List<String> refs = new ArrayList<>();

    @JsonView({Run.class, RunUpdate.class})
    @JsonProperty(access = Access.READ_ONLY)
    @JsonInclude(value = Include.CUSTOM, valueFilter = EmptyListIncludeFilter.class)
    private final @Getter(AccessLevel.NONE) List<Integer> caseIDs = new ArrayList<>();

    @JsonProperty(access = Access.WRITE_ONLY)
    private int id;

    @JsonProperty(access = Access.WRITE_ONLY)
    private String url;

    @JsonProperty(access = Access.WRITE_ONLY)
    private String config;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int projectID;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int planID;

    @JsonProperty(access = Access.WRITE_ONLY)
    private String entryID;

    @JsonProperty(access = Access.WRITE_ONLY)
    private List<Integer> configIDs;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int untestedCount;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int passedCount;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int retestCount;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int failedCount;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int blockedCount;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int entryIndex;

    @JsonProperty(value = "created_by", access = Access.WRITE_ONLY)
    private int creatorID;

    @JsonProperty(value = "is_completed", access = Access.WRITE_ONLY)
    private boolean completed;

    @JsonProperty(access = Access.WRITE_ONLY)
    @JsonDeserialize(converter = TimeOnDeserializeConverter.class)
    private DateTime createdOn;

    @JsonProperty(access = Access.WRITE_ONLY)
    @JsonDeserialize(converter = TimeOnDeserializeConverter.class)
    private DateTime updatedOn;

    @JsonProperty(access = Access.WRITE_ONLY)
    @JsonDeserialize(converter = TimeOnDeserializeConverter.class)
    private DateTime completedOn;

    @JsonIgnore
    private final @Getter(AccessLevel.NONE) Map<String, Integer> customStatuses = new HashMap<>();

    /**
     * Returns run object builder.
     *
     * @return RunBuilder – run builder object
     */
    public static RunBuilder construct() {
        return new RunBuilder();
    }

    public int getID() {
        return id;
    }

    public String getURL() {
        return url;
    }

    public boolean isIncludeAll() {
        return includeAll;
    }

    /**
     * The number of tests in the test run with the respective custom status.
     *
     * @param systemName the unique name of custom status in the database without '_count' postfix
     * @return int – the number of tests in the test run
     */
    public int getCustomStatusCount(String systemName) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(systemName), "System name should not be empty or null");
        Preconditions.checkArgument(
                !systemName.endsWith(CUSTOM_STATUS_COUNT_POSTFIX),
                "System name should not have '%s' postfix",
                CUSTOM_STATUS_COUNT_POSTFIX
        );

        return customStatuses.get(systemName);
    }

    @JsonAnySetter
    private void deserializeCustomStatus(String statusSystemName, int count) {
        if (statusSystemName.endsWith(CUSTOM_STATUS_COUNT_POSTFIX)) {
            customStatuses.put(statusSystemName.replaceFirst(CUSTOM_STATUS_COUNT_POSTFIX, StringUtils.EMPTY), count);
        } else {
            throw new UnsupportedOperationException("Unrecognized json property: " + statusSystemName);
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public static class RunBuilder {
        private String name;
        private String description;
        private int milestoneID;
        private int suiteID;
        private int assigneeID;
        private boolean includeAll = true;
        private final List<String> refs = new ArrayList<>();
        private final List<Integer> caseIDs = new ArrayList<>();

        /**
         * @param name the name of the test run
         * @return RunBuilder – run builder object
         */
        public RunBuilder name(String name) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
            this.name = name;
            return this;
        }

        /**
         * @param description the description of the test run
         * @return RunBuilder – run builder object
         */
        public RunBuilder description(String description) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(description), "Description should not be empty or null");
            this.description = description;
            return this;
        }

        /**
         * @param milestoneID the ID of the milestone to link to the test run
         * @return RunBuilder – run builder object
         */
        public RunBuilder milestoneID(int milestoneID) {
            Preconditions.checkArgument(milestoneID > 0, "Milestone ID should be positive number");
            this.milestoneID = milestoneID;
            return this;
        }

        /**
         * The ID of the test suite for the test run
         * (optional if the project is operating in single suite mode, required otherwise).
         *
         * @param suiteID the ID of the test suite
         * @return RunBuilder – run builder object
         */
        public RunBuilder suiteID(int suiteID) {
            Preconditions.checkArgument(suiteID > 0, "Suite ID should be positive number");
            this.suiteID = suiteID;
            return this;
        }

        /**
         * @param assigneeID the ID of the user the test run should be assigned to
         * @return RunBuilder – run builder object
         */
        public RunBuilder assigneeID(int assigneeID) {
            Preconditions.checkArgument(assigneeID > 0, "Assignee ID should be positive number");
            this.assigneeID = assigneeID;
            return this;
        }

        /**
         * True for including all test cases of the test suite
         * and false for a custom case selection (default: true).
         *
         * @param includeAll test cases included all flag, true by default
         * @return RunBuilder – run builder object
         */
        public RunBuilder includeAll(boolean includeAll) {
            this.includeAll = includeAll;
            return this;
        }

        /**
         * @param caseIDs list of case ID's for the custom case selection
         * @return RunBuilder – run builder object
         */
        public RunBuilder caseIDs(List<Integer> caseIDs) {
            Preconditions.checkArgument(CollectionUtils.isNotEmpty(caseIDs), "Case ID's should not be empty or null");
            this.caseIDs.addAll(caseIDs);
            return this;
        }

        /**
         * @param caseIDs an array of case ID's for the custom case selection
         * @return RunBuilder – run builder object
         */
        public RunBuilder caseIDs(int... caseIDs) {
            Preconditions.checkArgument(ArrayUtils.isNotEmpty(caseIDs), "Case ID's should not be empty or null");
            this.caseIDs.addAll(Ints.asList(caseIDs));
            return this;
        }

        /**
         * Add the list of references/requirements.
         *
         * @param refs list of references/requirements
         * @return RunBuilder – run builder object
         */
        public RunBuilder refs(List<String> refs) {
            Preconditions.checkArgument(CollectionUtils.isNotEmpty(refs), "References should not be empty or null");
            this.refs.addAll(refs);
            return this;
        }

        /**
         * Parse and add list of comma-separated references/requirements.
         *
         * @param refs a comma-separated list of references/requirements
         * @return RunBuilder – run builder object
         */
        public RunBuilder refs(String refs) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(refs), "References should not be empty or null");
            this.refs.addAll(new ReferencesDeserializeConverter().convert(refs));
            return this;
        }

        /**
         * Add the array of references/requirements.
         *
         * @param refs array of references/requirements
         * @return RunBuilder – run builder object
         */
        public RunBuilder refs(String... refs) {
            Preconditions.checkArgument(ArrayUtils.isNotEmpty(refs), "References should not be empty or null");

            this.refs.addAll(Arrays.stream(refs)
                    .peek(ref -> Preconditions.checkArgument(StringUtils.isNotEmpty(ref), "Reference should not be empty or null"))
                    .collect(Collectors.toList()));

            return this;
        }

        /**
         * Build ready run object.
         *
         * @return Run – constructed run object
         */
        public Run build() {
            if (includeAll && CollectionUtils.isNotEmpty(caseIDs)) {
                throw new IllegalArgumentException("If run includeAll() is true, than run caseIDs() should be empty");
            } else if (!includeAll && CollectionUtils.isEmpty(caseIDs)) {
                throw new IllegalArgumentException("If run includeAll() is false, than run caseIDs() should not be empty");
            }

            Run run = new Run();

            run.name = name;
            run.description = description;
            run.milestoneID = milestoneID;
            run.suiteID = suiteID;
            run.assigneeID = assigneeID;
            run.includeAll = includeAll;
            run.refs.addAll(refs);
            run.caseIDs.addAll(caseIDs);

            return run;
        }
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class RunUpdate extends Run {
        /**
         * Returns run object builder for update operations.
         *
         * @return RunUpdateBuilder – run update builder object
         */
        public static RunUpdateBuilder constructUpdate() {
            return new RunUpdateBuilder();
        }

        /**
         * Due to case ID's and references are completely overwritten,
         * it will be useful to have a complete collection of these elements.
         * When updating, these elements will already be present when building the query.
         *
         * @param run pre-filled run
         * @return RunUpdateBuilder – run update builder object
         */
        public static RunUpdateBuilder constructUpdate(Run run) {
            return new RunUpdateBuilder(run.caseIDs, run.refs);
        }

        @SuppressWarnings("FieldCanBeLocal")
        @NoArgsConstructor(access = AccessLevel.PRIVATE)
        public static class RunUpdateBuilder {
            private String name;
            private String description;
            private int milestoneID;
            private Boolean includeAll;
            private List<Integer> caseIDs = new ArrayList<>();
            private List<String> refs = new ArrayList<>();

            public RunUpdateBuilder(List<Integer> caseIDs, List<String> refs) {
                this.caseIDs = caseIDs;
                this.refs = refs;
            }

            /**
             * @param name the name of the test run
             * @return RunUpdateBuilder – run update builder object
             */
            public RunUpdateBuilder name(String name) {
                Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
                this.name = name;
                return this;
            }

            /**
             * Use empty string for removing description.
             *
             * @param description the description of the test run
             * @return RunUpdateBuilder – run update builder object
             */
            public RunUpdateBuilder description(String description) {
                Preconditions.checkNotNull(description, "Description should not be null");
                this.description = description;
                return this;
            }

            /**
             * @param milestoneID the ID of the milestone to link to the test run
             * @return RunUpdateBuilder – run update builder object
             */
            public RunUpdateBuilder milestoneID(int milestoneID) {
                Preconditions.checkArgument(milestoneID > 0, "Milestone ID should be positive number");
                this.milestoneID = milestoneID;
                return this;
            }

            /**
             * True for including all test cases of the test suite
             * and false for a custom case selection (default: true).
             *
             * @param includeAll test cases included all flag, true by default
             * @return RunUpdateBuilder – run update builder object
             */
            public RunUpdateBuilder includeAll(boolean includeAll) {
                this.includeAll = includeAll;
                return this;
            }

            /**
             * If the case ID's already establish, it will be replaced.
             * Use {@link Collections#emptyList()} for removing case ID's.
             *
             * @param caseIDs list of case ID's for the custom case selection
             * @return RunUpdateBuilder – run update builder object
             */
            public RunUpdateBuilder caseIDs(List<Integer> caseIDs) {
                Preconditions.checkNotNull(caseIDs, "Case ID's should not be null");

                if (CollectionsUtils.isEmptyList(caseIDs)) {
                    this.caseIDs = caseIDs;
                    return this;
                } else if (CollectionsUtils.isEmptyList(this.caseIDs)) {
                    this.caseIDs = new ArrayList<>();
                }

                this.caseIDs.addAll(caseIDs);
                return this;
            }

            /**
             * If the case ID's already establish, it will be replaced.
             *
             * @param caseIDs an array of case ID's for the custom case selection
             * @return RunUpdateBuilder – run update builder object
             */
            public RunUpdateBuilder caseIDs(int... caseIDs) {
                Preconditions.checkArgument(ArrayUtils.isNotEmpty(caseIDs), "Case ID's should be not empty or null");

                if (CollectionsUtils.isEmptyList(this.caseIDs)) {
                    this.caseIDs = new ArrayList<>();
                }

                this.caseIDs.addAll(Ints.asList(caseIDs));
                return this;
            }

            /**
             * Add the list of references/requirements.
             * If the refs already establish, it will be replaced.
             * Use {@link Collections#emptyList()} for removing references/requirements.
             *
             * @param refs list of references/requirements
             * @return RunUpdateBuilder – run update builder object
             */
            public RunUpdateBuilder refs(List<String> refs) {
                Preconditions.checkNotNull(refs, "References should not be null");

                if (CollectionsUtils.isEmptyList(refs)) {
                    this.refs = refs;
                    return this;
                } else if (CollectionsUtils.isEmptyList(this.refs)) {
                    this.refs = new ArrayList<>();
                }

                this.refs.addAll(refs);
                return this;
            }

            /**
             * Parse and add list of comma-separated references/requirements.
             * If the refs already establish, it will be replaced.
             * Use empty string for removing references/requirements.
             *
             * @param refs a comma-separated list of references/requirements
             * @return RunUpdateBuilder – run update builder object
             */
            public RunUpdateBuilder refs(String refs) {
                if (StringUtils.isEmpty(refs)) {
                    if (CollectionsUtils.isNotEmptyList(this.refs)) {
                        this.refs = Collections.emptyList();
                    }

                    return this;
                } else if (CollectionsUtils.isEmptyList(this.refs)) {
                    this.refs = new ArrayList<>();
                }

                this.refs.addAll(new ReferencesDeserializeConverter().convert(refs));
                return this;
            }

            /**
             * Add the array of references/requirements.
             * If the refs already establish, it will be replaced.
             *
             * @param refs array of references/requirements
             * @return RunUpdateBuilder – run update builder object
             */
            public RunUpdateBuilder refs(String... refs) {
                Preconditions.checkArgument(ArrayUtils.isNotEmpty(refs), "References should not be empty or null");

                if (CollectionsUtils.isEmptyList(this.refs)) {
                    this.refs = new ArrayList<>();
                }

                this.refs.addAll(Arrays.stream(refs)
                        .peek(ref -> Preconditions.checkArgument(StringUtils.isNotEmpty(ref), "Reference should not be empty or null"))
                        .collect(Collectors.toList()));

                return this;
            }

            /**
             * Build ready run update object.
             *
             * @return RunUpdate – constructed run update object
             */
            public RunUpdate build() {
                if (Objects.isNull(includeAll) && CollectionUtils.isNotEmpty(caseIDs)) {
                    throw new IllegalArgumentException("If run update caseIDs() is not empty, than provide run update includeAll() flag");
                } else if (Objects.nonNull(includeAll)) {
                    if (includeAll && CollectionUtils.isNotEmpty(caseIDs)) {
                        throw new IllegalArgumentException("If run update includeAll() is true, than run update caseIDs() should be empty");
                    } else if (!includeAll && CollectionUtils.isEmpty(caseIDs)) {
                        throw new IllegalArgumentException("If run update includeAll() is false, than run update caseIDs() should not be empty");
                    }
                }

                return BuilderUtils.fillWithBuilderFields(new RunUpdate(), this);
            }
        }
    }
}
