package com.tronstone.client.testrail.api.models.beans.cases.history;

import lombok.AllArgsConstructor;

import java.util.Arrays;

/**
 * The following case history types are supported:
 * <pre>
 *      STRING
 *      INTEGER
 *      BOOLEAN
 *      DATE
 *      TIMESPAN
 *      TEXT
 *      URL
 *      STEPS
 * </pre>
 */
@AllArgsConstructor
public enum CaseHistoryChangesType {
    STRING(1),
    INTEGER(2),
    BOOLEAN(3),
    DATE(4),
    TIMESPAN(5),
    TEXT(6),
    URL(7),
    STEPS(8);

    private final int id;

    @Override
    public String toString() {
        return name().toLowerCase();
    }

    public static CaseHistoryChangesType valueOfChangesType(int changesTypeID) {
        return Arrays.stream(values())
                .filter(changesType -> changesType.id == changesTypeID)
                .findFirst()
                .orElseThrow(() -> new UnsupportedOperationException("Unsupported or non-existing case history changes type"));
    }
}
