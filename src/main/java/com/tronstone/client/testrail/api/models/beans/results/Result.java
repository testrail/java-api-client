package com.tronstone.client.testrail.api.models.beans.results;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;
import com.tronstone.client.testrail.api.models.beans.fields.CustomField;
import com.tronstone.client.testrail.api.models.beans.fields.FieldType;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.EstimateDeserializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.ReferencesDeserializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.TimeOnDeserializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.serialize.EstimateSerializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.serialize.ReferencesSerializeConverter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Period;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.tronstone.client.testrail.api.models.beans.fields.CustomField.CUSTOM_FIELD_SYSTEM_PREFIX;

@Getter
@ToString
@EqualsAndHashCode
@JsonInclude(Include.NON_DEFAULT)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Result {
    private int testID;
    private int statusID;
    private String comment;
    private String version;

    @JsonSerialize(converter = EstimateSerializeConverter.class)
    @JsonDeserialize(converter = EstimateDeserializeConverter.class)
    private Period elapsed;

    @JsonSerialize(converter = ReferencesSerializeConverter.class)
    @JsonDeserialize(converter = ReferencesDeserializeConverter.class)
    private final List<String> defects = new ArrayList<>();

    @JsonProperty("assignedto_id")
    private int assigneeID;

    @JsonProperty(access = Access.READ_ONLY)
    private @Getter(AccessLevel.NONE) int caseID;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int id;

    @JsonProperty(access = Access.WRITE_ONLY)
    private List<String> attachmentIDs;

    @JsonProperty(value = "created_by", access = Access.WRITE_ONLY)
    private int creatorID;

    @JsonProperty(access = Access.WRITE_ONLY)
    @JsonDeserialize(converter = TimeOnDeserializeConverter.class)
    private DateTime createdOn;

    @JsonIgnore
    private final @Getter(AccessLevel.NONE) Map<String, CustomField> customFields = new HashMap<>();

    /**
     * Returns result object builder.
     *
     * @return ResultBuilder – result builder object
     */
    public static ResultBuilder construct() {
        return new ResultBuilder();
    }

    public int getID() {
        return id;
    }

    /**
     * To get the value with correct type use {@link FieldType} mapped types.
     * Warning, label it is not unique value, if two custom field will
     * have the same value, will return first available. For more consistent
     * results use {@link Result#getCustomField(String, FieldType)}
     *
     * @param resultFieldType the custom field type
     * @param label the label of the field as it appears in the user interface
     * @param <T> type of custom field value
     * @return T – the value of the custom field or null if value not exists
     */
    @SuppressWarnings("unchecked")
    public <T> T getCustomField(FieldType<T> resultFieldType, String label) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(label), "Label should not be empty or null");

        CustomField customField = customFields.values()
                .stream()
                .filter(field -> field.getLabel().equalsIgnoreCase(label))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("The label does not match any of the existing custom fields"));

        Preconditions.checkArgument(
                customField.getFieldType().equals(resultFieldType),
                String.format(
                        "Result field type should be the same as type with containing field: expected type '%s' but actual '%s'",
                        resultFieldType.getName(),
                        customField.getFieldType().getName()
                )
        );

        return (T) customField.getFieldValue();
    }

    /**
     * To get the value with correct type use {@link FieldType} mapped types.
     *
     * @param systemName the unique name of custom case field in the database without 'custom_' prefix
     * @param resultFieldType the custom field type
     * @param <T> – type of custom field value
     * @return T – the value of the custom field or null if value not exists
     */
    @SuppressWarnings("unchecked")
    public <T> T getCustomField(String systemName, FieldType<T> resultFieldType) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(systemName), "System name should not be empty or null");
        Preconditions.checkArgument(
                !systemName.startsWith(CUSTOM_FIELD_SYSTEM_PREFIX),
                "System name should not have '%s' prefix",
                CUSTOM_FIELD_SYSTEM_PREFIX
        );

        CustomField customField = customFields.get(systemName);
        Preconditions.checkArgument(
                customField.getFieldType().equals(resultFieldType),
                String.format(
                        "Result field type should be the same as type with containing field: expected type '%s' but actual '%s'",
                        resultFieldType.getName(),
                        customField.getFieldType().getName()
                )
        );

        return (T) customField.getFieldValue();
    }

    @JsonAnyGetter
    private Map<String, Object> serializeCustomFields() {
        return customFields.values().stream().collect(
                HashMap::new,
                (map, customField) -> map.put(customField.getPrefixedSystemName(), customField.getFieldValue()),
                HashMap::putAll
        );
    }

    @JsonAnySetter
    private void deserializeCustomField(String fieldSystemName, CustomField customField) {
        if (fieldSystemName.startsWith(CUSTOM_FIELD_SYSTEM_PREFIX)) {
            customFields.put(fieldSystemName.replaceFirst(CUSTOM_FIELD_SYSTEM_PREFIX, StringUtils.EMPTY), customField);
        } else {
            throw new UnsupportedOperationException("Unrecognized json property: " + fieldSystemName);
        }
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class ResultBuilder {
        private int testID;
        private int caseID;
        private int statusID;
        private String comment;
        private String version;
        private Period elapsed;
        private int assigneeID;
        private final List<String> defects = new ArrayList<>();
        private final Map<String, CustomField> customFields = new HashMap<>();

        /**
         * Each test result must specify the test ID.
         * Required for multiply results adding.
         *
         * @param testID the ID of the test
         * @return ResultBuilder – result builder object
         */
        public ResultBuilder testID(int testID) {
            Preconditions.checkArgument(testID > 0, "Test ID should be positive number");
            this.testID = testID;
            return this;
        }

        /**
         * Each test result must specify the case ID.
         * Required for multiply results adding for case.
         *
         * @param caseID the ID of the test case
         * @return ResultBuilder – result builder object
         */
        public ResultBuilder caseID(int caseID) {
            Preconditions.checkArgument(caseID > 0, "Case ID should be positive number");
            this.caseID = caseID;
            return this;
        }

        /**
         * @param statusID the ID of the test status
         * @return ResultBuilder – result builder object
         */
        public ResultBuilder statusID(int statusID) {
            Preconditions.checkArgument(statusID > 0, "Status ID should be positive number");
            this.statusID = statusID;
            return this;
        }

        /**
         * @param comment the comment/description for the test result
         * @return ResultBuilder – result builder object
         */
        public ResultBuilder comments(String comment) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(comment), "Comments should not be empty or null");
            this.comment = comment;
            return this;
        }

        /**
         * @param version the version or build you tested against
         * @return ResultBuilder – result builder object
         */
        public ResultBuilder version(String version) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(version), "Version should not be empty or null");
            this.version = version;
            return this;
        }

        /**
         * Set the elapsed period.
         *
         * @param elapsed the time it took to execute the test
         * @return ResultBuilder – result builder object
         */
        public ResultBuilder elapsed(Period elapsed) {
            Preconditions.checkNotNull(elapsed, "Elapsed should not be null");
            Preconditions.checkArgument(!elapsed.equals(Period.ZERO), "Elapsed should not be zero");

            this.elapsed = elapsed;
            return this;
        }

        /**
         * Parse and set elapsed time.
         *
         * @param elapsed the time it took to execute the test, e.g. “30s” or “1m 45s”
         * @return ResultBuilder – result builder object
         */
        public ResultBuilder elapsed(String elapsed) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(elapsed), "Elapsed should not be empty or null");
            this.elapsed = new EstimateDeserializeConverter().convert(elapsed);
            return this;
        }

        /**
         * @param assigneeID the ID of a user the test should be assigned to
         * @return ResultBuilder – result builder object
         */
        public ResultBuilder assigneeID(int assigneeID) {
            Preconditions.checkArgument(assigneeID > 0, "Assignee ID should be positive number");
            this.assigneeID = assigneeID;
            return this;
        }

        /**
         * Add the list of defects to link to the test result.
         *
         * @param defects list of defects
         * @return ResultBuilder – result builder object
         */
        public ResultBuilder defects(List<String> defects) {
            Preconditions.checkArgument(CollectionUtils.isNotEmpty(defects), "Defects should not be empty or null");
            this.defects.addAll(defects);
            return this;
        }

        /**
         * Parse and add list of defects to link to the test result.
         *
         * @param defects a comma-separated list of defects
         * @return ResultBuilder – result builder object
         */
        public ResultBuilder defects(String defects) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(defects), "Defects should not be empty or null");
            this.defects.addAll(new ReferencesDeserializeConverter().convert(defects));
            return this;
        }

        /**
         * Add the array of defects to link to the test result.
         *
         * @param defects array of defects
         * @return ResultBuilder – result builder object
         */
        public ResultBuilder defects(String... defects) {
            Preconditions.checkArgument(ArrayUtils.isNotEmpty(defects), "Defects should not be empty or null");

            this.defects.addAll(Arrays.stream(defects)
                    .peek(defect -> Preconditions.checkArgument(StringUtils.isNotEmpty(defect), "Defect should not be empty or null"))
                    .collect(Collectors.toList()));

            return this;
        }

        /**
         * Add custom field to result.
         *
         * @param systemName the unique name of custom case field in the database without 'custom_' prefix
         * @param resultFieldType the custom field type
         * @param value the custom field value data
         * @param <T> type of custom field value
         * @return ResultBuilder – result builder object
         */
        public <T> ResultBuilder addCustomField(String systemName, FieldType<T> resultFieldType, T value) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(systemName), "System name should not be empty or null");
            Preconditions.checkNotNull(resultFieldType, "Result field type should not be null");
            Preconditions.checkNotNull(value, "Value should not be null");
            Preconditions.checkArgument(!systemName.startsWith(CUSTOM_FIELD_SYSTEM_PREFIX), "System name should not have 'custom_' prefix");
            Preconditions.checkArgument(!resultFieldType.equals(FieldType.STEPS), "'Steps' is not supporting for add custom field");

            customFields.put(systemName, new CustomField(CUSTOM_FIELD_SYSTEM_PREFIX + systemName, resultFieldType, value));
            return this;
        }

        /**
         * Build ready result object.
         *
         * @return Result – constructed result object
         */
        public Result build() {
            Result result = new Result();

            result.testID = testID;
            result.caseID = caseID;
            result.statusID = statusID;
            result.comment = comment;
            result.version = version;
            result.elapsed = elapsed;
            result.assigneeID = assigneeID;
            result.defects.addAll(defects);
            result.customFields.putAll(customFields);

            return result;
        }
    }
}