package com.tronstone.client.testrail.api.models.beans.statuses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CaseStatus {
    private String name;
    private String abbreviation;

    @JsonProperty("case_status_id")
    private int id;

    @JsonProperty("is_default")
    private @Getter(AccessLevel.NONE) boolean statusDefault;

    @JsonProperty("is_approved")
    private boolean approved;

    public int getID() {
        return id;
    }

    public boolean isDefault() {
        return statusDefault;
    }
}
