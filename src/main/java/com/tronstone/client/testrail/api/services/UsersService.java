package com.tronstone.client.testrail.api.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.base.Preconditions;
import com.tronstone.client.inner.annotations.MapName;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.testrail.api.models.beans.users.User;
import com.tronstone.client.testrail.api.models.beans.users.User.UserUpdate;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.ContentType;

import java.util.List;

/**
 * Use the following API methods to request details about users.
 * Any user can retrieve his/her own account information.
 * Retrieving information for any other use requires administrator access.
 */
public class UsersService extends AbstractTestRailService {
    public UsersService(TestRailClientConfiguration configuration) {
        super(configuration);
    }

    /**
     * Returns an existing user.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/users#getuser">Get User</a>
     * @param userID the ID of the user
     * @return User – object representation of the user
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_user/{user_id}")
    public User getUser(@MapName("user_id") int userID) {
        Preconditions.checkArgument(userID > 0, "User ID should be positive number");
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), User.class);
    }

    /**
     * Returns user details for the TestRail user making the API request.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/users#getcurrentuser">Get Current User</a>
     * @return User – object representation of the user
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_current_user")
    public User getCurrentUser() {
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), User.class);
    }

    /**
     * Returns an existing user by his/her email address.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/users#getuserbyemail">Get User by Email</a>
     * @param email the email address to get the user for
     * @return User – object representation of the user
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_user_by_email&email={email}")
    public User getUserByEmail(String email) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(email), "Email should not be empty or null");
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), User.class);
    }

    /**
     * Returns a list of users.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/users#getusers">Get Users</a>
     * @return List<User> – list of users
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_users")
    public List<User> getUsers() {
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), new TypeReference<List<User>>() { });
    }

    /**
     * Returns a list of users.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/users#getusers">Get Users</a>
     * @param projectID the ID of the project for which you would like to retrieve user information
     * @return List<User> – list of users
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_users/{project_id}")
    public List<User> getUsers(@MapName("project_id") int projectID) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), new TypeReference<List<User>>() { });
    }

    /**
     * Creates a new user.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/users#adduser">Add User</a>
     * @param user new user
     * @return User – created user
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_user")
    public User addUser(User user) {
        Preconditions.checkNotNull(user, "User should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writeValueAsBytes(user), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                User.class
        );
    }

    /**
     * Updates an existing user.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/users#updateuser">Update User</a>
     * @param userID the ID of the user
     * @param userUpdate user that to be updated
     * @return User – updated user
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "update_user/{user_id}")
    public User updateUser(@MapName("user_id") int userID, UserUpdate userUpdate) {
        Preconditions.checkArgument(userID > 0, "User ID should be positive number");
        Preconditions.checkNotNull(userUpdate, "User update should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writeValueAsBytes(userUpdate), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                User.class
        );
    }
}
