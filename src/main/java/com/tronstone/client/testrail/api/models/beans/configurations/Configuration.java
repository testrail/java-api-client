package com.tronstone.client.testrail.api.models.beans.configurations;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Configuration {
    private int id;
    private String name;
    private int groupID;

    public int getID() {
        return id;
    }
}
