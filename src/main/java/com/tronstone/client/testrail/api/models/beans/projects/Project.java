package com.tronstone.client.testrail.api.models.beans.projects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.google.common.base.Preconditions;
import com.tronstone.client.inner.utils.BuilderUtils;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.TimeOnDeserializeConverter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.List;

import static com.fasterxml.jackson.annotation.JsonProperty.Access;

@Getter
@ToString
@EqualsAndHashCode
@JsonInclude(Include.NON_DEFAULT)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Project {
	private String name;
	private String announcement;
	private @Getter(AccessLevel.NONE) Boolean showAnnouncement;

	@JsonSerialize(converter = SuiteModeSerializeConverter.class)
	@JsonDeserialize(converter = SuiteModeDeserializeConverter.class)
	private SuiteMode suiteMode;

	@JsonProperty(access = Access.WRITE_ONLY)
	private int id;

	@JsonProperty(access = Access.WRITE_ONLY)
	private String url;

	@JsonProperty(access = Access.WRITE_ONLY)
	private int defaultRoleID;

	@JsonProperty(access = Access.WRITE_ONLY)
	private List<ProjectGroup> groups;

	@JsonProperty(access = Access.WRITE_ONLY)
	private List<ProjectUser> users;

	@JsonProperty(access = Access.WRITE_ONLY)
	@JsonDeserialize(converter = TimeOnDeserializeConverter.class)
	private DateTime completedOn;

	@JsonProperty(value = "default_role", access = Access.WRITE_ONLY)
	private String defaultRoleName;

	@JsonProperty(value = "is_completed", access = Access.WRITE_ONLY)
	private boolean completed;

	/**
	 * Returns project object builder.
	 *
	 * @param name the name of the project
	 * @return ProjectBuilder – project builder object
	 */
	public static ProjectBuilder construct(String name) {
		Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
		return new ProjectBuilder(name);
	}

	public int getID() {
		return id;
	}

	public String getURL() {
		return url;
	}

	public boolean isShowAnnouncement() {
		return showAnnouncement;
	}

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	public static class ProjectBuilder {
		private @NonNull String name;
		private String announcement;
		private Boolean showAnnouncement;
		private SuiteMode suiteMode;

		/**
		 * @param announcement the description/announcement of the project
		 * @return ProjectBuilder – project builder object
		 */
		public ProjectBuilder announcement(String announcement) {
			Preconditions.checkArgument(StringUtils.isNotEmpty(announcement), "Announcement should not be empty or null");
			this.announcement = announcement;
			return this;
		}

		/**
		 * True if the announcement should be displayed on
		 * the project’s overview page and false otherwise.
		 *
		 * @param showAnnouncement project show announcement flag, false by default
		 * @return ProjectBuilder – project builder object
		 */
		public ProjectBuilder showAnnouncement(boolean showAnnouncement) {
			this.showAnnouncement = showAnnouncement;
			return this;
		}

		/**
		 * @param suiteMode the suite mode of the project
		 * @return ProjectBuilder – project builder object
		 */
		public ProjectBuilder suiteMode(SuiteMode suiteMode) {
			Preconditions.checkNotNull(suiteMode, "Suite mode should not be null");
			this.suiteMode = suiteMode;
			return this;
		}

		/**
		 * Build ready project object.
		 *
		 * @return Project – constructed project object
		 */
		public Project build() {
			Project project = new Project();

			project.name = name;
			project.announcement = announcement;
			project.showAnnouncement = showAnnouncement;
			project.suiteMode = suiteMode;

			return project;
		}
	}

	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static class ProjectUpdate extends Project {
		/**
		 * Returns project object builder for update operations.
		 *
		 * @return ProjectUpdateBuilder – project update builder object
		 */
		public static ProjectUpdateBuilder constructUpdate() {
			return new ProjectUpdateBuilder();
		}

		@SuppressWarnings("FieldCanBeLocal")
		@NoArgsConstructor(access = AccessLevel.PRIVATE)
		public static class ProjectUpdateBuilder {
			private String name;
			private String announcement;
			private Boolean showAnnouncement;
			private SuiteMode suiteMode;

			/**
			 * @param name the name of the project
			 * @return ProjectUpdateBuilder – project builder object
			 */
			public ProjectUpdateBuilder name(String name) {
				Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
				this.name = name;
				return this;
			}

			/**
			 * Use empty string for removing announcement.
			 *
			 * @param announcement the description/announcement of the project
			 * @return ProjectUpdateBuilder – project builder object
			 */
			public ProjectUpdateBuilder announcement(String announcement) {
				Preconditions.checkNotNull(announcement, "Announcement should not be null");
				this.announcement = announcement;
				return this;
			}

			/**
			 * True if the announcement should be displayed on
			 * the project’s overview page and false otherwise.
			 *
			 * @param showAnnouncement project show announcement flag, false by default
			 * @return ProjectUpdateBuilder – project builder object
			 */
			public ProjectUpdateBuilder showAnnouncement(boolean showAnnouncement) {
				this.showAnnouncement = showAnnouncement;
				return this;
			}

			/**
			 * @param suiteMode the suite mode of the project
			 * @return ProjectUpdateBuilder – project builder object
			 */
			public ProjectUpdateBuilder suiteMode(SuiteMode suiteMode) {
				Preconditions.checkNotNull(suiteMode, "Suite mode should not be null");
				this.suiteMode = suiteMode;
				return this;
			}

			/**
			 * Build ready project update object.
			 *
			 * @return ProjectUpdate – constructed project update object
			 */
			@SneakyThrows
			public ProjectUpdate build() {
				return BuilderUtils.fillWithBuilderFields(new ProjectUpdate(), this);
			}
		}
	}

	private static class SuiteModeSerializeConverter extends StdConverter<SuiteMode, Integer> {
		@Override
		public Integer convert(SuiteMode suiteMode) {
			return suiteMode.getID();
		}
	}

	private static class SuiteModeDeserializeConverter extends StdConverter<Integer, SuiteMode> {
		@Override
		public SuiteMode convert(Integer suiteModeID) {
			return SuiteMode.valueOfSuiteMode(suiteModeID);
		}
	}
}