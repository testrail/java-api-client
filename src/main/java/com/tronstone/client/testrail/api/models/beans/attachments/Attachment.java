
package com.tronstone.client.testrail.api.models.beans.attachments;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.TimeOnDeserializeConverter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.joda.time.DateTime;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Attachment {
    private String id;
    private String name;
    private int size;
    private int userID;
    private int clientID;
    private int projectID;
    private int planID;
    private int runID;
    private int legacyID;
    private String entityID;
    private String dataID;
    private String filename;
    private String filetype;
    private String icon;

    @JsonProperty(value = "is_image")
    private boolean image;

    @JsonDeserialize(converter = TimeOnDeserializeConverter.class)
    private DateTime createdOn;

    @JsonDeserialize(converter = EntityTypeDeserializeConverter.class)
    private EntityType entityType;

    public String getID() {
        return id;
    }

    private static class EntityTypeDeserializeConverter extends StdConverter<String, EntityType> {
        @Override
        public EntityType convert(String entityType) {
            return EntityType.valueOf(entityType.toUpperCase());
        }
    }
}
