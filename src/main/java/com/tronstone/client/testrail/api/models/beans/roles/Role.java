package com.tronstone.client.testrail.api.models.beans.roles;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Role {
    private int id;
    private String name;

    @JsonProperty("is_default")
    private @Getter(AccessLevel.NONE) boolean typeDefault;

    @JsonProperty("is_project_admin")
    private boolean projectAdmin;

    public int getID() {
        return id;
    }

    public boolean isDefault() {
        return typeDefault;
    }
}
