package com.tronstone.client.testrail.api.models.beans.plans;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;
import com.tronstone.client.inner.utils.BuilderUtils;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.ReferencesDeserializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.TimeOnDeserializeConverter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.tronstone.client.testrail.api.models.beans.runs.Run.CUSTOM_STATUS_COUNT_POSTFIX;

@Getter
@ToString
@EqualsAndHashCode
@JsonInclude(Include.NON_DEFAULT)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Plan {
    @JsonView({Plan.class, PlanUpdate.class})
    private String name;

    @JsonView({Plan.class, PlanUpdate.class})
    private int milestoneID;

    @JsonView({Plan.class, PlanUpdate.class})
    private String description;

    @JsonView(Plan.class)
    private final List<Entry> entries = new ArrayList<>();

    @JsonProperty(access = Access.WRITE_ONLY)
    private int id;

    @JsonProperty(access = Access.WRITE_ONLY)
    private String url;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int projectID;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int untestedCount;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int passedCount;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int retestCount;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int failedCount;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int blockedCount;

    @JsonProperty(value = "created_by", access = Access.WRITE_ONLY)
    private int creatorID;

    @JsonProperty(value = "assignedto_id", access = Access.WRITE_ONLY)
    private int assigneeID;

    @JsonProperty(value = "is_completed", access = Access.WRITE_ONLY)
    private boolean completed;

    @JsonProperty(access = Access.WRITE_ONLY)
    @JsonDeserialize(converter = TimeOnDeserializeConverter.class)
    private DateTime createdOn;

    @JsonProperty(access = Access.WRITE_ONLY)
    @JsonDeserialize(converter = TimeOnDeserializeConverter.class)
    private DateTime completedOn;

    @JsonProperty(access = Access.WRITE_ONLY)
    @JsonDeserialize(converter = ReferencesDeserializeConverter.class)
    private final List<String> refs = new ArrayList<>();

    @JsonIgnore
    private final @Getter(AccessLevel.NONE) Map<String, Integer> customStatuses = new HashMap<>();

    /**
     * Returns test plan object builder.
     *
     * @param name the name of the test plan
     * @return PlanBuilder – test plan builder object
     */
    public static PlanBuilder construct(String name) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
        return new PlanBuilder(name);
    }

    public int getID() {
        return id;
    }

    public String getURL() {
        return url;
    }

    /**
     * The number of tests in the test plan with the respective custom status.
     *
     * @param systemName the unique name of custom status in the database without '_count' postfix
     * @return int – the number of tests in the test plan
     */
    public int getCustomStatusCount(String systemName) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(systemName), "System name should not be empty or null");
        Preconditions.checkArgument(
                !systemName.endsWith(CUSTOM_STATUS_COUNT_POSTFIX),
                "System name should not have '%s' postfix",
                CUSTOM_STATUS_COUNT_POSTFIX
        );

        return customStatuses.get(systemName);
    }

    @JsonAnySetter
    private void deserializeCustomStatus(String statusSystemName, int count) {
        if (statusSystemName.endsWith(CUSTOM_STATUS_COUNT_POSTFIX)) {
            customStatuses.put(statusSystemName.replaceFirst(CUSTOM_STATUS_COUNT_POSTFIX, StringUtils.EMPTY), count);
        } else {
            throw new UnsupportedOperationException("Unrecognized json property: " + statusSystemName);
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public static class PlanBuilder {
        private @NonNull String name;
        private int milestoneID;
        private String description;
        private final List<Entry> entries = new ArrayList<>();

        /**
         * @param milestoneID the ID of the milestone to link to the test plan
         * @return PlanBuilder – test plan builder object
         */
        public PlanBuilder milestoneID(int milestoneID) {
            Preconditions.checkArgument(milestoneID > 0, "Milestone ID should be positive number");
            this.milestoneID = milestoneID;
            return this;
        }

        /**
         * @param description the name of the test plan
         * @return PlanBuilder – test plan builder object
         */
        public PlanBuilder description(String description) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(description), "Description should not be empty or null");
            this.description = description;
            return this;
        }

        /**
         * Add entry to entry list.
         *
         * @param entry new entry
         * @return PlanBuilder – test plan builder object
         */
        public PlanBuilder addEntry(Entry entry) {
            Preconditions.checkNotNull(entry, "Entry should not be null");
            this.entries.add(entry);
            return this;
        }

        /**
         * Build ready test plan object.
         *
         * @return Plan – constructed test plan object
         */
        public Plan build() {
            Plan plan = new Plan();

            plan.name = name;
            plan.milestoneID = milestoneID;
            plan.description = description;
            plan.entries.addAll(entries);

            return plan;
        }
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class PlanUpdate extends Plan {
        /**
         * Returns plan object builder for update operations.
         *
         * @return PlanUpdateBuilder – plan update builder object
         */
        public static PlanUpdateBuilder constructUpdate() {
            return new PlanUpdateBuilder();
        }

        @SuppressWarnings("FieldCanBeLocal")
        @NoArgsConstructor(access = AccessLevel.PRIVATE)
        public static class PlanUpdateBuilder {
            private String name;
            private int milestoneID;
            private String description;

            /**
             * @param name the name of the entry
             * @return PlanUpdateBuilder – test plan update builder object
             */
            public PlanUpdateBuilder name(String name) {
                Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
                this.name = name;
                return this;
            }

            /**
             * @param milestoneID the ID of the milestone to link to the test plan
             * @return PlanUpdateBuilder – test plan update builder object
             */
            public PlanUpdateBuilder milestoneID(int milestoneID) {
                Preconditions.checkArgument(milestoneID > 0, "Milestone ID should be positive number");
                this.milestoneID = milestoneID;
                return this;
            }

            /**
             * Use empty string for removing description.
             *
             * @param description the name of the test plan
             * @return PlanUpdateBuilder – test plan update builder object
             */
            public PlanUpdateBuilder description(String description) {
                Preconditions.checkNotNull(description, "Description should not be null");
                this.description = description;
                return this;
            }

            /**
             * Build ready test plan update object.
             *
             * @return PlanUpdate – constructed test plan update object
             */
            public PlanUpdate build() {
                return BuilderUtils.fillWithBuilderFields(new PlanUpdate(), this);
            }
        }
    }
}
