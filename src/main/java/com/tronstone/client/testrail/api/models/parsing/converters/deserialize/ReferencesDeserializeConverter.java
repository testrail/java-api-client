package com.tronstone.client.testrail.api.models.parsing.converters.deserialize;

import com.fasterxml.jackson.databind.util.StdConverter;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReferencesDeserializeConverter extends StdConverter<String, List<String>> {
    @Override
    public List<String> convert(String refs) {
        return Stream.of(refs.split(",")).map(String::trim).filter(StringUtils::isNotEmpty).collect(Collectors.toList());
    }
}
