package com.tronstone.client.testrail.api.models.beans.cases.history;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.CaseHistoryChangesTypeDeserializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.TimeOnDeserializeConverter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.joda.time.DateTime;

import java.util.List;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CaseHistory {
    private int id;
    private int userID;
    private List<String> comments;
    private List<CaseHistoryChanges> changes;

    @JsonDeserialize(converter = TimeOnDeserializeConverter.class)
    private DateTime createdOn;

    @JsonProperty("type_id")
    @JsonDeserialize(converter = CaseHistoryChangesTypeDeserializeConverter.class)
    private CaseHistoryChangesType changesType;
}
