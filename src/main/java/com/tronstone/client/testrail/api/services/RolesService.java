package com.tronstone.client.testrail.api.services;

import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.testrail.api.models.beans.roles.Role;
import com.tronstone.client.testrail.api.services.pagination.ServicePaginator;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;

/**
 * Use the following API methods to request details about roles.
 */
public class RolesService extends AbstractTestRailService {
    public RolesService(TestRailClientConfiguration configuration) {
        super(configuration);
    }

    /**
     * Returns a list of available roles.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/roles#getrole">Get Role</a>
     * @return ServicePaginator<Role> – data sets pagination service for roles
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_roles")
    public ServicePaginator<Role> getRoles() {
        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Role.class);
    }
}
