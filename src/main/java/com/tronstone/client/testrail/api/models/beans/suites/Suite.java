package com.tronstone.client.testrail.api.models.beans.suites;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;
import com.tronstone.client.inner.utils.BuilderUtils;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.TimeOnDeserializeConverter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

@Getter
@ToString
@EqualsAndHashCode
@JsonInclude(Include.NON_DEFAULT)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Suite {
	private String name;
	private String description;

	@JsonProperty(access = Access.WRITE_ONLY)
	private int id;

	@JsonProperty(access = Access.WRITE_ONLY)
	private String url;

	@JsonProperty(access = Access.WRITE_ONLY)
	private int projectID;

	@JsonProperty(access = Access.WRITE_ONLY)
	@JsonDeserialize(converter = TimeOnDeserializeConverter.class)
	private DateTime completedOn;

	@JsonProperty(value = "is_baseline", access = Access.WRITE_ONLY)
	private boolean baseline;

	@JsonProperty(value = "is_completed", access = Access.WRITE_ONLY)
	private boolean completed;

	@JsonProperty(value = "is_master", access = Access.WRITE_ONLY)
	private boolean master;

	/**
	 * Returns suite object builder.
	 *
	 * @param name the name of the test suite
	 * @return SuiteBuilder – suite builder object
	 */
	public static SuiteBuilder construct(String name) {
		Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
		return new SuiteBuilder(name);
	}

	public int getID() {
		return id;
	}

	public String getURL() {
		return url;
	}

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	public static class SuiteBuilder {
		private @NonNull String name;
		private String description;

		/**
		 * @param description the description of the test suite
		 * @return SuiteBuilder – suite builder object
		 */
		public SuiteBuilder description(String description) {
			Preconditions.checkArgument(StringUtils.isNotEmpty(description), "Description should not be empty or null");
			this.description = description;
			return this;
		}

		/**
		 * Build ready suite object.
		 *
		 * @return Suite – constructed suite object
		 */
		public Suite build() {
			Suite suite = new Suite();

			suite.name = name;
			suite.description = description;

			return suite;
		}
	}

	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static class SuiteUpdate extends Suite {
		/**
		 * Returns suite object builder for update operations.
		 *
		 * @return SuiteUpdateBuilder – suite update builder object
		 */
		public static SuiteUpdateBuilder constructUpdate() {
			return new SuiteUpdateBuilder();
		}

		@SuppressWarnings("FieldCanBeLocal")
		@NoArgsConstructor(access = AccessLevel.PRIVATE)
		public static class SuiteUpdateBuilder {
			private String name;
			private String description;

			/**
			 * @param name the name of the test suite
			 * @return SuiteUpdateBuilder – suite update builder object
			 */
			public SuiteUpdateBuilder name(String name) {
				Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
				this.name = name;
				return this;
			}

			/**
			 * Use empty string for removing description.
			 *
			 * @param description the description of the test suite
			 * @return SuiteUpdateBuilder – suite update builder object
			 */
			public SuiteUpdateBuilder description(String description) {
				Preconditions.checkNotNull(description, "Description should not be null");
				this.description = description;
				return this;
			}

			/**
			 * Build ready suite update object.
			 *
			 * @return SuiteUpdate – constructed suite update object
			 */
			@SneakyThrows
			public SuiteUpdate build() {
				return BuilderUtils.fillWithBuilderFields(new SuiteUpdate(), this);
			}
		}
	}
}