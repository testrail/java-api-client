package com.tronstone.client.testrail.api.models.beans.projects;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.WordUtils;

import java.util.Arrays;

@AllArgsConstructor
public enum SuiteMode {
    SINGLE_SUITE(1),
    SINGLE_SUITE_BASELINES(2),
    MULTIPLE_SUITES(3);

    private final int id;

    public int getID() {
        return id;
    }

    @Override
    public String toString() {
        return WordUtils.capitalizeFully(name().toLowerCase().replaceAll("_", StringUtils.SPACE));
    }

    public static SuiteMode valueOfSuiteMode(int suiteModeID) {
        return Arrays.stream(values())
                .filter(mode -> mode.id == suiteModeID)
                .findFirst()
                .orElseThrow(() -> new UnsupportedOperationException("Unsupported or non-existing project suite mode"));
    }
}
