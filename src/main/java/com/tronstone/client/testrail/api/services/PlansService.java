package com.tronstone.client.testrail.api.services;

import com.google.common.base.Preconditions;
import com.tronstone.client.inner.annotations.MapName;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.testrail.api.models.beans.plans.Entry;
import com.tronstone.client.testrail.api.models.beans.plans.Entry.EntryUpdate;
import com.tronstone.client.testrail.api.models.beans.plans.Entry.Run;
import com.tronstone.client.testrail.api.models.beans.plans.Entry.Run.RunUpdate;
import com.tronstone.client.testrail.api.models.beans.plans.Plan;
import com.tronstone.client.testrail.api.models.beans.plans.Plan.PlanUpdate;
import com.tronstone.client.testrail.api.parameters.PlansParametersProvider;
import com.tronstone.client.testrail.api.services.pagination.ServicePaginator;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.entity.ContentType;

/**
 * Use the following API methods to request details about test plans and to create or modify test plans.
 * In TestRail, test plans allow you to group multiple test runs together
 * and automatically generate test runs for various browser, OS,
 * or other configurations you set without having to add each test run individually.
 */
public class PlansService extends AbstractTestRailService {
    public PlansService(TestRailClientConfiguration configuration) {
        super(configuration);
    }

    /**
     * Returns an existing test plan.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/plans#getplan">Get Plan</a>
     * @param planID the ID of the test plan
     * @return Plan – object representation of the test plan
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_plan/{plan_id}")
    public Plan getPlan(@MapName("plan_id") int planID) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), Plan.class);
    }

    /**
     * Returns a list of test plans for a project.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/plans#getplans">Get Plans</a>
     * @param projectID the ID of the project
     * @return ServicePaginator<Plan> – data sets pagination service for plans
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_plans/{project_id}")
    public ServicePaginator<Plan> getPlans(@MapName("project_id") int projectID) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Plan.class);
    }

    /**
     * Returns a list of filtered test plans for a project.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/plans#getplans">Get Plans</a>
     * @param projectID the ID of the project
     * @param parametersProvider request filter parameters object
     * @return ServicePaginator<Plan> – data sets pagination service for plans
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_runs/{project_id}")
    public ServicePaginator<Plan> getPlans(@MapName("project_id") int projectID, PlansParametersProvider parametersProvider) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        Preconditions.checkNotNull(parametersProvider, "Plans parameters provider should not be null");

        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Plan.class);
    }

    /**
     * Creates a new test plan.
     *
     * @param projectID the ID of the project the test plan should be added to
     * @param plan new test plan
     * @return Plan – created test plan
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_plan/{project_id}")
    public Plan addPlan(@MapName("project_id") int projectID, Plan plan) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        Preconditions.checkNotNull(plan, "Plan should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writerWithView(Plan.class).withView(Entry.class).withView(Run.class).writeValueAsBytes(plan), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Plan.class
        );
    }

    /**
     * Adds one or more new test runs to a test plan.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/plans#addplanentry">Add Plan Entry</a>
     * @param planID the ID of the plan the test runs should be added to
     * @param entry new entry
     * @return Entry – created entry
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_plan_entry/{plan_id}")
    public Entry addPlanEntry(@MapName("plan_id") int planID, Entry entry) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");
        Preconditions.checkNotNull(entry, "Entry should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writerWithView(Entry.class).withView(Run.class).writeValueAsBytes(entry), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Entry.class
        );
    }

    /**
     * Add new entry run to a test plan entry.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/plans#addruntoplanentry">Add Run to Plan Entry</a>
     * @param planID the ID of the plan the test runs should be added to
     * @param entryID the ID of the test plan entry
     * @param entryRun new entry run
     * @return Plan – object representation of the test plan
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_run_to_plan_entry/{plan_id}/{entry_id}")
    public Plan addRunToPlanEntry(@MapName("plan_id") int planID, @MapName("entry_id") String entryID, Run entryRun) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");
        Preconditions.checkArgument(StringUtils.isNotEmpty(entryID), "Entry ID should not be empty or null");
        Preconditions.checkNotNull(entryRun, "Entry run should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writerWithView(Run.class).writeValueAsBytes(entryRun), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Plan.class
        );
    }

    /**
     * Updates an existing test plan (partial updates are supported,
     * i.e. you can submit and update specific fields only).
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/plans#updateplan">Update Plan</a>
     * @param planID the ID of the test plan
     * @param planUpdate test plan that to be updated
     * @return Plan – updated test plan
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "update_plan/{plan_id}")
    public Plan updatePlan(@MapName("plan_id") int planID, PlanUpdate planUpdate) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");
        Preconditions.checkNotNull(planUpdate, "Plan update should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writerWithView(PlanUpdate.class).withView(EntryUpdate.class).withView(RunUpdate.class).writeValueAsBytes(planUpdate), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Plan.class
        );
    }

    /**
     * Updates one or more groups of test runs in a plan (partial updates are supported,
     * i.e. you can submit and update specific fields only).
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/plans#updateplanentry">Update Plan Entry</a>
     * @param planID the ID of the test plan
     * @param entryID the ID of the test plan entry
     * @param entryUpdate entry that to be updated
     * @return Entry – updated entry
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "update_plan_entry/{plan_id}/{entry_id}")
    public Entry updatePlanEntry(@MapName("plan_id") int planID, @MapName("entry_id") String entryID, EntryUpdate entryUpdate) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");
        Preconditions.checkArgument(StringUtils.isNotEmpty(entryID), "Entry ID should not be empty or null");
        Preconditions.checkNotNull(entryUpdate, "Entry update should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writerWithView(EntryUpdate.class).withView(RunUpdate.class).writeValueAsBytes(entryUpdate), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Entry.class
        );
    }

    /**
     * Updates a run inside a plan entry that uses configurations.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/plans#updateruninplanentry">Update Run in Plan Entry</a>
     * @param entryRunID the ID of the test entry run
     * @param entryRunUpdate entry run that to be updated
     * @return Plan – updated entry run inside updated plan
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "update_run_in_plan_entry/{run_id}")
    public Plan updateRunInPlanEntry(@MapName("run_id") int entryRunID, RunUpdate entryRunUpdate) {
        Preconditions.checkArgument(entryRunID > 0, "Entry run ID should be positive number");
        Preconditions.checkNotNull(entryRunUpdate, "Entry run update should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writerWithView(RunUpdate.class).writeValueAsBytes(entryRunUpdate), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Plan.class
        );
    }

    /**
     * Closes an existing test plan and archives its test runs & results.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/plans#closeplan">Close Plan</a>
     * @param planID the ID of the test plan
     * @return Plan – closed test plan
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "close_plan/{plan_id}")
    public Plan closePlan(@MapName("plan_id") int planID) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString())
                        .execute()
                        .returnContent()
                        .asString(),
                Plan.class
        );
    }

    /**
     * Deletes an existing test plan.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/plans#deleteplan">Delete Plan</a>
     * @param planID the ID of the test plan
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "delete_plan/{plan_id}")
    public void deletePlan(@MapName("plan_id") int planID) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");
        getRequester().prepareRequest().addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType()).execute();
    }

    /**
     * Deletes one or more existing test runs from a plan.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/plans#deleteplanentry">Delete Plan Entry</a>
     * @param planID the ID of the test plan
     * @param entryID the ID of the test plan entry
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "delete_plan_entry/{plan_id}/{entry_id}")
    public void deletePlanEntry(@MapName("plan_id") int planID, @MapName("entry_id") String entryID) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");
        Preconditions.checkArgument(StringUtils.isNotEmpty(entryID), "Entry ID should not be empty or null");

        getRequester().prepareRequest().addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType()).execute();
    }

    /**
     * Deletes a test run from a test plan entry.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/plans#deleterunfromplanentry">Delete Plan from Plan Entry</a>
     * @param planID the ID of the test plan
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "delete_run_from_plan_entry/{run_id}")
    public void deleteRunFromPlanEntry(@MapName("plan_id") int planID) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");
        getRequester().prepareRequest().addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType()).execute();
    }
}
