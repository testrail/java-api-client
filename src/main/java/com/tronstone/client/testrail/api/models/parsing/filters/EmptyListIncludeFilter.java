package com.tronstone.client.testrail.api.models.parsing.filters;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class EmptyListIncludeFilter {
    @Override
    public boolean equals(Object object) {
        if (Objects.isNull(object) || !(object instanceof List)) {
            return true;
        }

        if (Collections.EMPTY_LIST.getClass().isAssignableFrom(object.getClass())) {
            return false;
        } else {
            return ((List<?>) object).isEmpty();
        }
    }
}
