package com.tronstone.client.testrail.api.models.beans.steps;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.ReferencesDeserializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.serialize.ReferencesSerializeConverter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ToString
@Setter @Getter
@EqualsAndHashCode
@JsonInclude(Include.NON_DEFAULT)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ResultStep {
    private String content;
    private String additionalInfo;
    private String expected;
    private String actual;
    private int statusID;

    @JsonSerialize(converter = ReferencesSerializeConverter.class)
    @JsonDeserialize(converter = ReferencesDeserializeConverter.class)
    private final @Setter(AccessLevel.NONE) List<String> refs = new ArrayList<>();

    /**
     * Returns result step object builder.
     *
     * @return ResultStepBuilder – result step builder object
     */
    public static ResultStepBuilder construct() {
        return new ResultStepBuilder();
    }

    /**
     * Returns list of result steps.
     *
     * @param resultSteps result steps array
     * @return List<ResultStep> – list of result steps
     */
    public static List<ResultStep> createSteps(ResultStep... resultSteps) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(resultSteps), "Result steps should not be empty or null");
        return Arrays.asList(resultSteps);
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class ResultStepBuilder {
        private String content;
        private String additionalInfo;
        private String expected;
        private String actual;
        private int statusID;
        private final List<String> refs = new ArrayList<>();

        /**
         * The text contents of the “Step” field.
         *
         * @param content text value
         * @return ResultStepBuilder – result step builder object
         */
        public ResultStepBuilder content(String content) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(content), "Content should not be empty or null");
            this.content = content;
            return this;
        }

        /**
         * The text contents of the “Additional Info” field.
         *
         * @param additionalInfo text value
         * @return ResultStepBuilder – result step builder object
         */
        public ResultStepBuilder additionalInfo(String additionalInfo) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(additionalInfo), "Additional info should not be empty or null");
            this.additionalInfo = additionalInfo;
            return this;
        }

        /**
         * The text contents of the “Expected Result” field.
         *
         * @param expected text value
         * @return ResultStepBuilder – result step builder object
         */
        public ResultStepBuilder expected(String expected) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(expected), "Expected should not be empty or null");
            this.expected = expected;
            return this;
        }

        /**
         * The text contents of the “Actual Result” field.
         *
         * @param actual text value
         * @return ResultStepBuilder – result step builder object
         */
        public ResultStepBuilder actual(String actual) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(actual), "Actual should not be empty or null");
            this.actual = actual;
            return this;
        }

        /**
         * The ID of the test status.
         *
         * @param statusID The ID of the result step status
         * @return ResultStepBuilder – result step builder object
         */
        public ResultStepBuilder statusID(int statusID) {
            Preconditions.checkArgument(statusID > 0, "Status ID should be positive number");
            this.statusID = statusID;
            return this;
        }

        /**
         * Add the list of references/requirements.
         *
         * @param refs list of references/requirements
         * @return ResultStepBuilder – result step builder object
         */
        public ResultStepBuilder refs(List<String> refs) {
            Preconditions.checkArgument(CollectionUtils.isNotEmpty(refs), "References should not be empty or null");
            this.refs.addAll(refs);
            return this;
        }

        /**
         * Parse and add list of comma-separated references/requirements.
         *
         * @param refs a comma-separated list of references/requirements
         * @return ResultStepBuilder – result step builder object
         */
        public ResultStepBuilder refs(String refs) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(refs), "References should not be empty or null");
            this.refs.addAll(new ReferencesDeserializeConverter().convert(refs));
            return this;
        }

        /**
         * Add the array of references/requirements.
         *
         * @param refs array of references/requirements
         * @return ResultStepBuilder – result step builder object
         */
        public ResultStepBuilder refs(String... refs) {
            Preconditions.checkArgument(ArrayUtils.isNotEmpty(refs), "References should not be empty or null");

            this.refs.addAll(Arrays.stream(refs)
                    .peek(ref -> Preconditions.checkArgument(StringUtils.isNotEmpty(ref), "Reference should not be empty or null"))
                    .collect(Collectors.toList()));

            return this;
        }

        /**
         * Build ready result step object.
         *
         * @return ResultStep – constructed result step object
         */
        public ResultStep build() {
            ResultStep resultStep = new ResultStep();

            resultStep.content = content;
            resultStep.additionalInfo = additionalInfo;
            resultStep.expected = expected;
            resultStep.actual = actual;
            resultStep.statusID = statusID;
            resultStep.refs.addAll(refs);

            return resultStep;
        }
    }
}
