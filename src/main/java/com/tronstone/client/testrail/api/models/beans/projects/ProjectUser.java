package com.tronstone.client.testrail.api.models.beans.projects;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProjectUser {
    private int userID;
    private int globalRoleID;
    private int projectRoleID;

    @JsonProperty("global_role")
    private String globalRoleName;

    @JsonProperty("project_role")
    private String projectRoleName;
}
