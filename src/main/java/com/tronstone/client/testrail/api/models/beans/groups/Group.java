package com.tronstone.client.testrail.api.models.beans.groups;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Group {
    private int id;
    private String name;
    private List<Integer> userIDs;

    public int getID() {
        return id;
    }
}
