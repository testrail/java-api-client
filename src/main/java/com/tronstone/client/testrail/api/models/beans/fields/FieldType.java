package com.tronstone.client.testrail.api.models.beans.fields;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.tronstone.client.testrail.api.models.beans.steps.ResultStep;
import com.tronstone.client.testrail.api.models.beans.steps.Step;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Lombok;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.util.List;

/**
 * The following custom field types are supported:
 * <pre>
 *      STRING – java.lang.String
 *      INTEGER – java.lang.Integer
 *      TEXT – java.lang.String
 *      URL – java.lang.String
 *      CHECKBOX – java.lang.Boolean
 *      DROPDOWN – java.lang.Integer
 *      USER – java.lang.Integer
 *      DATE – java.lang.String
 *      MILESTONE – java.lang.Integer
 *      STEPS – java.util.List<Step>
 *      STEP_RESULTS – java.util.List<StepResult>
 *      MULTI_SELECT – java.util.List<String>
 * </pre>
 */
@EqualsAndHashCode
public class FieldType<T> {
    public static final FieldType<String> STRING = new FieldType<>(1, "String", new TypeReference<String>() { });
    public static final FieldType<Integer> INTEGER = new FieldType<>(2, "Integer", new TypeReference<Integer>() { });
    public static final FieldType<String> TEXT = new FieldType<>(3, "Text", new TypeReference<String>() { });
    public static final FieldType<String> URL = new FieldType<>(4, "URL", new TypeReference<String>() { });
    public static final FieldType<Boolean> CHECKBOX = new FieldType<>(5, "Checkbox", new TypeReference<Boolean>() { });
    public static final FieldType<Integer> DROPDOWN = new FieldType<>(6, "Dropdown", new TypeReference<Integer>() { });
    public static final FieldType<Integer> USER = new FieldType<>(7, "User", new TypeReference<Integer>() { });
    public static final FieldType<String> DATE = new FieldType<>(8, "Date", new TypeReference<String>() { });
    public static final FieldType<Integer> MILESTONE = new FieldType<>(9, "Milestone", new TypeReference<Integer>() { });
    public static final FieldType<List<Step>> STEPS = new FieldType<>(10, "Steps", new TypeReference<List<Step>>() { });
    public static final FieldType<List<ResultStep>> STEP_RESULTS = new FieldType<>(11, "StepResults", new TypeReference<List<ResultStep>>() { });
    public static final FieldType<List<Integer>> MULTI_SELECT = new FieldType<>(12, "MultiSelect", new TypeReference<List<Integer>>() { });

    private final TypeFactory typeFactory = TypeFactory.defaultInstance();

    private final @Getter int id;
    private final @Getter String name;
    private final @Getter JavaType javaType;

    private FieldType(int id, String name, TypeReference<T> typeReference) {
        this.id = id;
        this.name = name;
        javaType = typeFactory.constructType(typeReference);
    }

    @Override
    public String toString() {
        return name + "(id=" + id + ", javaType=" + javaType.toString() + ")";
    }

    @SuppressWarnings("unchecked")
    public static <T> FieldType<T> valueOf(int fieldTypeID) {
        Class<?> fieldTypeClass = FieldType.class;
        return FieldUtils.getAllFieldsList(fieldTypeClass)
                .stream()
                .filter(field -> fieldTypeClass.isAssignableFrom(field.getType()))
                .map(field -> {
                    try {
                        return (FieldType<T>) FieldUtils.readStaticField(field);
                    } catch (IllegalAccessException ex) {
                        throw Lombok.sneakyThrow(ex);
                    }
                }).filter(type -> type.id == fieldTypeID)
                .findFirst()
                .orElseThrow(() -> new UnsupportedOperationException("Unsupported or non-existing field type"));
    }
}
