package com.tronstone.client.testrail.api.models.beans.tests;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;
import com.tronstone.client.testrail.api.models.beans.fields.CustomField;
import com.tronstone.client.testrail.api.models.beans.fields.FieldType;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.EstimateDeserializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.ReferencesDeserializeConverter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.Period;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import static com.tronstone.client.testrail.api.models.beans.fields.CustomField.CUSTOM_FIELD_SYSTEM_PREFIX;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Test {
	private int id;
	private String title;
	private int sectionsDisplayOrder;
	private int casesDisplayOrder;
	private List<String> caseComments;
	private int runID;
	private int caseID;
	private int templateID;
	private int milestoneID;
	private int typeID;
	private int priorityID;
	private int statusID;

	@JsonDeserialize(converter = EstimateDeserializeConverter.class)
	private Period estimate;

	@JsonDeserialize(converter = EstimateDeserializeConverter.class)
	private Period estimateForecast;

	@JsonDeserialize(converter = ReferencesDeserializeConverter.class)
	private List<String> refs;

	@JsonProperty("assignedto_id")
	private int assigneeID;

	@JsonIgnore
	private final @Getter(AccessLevel.NONE) Map<String, CustomField> customFields = new HashMap<>();

	public int getID() {
		return id;
	}

	/**
	 * To get the value with correct type use {@link FieldType} mapped types.
	 * Warning, label it is not unique value, if two custom field will
	 * have the same value, will return first available. For more consistent
	 * results use {@link Test#getCustomField(String, FieldType)}
	 *
	 * @param caseFieldType the custom field type
	 * @param label the label of the field as it appears in the user interface
	 * @param <T> type of custom field value
	 * @return T – the value of the custom field or null if value not exists
	 */
	@SuppressWarnings("unchecked")
	public <T> T getCustomField(FieldType<T> caseFieldType, String label) {
		Preconditions.checkArgument(StringUtils.isNotEmpty(label), "Label should not be empty or null");

		CustomField customField = customFields.values()
				.stream()
				.filter(field -> field.getLabel().equalsIgnoreCase(label))
				.findFirst()
				.orElseThrow(() -> new NoSuchElementException("The label does not match any of the existing custom fields"));

		Preconditions.checkArgument(
				customField.getFieldType().equals(caseFieldType),
				String.format(
						"Case field type should be the same as type with containing field: expected type '%s' but actual '%s'",
						caseFieldType.getName(),
						customField.getFieldType().getName()
				)
		);

		return (T) customField.getFieldValue();
	}

	/**
	 * To get the value with correct type use {@link FieldType} mapped types.
	 *
	 * @param systemName the unique name of custom case field in the database without 'custom_' prefix
	 * @param caseFieldType the custom field type
	 * @param <T> – type of custom field value
	 * @return T – the value of the custom field or null if value not exists
	 */
	@SuppressWarnings("unchecked")
	public <T> T getCustomField(String systemName, FieldType<T> caseFieldType) {
		Preconditions.checkArgument(StringUtils.isNotEmpty(systemName), "System name should not be empty or null");
		Preconditions.checkArgument(
				!systemName.startsWith(CUSTOM_FIELD_SYSTEM_PREFIX),
				"System name should not have '%s' prefix",
				CUSTOM_FIELD_SYSTEM_PREFIX
		);

		CustomField customField = customFields.get(systemName);
		Preconditions.checkArgument(
				customField.getFieldType().equals(caseFieldType),
				String.format(
						"Case field type should be the same as type with containing field: expected type '%s' but actual '%s'",
						caseFieldType.getName(),
						customField.getFieldType().getName()
				)
		);

		return (T) customField.getFieldValue();
	}

	@JsonAnySetter
	private void deserializeCustomField(String fieldSystemName, CustomField customField) {
		if (fieldSystemName.startsWith(CUSTOM_FIELD_SYSTEM_PREFIX)) {
			customFields.put(fieldSystemName.replaceFirst(CUSTOM_FIELD_SYSTEM_PREFIX, StringUtils.EMPTY), customField);
		} else {
			throw new UnsupportedOperationException("Unrecognized json property: " + fieldSystemName);
		}
	}
}