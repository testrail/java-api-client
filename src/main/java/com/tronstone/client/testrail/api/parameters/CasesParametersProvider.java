package com.tronstone.client.testrail.api.parameters;

import com.google.common.base.Preconditions;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.concurrent.TimeUnit;

@NoArgsConstructor(staticName = "prepare")
public class CasesParametersProvider extends PaginationParametersProvider<CasesParametersProvider> {
    private final StringBuilder parametersBuilder = getParametersBuilder();

    /**
     * The ID of the test suite (optional if the project is operating in single suite mode).
     *
     * @param suiteID the ID of the test suite
     * @return CaseParametersProvider – provider object
     */
    public CasesParametersProvider suiteID(int suiteID) {
        Preconditions.checkArgument(suiteID > 0, "Suite ID should be positive number");
        parametersBuilder.append("&suite_id=").append(suiteID);
        return this;
    }

    /**
     * Only return test cases created before this date.
     *
     * @param createdBefore time that will be converted to UNIX timestamp
     * @return CaseParametersProvider – provider object
     */
    public CasesParametersProvider createdBefore(DateTime createdBefore) {
        Preconditions.checkNotNull(createdBefore, "Created before should not be null");
        parametersBuilder.append("&created_before=").append(TimeUnit.MILLISECONDS.toSeconds(createdBefore.getMillis()));
        return this;
    }

    /**
     * Only return test cases created after this date.
     *
     * @param createdAfter time that will be converted to UNIX timestamp
     * @return CaseParametersProvider – provider object
     */
    public CasesParametersProvider createdAfter(DateTime createdAfter) {
        Preconditions.checkNotNull(createdAfter, "Created after should not be null");
        parametersBuilder.append("&created_after=").append(TimeUnit.MILLISECONDS.toSeconds(createdAfter.getMillis()));
        return this;
    }

    /**
     * Only return cases with matching filter string in the case title.
     *
     * @param filter case title filter
     * @return CaseParametersProvider – provider object
     */
    public CasesParametersProvider filter(String filter) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(filter), "Filter should not be empty or null");
        parametersBuilder.append("&filter=").append(filter);
        return this;
    }

    /**
     * A comma-separated list of milestone ID's
     * to filter by (not available if the milestone field is disabled for the project).
     *
     * @param milestoneIDs array of milestone ID's
     * @return CaseParametersProvider – provider object
     */
    public CasesParametersProvider milestoneID(int... milestoneIDs) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(milestoneIDs), "Milestone ID's should not be empty or null");
        parametersBuilder.append("&milestone_id=").append(StringUtils.join(milestoneIDs, ','));
        return this;
    }

    /**
     * A comma-separated list of priority ID's to filter by.
     *
     * @param priorityIDs array of priority ID's
     * @return CaseParametersProvider – provider object
     */
    public CasesParametersProvider priorityID(int... priorityIDs) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(priorityIDs), "Priority ID's should not be empty or null");
        parametersBuilder.append("&priority_id=").append(StringUtils.join(priorityIDs, ','));
        return this;
    }

    /**
     * A single Reference ID (e.g. TR-1, 4291, etc.).
     *
     * @param ref single reference ID
     * @return CaseParametersProvider – provider object
     */
    public CasesParametersProvider refs(String ref) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(ref), "Reference should not be empty or null");
        parametersBuilder.append("&refs=").append(ref);
        return this;
    }

    /**
     * The ID of a test case section.
     *
     * @param sectionID the ID of the test case section
     * @return CaseParametersProvider – provider object
     */
    public CasesParametersProvider sectionID(int sectionID) {
        Preconditions.checkArgument(sectionID > 0, "Section ID should be positive number");
        parametersBuilder.append("&section_id=").append(sectionID);
        return this;
    }

    /**
     * A comma-separated list of template ID's to filter by.
     *
     * @param templateIDs array of template ID's
     * @return CaseParametersProvider – provider object
     */
    public CasesParametersProvider templateID(int... templateIDs) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(templateIDs), "Template ID's should not be empty or null");
        parametersBuilder.append("&template_id=").append(StringUtils.join(templateIDs, ','));
        return this;
    }

    /**
     * A comma-separated list of case type ID's to filter by.
     *
     * @param typeIDs array of type ID's
     * @return CaseParametersProvider – provider object
     */
    public CasesParametersProvider typeID(int... typeIDs) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(typeIDs), "Type ID's should not be empty or null");
        parametersBuilder.append("&type_id=").append(StringUtils.join(typeIDs, ','));
        return this;
    }

    /**
     * Only return test cases updated before this date.
     *
     * @param updatedBefore time that will be converted to UNIX timestamp
     * @return CaseParametersProvider – provider object
     */
    public CasesParametersProvider updatedBefore(DateTime updatedBefore) {
        Preconditions.checkNotNull(updatedBefore, "Updated before should not be null");
        parametersBuilder.append("&updated_before=").append(TimeUnit.MILLISECONDS.toSeconds(updatedBefore.getMillis()));
        return this;
    }

    /**
     * Only return test cases updated after this date.
     *
     * @param updatedAfter time that will be converted to UNIX timestamp
     * @return CaseParametersProvider – provider object
     */
    public CasesParametersProvider updatedAfter(DateTime updatedAfter) {
        Preconditions.checkNotNull(updatedAfter, "Updated after should not be null");
        parametersBuilder.append("&updated_after=").append(TimeUnit.MILLISECONDS.toSeconds(updatedAfter.getMillis()));
        return this;
    }

    /**
     * A comma-separated list of user ID's who updated test cases to filter by.
     *
     * @param userIDs array of user ID's
     * @return CaseParametersProvider – provider object
     */
    public CasesParametersProvider updatedBy(int... userIDs) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(userIDs), "User ID's should not be empty or null");
        parametersBuilder.append("&updated_by=").append(StringUtils.join(userIDs, ','));
        return this;
    }
}
