package com.tronstone.client.testrail.api.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.testrail.api.models.beans.priorities.Priority;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.SneakyThrows;

import java.util.List;

/**
 * Use the following API methods to request details about the priorities field on test cases.
 */
public class PrioritiesService extends AbstractTestRailService {
    public PrioritiesService(TestRailClientConfiguration configuration) {
        super(configuration);
    }

    /**
     * Returns a list of available test case priorities.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/priorities#getpriorities">Get Priorities</a>
     * @return List<Priority> – list of case priorities
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_priorities")
    public List<Priority> getPriorities() {
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), new TypeReference<List<Priority>>() { });
    }
}
