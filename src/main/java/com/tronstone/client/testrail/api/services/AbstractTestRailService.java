package com.tronstone.client.testrail.api.services;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.tronstone.client.inner.annotations.AutomaticAuthentication;
import com.tronstone.client.inner.annotations.ServiceURI;
import com.tronstone.client.inner.service.BasicService;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import com.tronstone.client.testrail.processing.ResponseErrorHandler;
import lombok.AccessLevel;
import lombok.Getter;

import static com.tronstone.client.testrail.api.services.AbstractTestRailService.SERVICE_PREFIX;

@AutomaticAuthentication
@Getter(AccessLevel.PROTECTED)
@ServiceURI("index.php?" + SERVICE_PREFIX)
public abstract class AbstractTestRailService extends BasicService {
    public static final String SERVICE_PREFIX = "/api/v2/";

    private ObjectMapper objectMapper = new ObjectMapper();
    private final TestRailClientConfiguration configuration;

    protected AbstractTestRailService(TestRailClientConfiguration configuration) {
        super(configuration.getInstanceURI(), configuration.getAuthentication(), new ResponseErrorHandler());

        this.configuration = configuration;
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategies.SNAKE_CASE);
        objectMapper.setVisibility(objectMapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(Visibility.ANY)
                .withIsGetterVisibility(Visibility.NONE)
                .withGetterVisibility(Visibility.NONE)
        );
    }

    protected AbstractTestRailService(TestRailClientConfiguration configuration, ObjectMapper objectMapper) {
        this(configuration);
        this.objectMapper = objectMapper;
    }
}
