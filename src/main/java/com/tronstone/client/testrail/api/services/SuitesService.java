package com.tronstone.client.testrail.api.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Preconditions;
import com.tronstone.client.inner.annotations.MapName;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.testrail.api.models.beans.deletion.DeletionData;
import com.tronstone.client.testrail.api.models.beans.suites.Suite;
import com.tronstone.client.testrail.api.models.beans.suites.Suite.SuiteUpdate;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.SneakyThrows;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.http.entity.ContentType;

import java.util.List;

/**
 * Use the following API methods to request details about test suites and to create or modify test suites.
 */
public class SuitesService extends AbstractTestRailService {
    public SuitesService(TestRailClientConfiguration configuration) {
        super(configuration);
    }

    /**
     * Returns an existing test suite.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/suites#getsuite">Get Suite</a>
     * @param suiteID the ID of the test suite
     * @return Suite – object representation of the suite
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_suite/{suite_id}")
    public Suite getSuite(@MapName("suite_id") int suiteID) {
        Preconditions.checkArgument(suiteID > 0, "Suite ID should be positive number");
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), Suite.class);
    }

    /**
     * Returns a list of test suites for a project.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/suites#getsuites">Get Suites</a>
     * @param projectID the ID of the project
     * @return List<Suite> – list of suites
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_suites/{project_id}")
    public List<Suite> getSuites(@MapName("project_id") int projectID) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), new TypeReference<List<Suite>>() { });
    }

    /**
     * Creates a new test suite.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/suites#addsuite">Add Suite</a>
     * @param projectID the ID of the project the test suite should be added to
     * @param suite new suite
     * @return Suite – created suite
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_suite/{project_id}")
    public Suite addSuite(@MapName("project_id") int projectID, Suite suite) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        Preconditions.checkNotNull(suite, "Suite should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writeValueAsBytes(suite), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Suite.class
        );
    }

    /**
     * Updates an existing test suite (partial updates are supported,
     * i.e. you can submit and update specific fields only).
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/suites#updatesuite">Update Suite</a>
     * @param suiteID the ID of the test suite
     * @param suiteUpdate suite that to be updated
     * @return Suite – updated suite
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "update_suite/{suite_id}")
    public Suite updateSuite(@MapName("suite_id") int suiteID, SuiteUpdate suiteUpdate) {
        Preconditions.checkArgument(suiteID > 0, "Suite ID should be positive number");
        Preconditions.checkNotNull(suiteUpdate, "Suite update should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writeValueAsBytes(suiteUpdate), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Suite.class
        );
    }

    /**
     * Deletes an existing test suite.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/suites#deletesuite">Delete Suite</a>
     * @param suiteID the ID of the test suite
     * @param soft when true will return information about the data which will be deleted but will not proceed with the deletion
     * @return DeletionData – data on the number of affected set when soft is true, in deletion case return null
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "delete_suite/{suite_id}")
    public DeletionData deleteSuite(@MapName("suite_id") int suiteID, boolean soft) {
        Preconditions.checkArgument(suiteID > 0, "Suite ID should be positive number");

        ObjectNode deleteSuiteNode = getObjectMapper().createObjectNode();
        deleteSuiteNode.put("soft", BooleanUtils.toInteger(soft));

        JsonNode responseNode = getObjectMapper().readTree(getRequester().prepareRequest()
                .bodyByteArray(getObjectMapper().writeValueAsBytes(deleteSuiteNode), ContentType.APPLICATION_JSON)
                .execute()
                .returnContent()
                .asString()
        );

        if (responseNode.isEmpty()) {
            return null;
        }

        return getObjectMapper().treeToValue(responseNode, DeletionData.class);
    }

    /**
     * Deletes an existing test suite.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/suites#deletesuite">Delete Suite</a>
     * @param suiteID the ID of the test suite
     */
    public void deleteSuite(int suiteID) {
        deleteSuite(suiteID, false);
    }
}
