package com.tronstone.client.testrail.api.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.testrail.api.models.beans.cases.types.CaseType;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.SneakyThrows;

import java.util.List;

/**
 * Use the following API methods to request details about case type.
 */
public class CaseTypesService extends AbstractTestRailService {
    public CaseTypesService(TestRailClientConfiguration configuration) {
        super(configuration);
    }

    /**
     * Returns a list of available case types.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/case-types#getcasetypes">Get Case Types</a>
     * @return List<CaseType> – list of case types
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_case_types")
    public List<CaseType> getCaseTypes() {
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), new TypeReference<List<CaseType>>() { });
    }
}

