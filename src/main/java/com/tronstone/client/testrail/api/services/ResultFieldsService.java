package com.tronstone.client.testrail.api.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.testrail.api.models.beans.fields.Field;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.SneakyThrows;

import java.util.List;

/**
 * Use the following API methods to request details about custom fields for test results.
 */
public class ResultFieldsService extends AbstractTestRailService {
    public ResultFieldsService(TestRailClientConfiguration configuration) {
        super(configuration);
    }

    /**
     * Returns a list of available test result custom fields.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/result-fields#getresultfields">Get Result Fields</a>
     * @return List<Field> – list of result fields
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_result_fields")
    public List<Field> getResultFields() {
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), new TypeReference<List<Field>>() { });
    }
}
