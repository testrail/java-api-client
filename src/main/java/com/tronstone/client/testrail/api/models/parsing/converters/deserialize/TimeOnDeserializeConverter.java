package com.tronstone.client.testrail.api.models.parsing.converters.deserialize;

import com.fasterxml.jackson.databind.util.StdConverter;
import org.joda.time.DateTime;

import java.util.concurrent.TimeUnit;

public class TimeOnDeserializeConverter extends StdConverter<Long, DateTime> {
    @Override
    public DateTime convert(Long timeOn) {
        return new DateTime(TimeUnit.SECONDS.toMillis(timeOn));
    }
}
