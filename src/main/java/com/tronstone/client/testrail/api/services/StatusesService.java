package com.tronstone.client.testrail.api.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.testrail.api.models.beans.statuses.CaseStatus;
import com.tronstone.client.testrail.api.models.beans.statuses.Status;
import com.tronstone.client.testrail.api.services.pagination.ServicePaginator;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.SneakyThrows;

import java.util.List;

/**
 * Use the following API methods to request details about test statuses.
 */
public class StatusesService extends AbstractTestRailService {
    public StatusesService(TestRailClientConfiguration configuration) {
        super(configuration);
    }

    /**
     * Returns a list of available test case statuses.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/statuses#getcasestatuses">Get Case Statuses</a>
     * @return ServicePaginator<CaseStatus> – data sets pagination service for case statuses
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_case_statuses")
    public ServicePaginator<CaseStatus> getCaseStatuses() {
        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), CaseStatus.class);
    }

    /**
     * Returns a list of available test statuses.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/statuses#getstatuses">Get Statuses</a>
     * @return List<Status> – list of statuses
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_statuses")
    public List<Status> getStatuses() {
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), new TypeReference<List<Status>>() { });
    }
}
