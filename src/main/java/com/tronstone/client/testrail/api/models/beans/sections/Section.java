package com.tronstone.client.testrail.api.models.beans.sections;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.google.common.base.Preconditions;
import com.tronstone.client.inner.utils.BuilderUtils;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Getter
@ToString
@EqualsAndHashCode
@JsonInclude(Include.NON_DEFAULT)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Section {
	private String name;
	private String description;
	private int suiteID;
	private int parentID;

	@JsonProperty(access = Access.WRITE_ONLY)
	private int id;

	@JsonProperty(access = Access.WRITE_ONLY)
	private int depth;

	@JsonProperty(access = Access.WRITE_ONLY)
	private int displayOrder;

	/**
	 * Returns section object builder.
	 *
	 * @param name the name of the section
	 * @return SectionBuilder – section builder object
	 */
	public static SectionBuilder construct(String name) {
		Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
		return new SectionBuilder(name);
	}

	public int getID() {
		return id;
	}

	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	public static class SectionBuilder {
		private @NonNull String name;
		private String description;
		private int suiteID;
		private int parentID;

		/**
		 * @param description the description of the section
		 * @return SectionBuilder – section builder object
		 */
		public SectionBuilder description(String description) {
			Preconditions.checkArgument(StringUtils.isNotEmpty(description), "Description should not be empty or null");
			this.description = description;
			return this;
		}

		/**
		 * The ID of the test suite (ignored if the project is operating
		 * in single suite mode, required otherwise).
		 *
		 * @param suiteID the ID of the test suite
		 * @return SectionBuilder – section builder object
		 */
		public SectionBuilder suiteID(int suiteID) {
			Preconditions.checkArgument(suiteID > 0, "Suite ID should be positive number");
			this.suiteID = suiteID;
			return this;
		}

		/**
		 * @param parentID the ID of the parent section (to build section hierarchies)
		 * @return SectionBuilder – section builder object
		 */
		public SectionBuilder parentID(int parentID) {
			Preconditions.checkArgument(parentID > 0, "Parent ID should be positive number");
			this.parentID = parentID;
			return this;
		}

		/**
		 * Build ready section object.
		 *
		 * @return Section – constructed section object
		 */
		public Section build() {
			Section section = new Section();

			section.name = name;
			section.description = description;
			section.suiteID = suiteID;
			section.parentID = parentID;

			return section;
		}
	}

	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static class SectionUpdate extends Section {
		/**
		 * Returns section object builder for update operations.
		 *
		 * @return SectionUpdateBuilder – section update builder object
		 */
		public static SectionUpdateBuilder constructUpdate() {
			return new SectionUpdateBuilder();
		}

		@SuppressWarnings("FieldCanBeLocal")
		@NoArgsConstructor(access = AccessLevel.PRIVATE)
		public static class SectionUpdateBuilder {
			private String name;
			private String description;

			/**
			 * @param name the name of the section
			 * @return SectionUpdateBuilder – section update builder object
			 */
			public SectionUpdateBuilder name(String name) {
				Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
				this.name = name;
				return this;
			}

			/**
			 * Use empty string for removing description.
			 *
			 * @param description the description of the section
			 * @return SectionUpdateBuilder – section update builder object
			 */
			public SectionUpdateBuilder description(String description) {
				Preconditions.checkNotNull(description, "Description should not be null");
				this.description = description;
				return this;
			}

			/**
			 * Build ready section update object.
			 *
			 * @return Section – constructed section update object
			 */
			@SneakyThrows
			public SectionUpdate build() {
				return BuilderUtils.fillWithBuilderFields(new SectionUpdate(), this);
			}
		}
	}
}