package com.tronstone.client.testrail.api.parameters;

import com.google.common.base.Preconditions;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.concurrent.TimeUnit;

@NoArgsConstructor(staticName = "prepare")
public class RunsParametersProvider extends PaginationParametersProvider<RunsParametersProvider>  {
    private final StringBuilder parametersBuilder = getParametersBuilder();

    /**
     * Only return test runs created after this date.
     *
     * @param createdAfter time that will be converted to UNIX timestamp
     * @return RunsParametersProvider – provider object
     */
    public RunsParametersProvider createdAfter(DateTime createdAfter) {
        Preconditions.checkNotNull(createdAfter, "Created after should not be null");
        parametersBuilder.append("&created_after=").append(TimeUnit.MILLISECONDS.toSeconds(createdAfter.getMillis()));
        return this;
    }

    /**
     * Only return test runs created before this date.
     *
     * @param createdBefore time that will be converted to UNIX timestamp
     * @return RunsParametersProvider – provider object
     */
    public RunsParametersProvider createdBefore(DateTime createdBefore) {
        Preconditions.checkNotNull(createdBefore, "Created before should not be null");
        parametersBuilder.append("&created_before=").append(TimeUnit.MILLISECONDS.toSeconds(createdBefore.getMillis()));
        return this;
    }

    /**
     * A comma-separated array of creators (user ID's) to filter by.
     *
     * @param userIDs array of user ID's
     * @return RunsParametersProvider – provider object
     */
    public RunsParametersProvider createdBy(int... userIDs) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(userIDs), "User ID's should not be empty or null");
        parametersBuilder.append("&created_by=").append(StringUtils.join(userIDs, ','));
        return this;
    }

    /**
     * True to return completed test runs only. False to return active test runs only.
     *
     * @param completed is completed flag
     * @return RunsParametersProvider – provider object
     */
    public RunsParametersProvider isCompleted(boolean completed) {
        parametersBuilder.append("&is_completed=").append(BooleanUtils.toInteger(completed));
        return this;
    }

    /**
     * A comma-separated array of milestone ID's to filter by.
     *
     * @param milestoneIDs array of milestone ID's
     * @return RunsParametersProvider – provider object
     */
    public RunsParametersProvider milestoneID(int... milestoneIDs) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(milestoneIDs), "Milestone ID's should not be empty or null");
        parametersBuilder.append("&milestone_id=").append(StringUtils.join(milestoneIDs, ','));
        return this;
    }

    /**
     * A single Reference ID (e.g. TR-a, 4291, etc.).
     *
     * @param ref single reference ID
     * @return RunsParametersProvider – provider object
     */
    public RunsParametersProvider refsFilter(String ref) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(ref), "Reference should not be empty or null");
        parametersBuilder.append("&refs_filter=").append(ref);
        return this;
    }

    /**
     * A comma-separated array of test suite ID's to filter by.
     *
     * @param suiteIDs array of suite ID's
     * @return RunsParametersProvider – provider object
     */
    public RunsParametersProvider suiteID(int... suiteIDs) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(suiteIDs), "Suite ID's should not be empty or null");
        parametersBuilder.append("&suite_id=").append(StringUtils.join(suiteIDs, ','));
        return this;
    }
}
