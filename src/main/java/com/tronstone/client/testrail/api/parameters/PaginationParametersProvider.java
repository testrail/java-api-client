package com.tronstone.client.testrail.api.parameters;

import com.google.common.base.Preconditions;
import com.tronstone.client.inner.interfaces.ParametersProvider;
import lombok.AccessLevel;
import lombok.Getter;

public abstract class PaginationParametersProvider<T> implements ParametersProvider {
    private final @Getter(AccessLevel.PACKAGE) StringBuilder parametersBuilder = new StringBuilder();

    /**
     * The number of data set the response should return (The response size is 250 by default).
     *
     * @param limit number of data set between 1 and 250 inclusive
     * @return T – provider object
     */
    @SuppressWarnings("unchecked")
    public T limit(int limit) {
        Preconditions.checkArgument(limit > 0 && limit <= 250, "Limit should be between 1 and 250 inclusive");
        parametersBuilder.append("&limit=").append(limit);
        return (T) this;
    }

    /**
     * Where to start counting the data set from (the offset).
     * The offset parameter is what is used to navigate to the next set of results.
     *
     * @param offset data set offset
     * @return T – provider object
     */
    @SuppressWarnings("unchecked")
    public T offset(int offset) {
        Preconditions.checkArgument(offset >= 0, "Offset should not be less than zero");
        parametersBuilder.append("&offset=").append(offset);
        return (T) this;
    }

    @Override
    public String provide(String preparedURI) {
        return parametersBuilder.insert(0, preparedURI).toString();
    }
}
