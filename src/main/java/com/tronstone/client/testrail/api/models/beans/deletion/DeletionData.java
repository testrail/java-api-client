package com.tronstone.client.testrail.api.models.beans.deletion;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DeletionData {
    private int tests;
    private int cases;
    private int runs;
    private int results;
}
