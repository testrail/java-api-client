package com.tronstone.client.testrail.api.models.beans.fields;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Preconditions;
import com.google.common.primitives.Ints;
import com.tronstone.client.inner.utils.BuilderUtils;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;
import net.sf.cglib.core.ReflectUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fasterxml.jackson.annotation.JsonProperty.Access;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FieldConfig {
    private FieldContext context;
    private FieldOptions options;

    @JsonProperty(access = Access.WRITE_ONLY)
    private String id;

    public String getID() {
        return id;
    }

    /**
     * Returns field config object builder.
     *
     * @return FieldConfigBuilder – field config builder object
     */
    public static FieldConfigBuilder construct() {
        return new FieldConfigBuilder();
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class FieldConfigBuilder {
        private boolean global;
        private Boolean required;
        private String defaultValue;
        private FieldFormat format;
        private int rows = -1;
        private final Map<Integer, String> items = new HashMap<>();
        private final List<Integer> projectIDs = new ArrayList<>();

        /**
         * Returns field config context object builder.
         *
         * Set global flag to true if you want the new custom field included for all projects.
         * Otherwise, (false) specify the ID’s of projects
         * to be included as the (projectIDs) parameter.
         *
         * @param global custom field global flag, false by default
         * @return ContextBuilder – field config context builder object
         */
        public ContextBuilder context(boolean global) {
            this.global = global;
            return new ContextBuilder();
        }

        public class ContextBuilder {
            /**
             * ID’s of projects new custom field will apply to if global is set to false.
             *
             * @param projectIDs list of projects ID's
             * @return ContextBuilder – field config context builder object
             */
            public ContextBuilder projectIDs(List<Integer> projectIDs) {
                Preconditions.checkArgument(CollectionUtils.isNotEmpty(projectIDs), "Project ID's should not be empty or null");
                FieldConfigBuilder.this.projectIDs.addAll(projectIDs);
                return this;
            }

            /**
             * ID’s of projects new custom field will apply to if global is set to false.
             *
             * @param projectIDs array of projects ID's
             * @return ContextBuilder – field config context builder object
             */
            public ContextBuilder projectIDs(int... projectIDs) {
                Preconditions.checkArgument(ArrayUtils.isNotEmpty(projectIDs), "Project ID's should not be empty or null");
                FieldConfigBuilder.this.projectIDs.addAll(Ints.asList(projectIDs));
                return this;
            }

            /**
             * Returns field config options object builder.
             *
             * @param required custom field required flag, false by default
             * @return OptionsBuilder – field config options builder object
             */
            public OptionsBuilder options(boolean required) {
                FieldConfigBuilder.this.required = required;
                return new OptionsBuilder();
            }
        }

        public class OptionsBuilder {
            /**
             * Most of the types have the option to set “default_value” with
             * exception for types Multiselect, Milestone and Date, where this option is not allowed.
             *
             * @param defaultValue custom field default value
             * @return OptionsBuilder – field config options builder object
             */
            public OptionsBuilder defaultValue(String defaultValue) {
                Preconditions.checkArgument(StringUtils.isNotEmpty(defaultValue), "Default value should not be empty or null");
                FieldConfigBuilder.this.defaultValue = defaultValue;
                return this;
            }

            /**
             * For text and steps items.
             *
             * @param format format of text
             * @return OptionsBuilder – field config options builder object
             */
            public OptionsBuilder format(FieldFormat format) {
                Preconditions.checkNotNull(format, "Format should not be null");
                FieldConfigBuilder.this.format = format;
                return this;
            }

            /**
             * For dropdown and multi-select items.
             *
             * @param items map of items where key is id starts from zero and value is dropdown or multi-select option
             * @return OptionsBuilder – field config options builder object
             */
            public OptionsBuilder items(Map<Integer, String> items) {
                Preconditions.checkArgument(MapUtils.isNotEmpty(items), "Items should not be empty or null");
                FieldConfigBuilder.this.items.putAll(items);
                return this;
            }

            /**
             * For dropdown and multi-select items.
             *
             * @param items array of items where every item is dropdown or multi-select option
             * @return OptionsBuilder – field config options builder object
             */
            public OptionsBuilder items(String... items) {
                Preconditions.checkArgument(ArrayUtils.isNotEmpty(items), "Items should not be empty or null");

                for (int i = 0; i < items.length; i++) {
                    FieldConfigBuilder.this.items.put(i, items[i]);
                }

                return this;
            }

            /**
             * Rows option specifies the initial size of the field when the user loads a form.
             * The valid values for it are e.g. 3, 4, …, 10 or 0 (empty string).
             *
             * @param rows initial size of the field
             * @return OptionsBuilder – field config options builder object
             */
            public OptionsBuilder rows(int rows) {
                Preconditions.checkArgument(rows == 0 || (rows >= 3 && rows <= 10), "Rows should be zero or between 3 and 10 inclusive");
                FieldConfigBuilder.this.rows = rows;
                return this;
            }

            /**
             * Build ready field config object.
             *
             * @return FieldConfig – constructed field config object
             */
            @SneakyThrows
            public FieldConfig build() {
                if (global && CollectionUtils.isNotEmpty(projectIDs)) {
                    throw new IllegalArgumentException("If config context global() is true, than config context projectIDs() should be empty");
                } else if (!global && CollectionUtils.isEmpty(projectIDs)) {
                    throw new IllegalArgumentException("If config context global() is false, than config context projectIDs() should not be empty");
                }

                FieldConfig fieldConfig = new FieldConfig();

                fieldConfig.context = BuilderUtils.fillWithBuilderFields(
                        (FieldContext) ReflectUtils.newInstance(FieldContext.class),
                        this
                );
                fieldConfig.options = BuilderUtils.fillWithBuilderFields(
                        (FieldOptions) ReflectUtils.newInstance(FieldOptions.class),
                        this
                );

                return fieldConfig;
            }
        }
    }
}
