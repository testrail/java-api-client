package com.tronstone.client.testrail.api.models.beans.fields;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.stream.Collectors;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;
import static com.fasterxml.jackson.annotation.JsonProperty.Access;

@Getter
@ToString
@EqualsAndHashCode
@JsonInclude(Include.NON_DEFAULT)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FieldOptions {
    private static final String ITEMS_SEPARATOR = ", ";

    private String defaultValue;

    @JsonProperty("is_required")
    private @Getter(AccessLevel.NONE) Boolean required;

    @JsonSerialize(converter = RowsSerializeConverter.class)
    private final int rows = -1;

    @JsonSerialize(converter = ItemsSerializeConverter.class)
    @JsonDeserialize(converter = ItemsDeserializeConverter.class)
    private Map<Integer, String> items;

    @JsonSerialize(converter = FieldFormatSerializeConverter.class)
    @JsonDeserialize(converter = FieldFormatDeserializeConverter.class)
    private FieldFormat format;

    @JsonProperty(access = Access.WRITE_ONLY)
    private boolean hasActual;

    @JsonProperty(access = Access.WRITE_ONLY)
    private boolean hasExpected;

    @JsonProperty(access = Access.WRITE_ONLY)
    private boolean hasAdditional;

    @JsonProperty(access = Access.WRITE_ONLY)
    private boolean hasReference;

    public boolean isRequired() {
        return required;
    }

    private static class RowsSerializeConverter extends StdConverter<Integer, String> {
        @Override
        public String convert(Integer rows) {
            if (rows == 0) {
                return StringUtils.EMPTY;
            }

            return rows.toString();
        }
    }

    private static class ItemsSerializeConverter extends StdConverter<Map<Integer, String>, String> {
        @Override
        public String convert(Map<Integer, String> items) {
            return Joiner.on(StringUtils.LF).withKeyValueSeparator(ITEMS_SEPARATOR).join(items);
        }
    }

    private static class ItemsDeserializeConverter extends StdConverter<String, Map<Integer, String>> {
        @Override
        @SuppressWarnings("UnstableApiUsage")
        public Map<Integer, String> convert(String items) {
            return Splitter.on(StringUtils.LF)
                    .withKeyValueSeparator(ITEMS_SEPARATOR)
                    .split(items)
                    .entrySet()
                    .stream()
                    .map(entry -> Pair.of(Integer.parseInt(entry.getKey()), entry.getValue()))
                    .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
        }
    }

    private static class FieldFormatSerializeConverter extends StdConverter<FieldFormat, String> {
        @Override
        public String convert(FieldFormat format) {
            return format.toString();
        }
    }

    private static class FieldFormatDeserializeConverter extends StdConverter<String, FieldFormat> {
        @Override
        public FieldFormat convert(String format) {
            return FieldFormat.valueOfFieldFormat(format);
        }
    }
}
