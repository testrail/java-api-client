package com.tronstone.client.testrail.api.models.beans.plans;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;
import com.google.common.primitives.Ints;
import com.tronstone.client.inner.utils.BuilderUtils;
import com.tronstone.client.inner.utils.CollectionsUtils;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.ReferencesDeserializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.serialize.ReferencesSerializeConverter;
import com.tronstone.client.testrail.api.models.parsing.filters.EmptyListIncludeFilter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@ToString
@EqualsAndHashCode
@JsonInclude(Include.NON_DEFAULT)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Entry {
    @JsonView({Entry.class, EntryUpdate.class})
    private String name;

    @JsonView({Entry.class, EntryUpdate.class})
    private String description;

    @JsonView(Entry.class)
    private int suiteID;

    @JsonView({Entry.class, EntryUpdate.class})
    private @Getter(AccessLevel.NONE) Boolean includeAll;

    @JsonView({Entry.class, EntryUpdate.class})
    @JsonSerialize(converter = ReferencesSerializeConverter.class)
    @JsonDeserialize(converter = ReferencesDeserializeConverter.class)
    @JsonInclude(value = Include.CUSTOM, valueFilter = EmptyListIncludeFilter.class)
    private final List<String> refs = new ArrayList<>();

    @JsonView({Entry.class, EntryUpdate.class})
    @JsonProperty(value = "assignedto_id", access = Access.READ_ONLY)
    private @Getter(AccessLevel.NONE) int assigneeID;

    @JsonProperty(access = Access.READ_ONLY)
    @JsonView({Entry.class, EntryUpdate.class})
    @JsonInclude(value = Include.CUSTOM, valueFilter = EmptyListIncludeFilter.class)
    private final @Getter(AccessLevel.NONE) List<Integer> caseIDs = new ArrayList<>();

    @JsonView(Entry.class)
    @JsonProperty(access = Access.READ_ONLY)
    private final @Getter(AccessLevel.NONE) List<Integer> configIDs = new ArrayList<>();

    @JsonView(Entry.class)
    @JsonProperty(value = "runs", access = Access.READ_ONLY)
    private final @Getter(AccessLevel.NONE) List<Run> entryRuns = new ArrayList<>();

    @JsonProperty(access = Access.WRITE_ONLY)
    private String id;

    @JsonProperty(access = Access.WRITE_ONLY)
    private List<com.tronstone.client.testrail.api.models.beans.runs.Run> runs;

    /**
     * Returns entry object builder.
     *
     * @param suiteID the ID of the test suite
     * @return EntryBuilder – entry builder object
     */
    public static EntryBuilder construct(int suiteID) {
        Preconditions.checkArgument(suiteID > 0, "Suite ID should be positive number");
        return new EntryBuilder(suiteID);
    }

    public String getID() {
        return id;
    }

    public boolean isIncludeAll() {
        return includeAll;
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public static class EntryBuilder {
        private @NonNull int suiteID;
        private String name;
        private int assigneeID;
        private String description;
        private boolean includeAll = true;
        private final List<String> refs = new ArrayList<>();
        private final List<Run> entryRuns = new ArrayList<>();
        private final List<Integer> caseIDs = new ArrayList<>();
        private final List<Integer> configIDs = new ArrayList<>();

        /**
         * @param name the name of the plan entry
         * @return EntryBuilder – entry builder object
         */
        public EntryBuilder name(String name) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
            this.name = name;
            return this;
        }

        /**
         * @param description the description of the plan entry
         * @return EntryBuilder – entry builder object
         */
        public EntryBuilder description(String description) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(description), "Description should not be empty or null");
            this.description = description;
            return this;
        }

        /**
         * @param assigneeID the ID of the user the plan entry should be assigned to
         * @return EntryBuilder – entry builder object
         */
        public EntryBuilder assigneeID(int assigneeID) {
            Preconditions.checkArgument(assigneeID > 0, "Assignee ID should be positive number");
            this.assigneeID = assigneeID;
            return this;
        }

        /**
         * True for including all test cases of the test suite
         * and false for a custom case selection (default: true).
         *
         * @param includeAll test cases included all flag, true by default
         * @return EntryBuilder – entry builder object
         */
        public EntryBuilder includeAll(boolean includeAll) {
            this.includeAll = includeAll;
            return this;
        }

        /**
         * @param caseIDs list of case ID's for the custom case selection
         * @return EntryBuilder – entry builder object
         */
        public EntryBuilder caseIDs(List<Integer> caseIDs) {
            Preconditions.checkArgument(CollectionUtils.isNotEmpty(caseIDs), "Case ID's should not be empty or null");
            this.caseIDs.addAll(caseIDs);
            return this;
        }

        /**
         * @param caseIDs an array of case ID's for the custom case selection
         * @return EntryBuilder – entry builder object
         */
        public EntryBuilder caseIDs(int... caseIDs) {
            Preconditions.checkArgument(ArrayUtils.isNotEmpty(caseIDs), "Case ID's should not be empty or null");
            this.caseIDs.addAll(Ints.asList(caseIDs));
            return this;
        }

        /**
         * @param configIDs list of config ID's
         * @return EntryBuilder – entry builder object
         */
        public EntryBuilder configIDs(List<Integer> configIDs) {
            Preconditions.checkArgument(CollectionUtils.isNotEmpty(configIDs), "Config ID's should not be empty or null");
            this.configIDs.addAll(configIDs);
            return this;
        }

        /**
         * @param configIDs an array of config ID's
         * @return EntryBuilder – entry builder object
         */
        public EntryBuilder configIDs(int... configIDs) {
            Preconditions.checkArgument(ArrayUtils.isNotEmpty(configIDs), "Config ID's should not be empty or null");
            this.configIDs.addAll(Ints.asList(configIDs));
            return this;
        }

        /**
         * Add the list of references/requirements.
         *
         * @param refs list of references/requirements
         * @return EntryBuilder – entry builder object
         */
        public EntryBuilder refs(List<String> refs) {
            Preconditions.checkArgument(CollectionUtils.isNotEmpty(refs), "References should not be empty or null");
            this.refs.addAll(refs);
            return this;
        }

        /**
         * Parse and add list of comma-separated references/requirements.
         *
         * @param refs a comma-separated list of references/requirements
         * @return EntryBuilder – entry builder object
         */
        public EntryBuilder refs(String refs) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(refs), "References should not be empty or null");
            this.refs.addAll(new ReferencesDeserializeConverter().convert(refs));
            return this;
        }

        /**
         * Add the array of references/requirements.
         *
         * @param refs an array of references/requirements
         * @return EntryBuilder – entry builder object
         */
        public EntryBuilder refs(String... refs) {
            Preconditions.checkArgument(ArrayUtils.isNotEmpty(refs), "References should not be empty or null");

            this.refs.addAll(Arrays.stream(refs)
                    .peek(ref -> Preconditions.checkArgument(StringUtils.isNotEmpty(ref), "Reference should not be empty or null"))
                    .collect(Collectors.toList()));

            return this;
        }

        /**
         * Add entry run to entry runs list.
         *
         * @param entryRun new entry run
         * @return EntryBuilder – entry builder object
         */
        public EntryBuilder addRun(Run entryRun) {
            Preconditions.checkNotNull(entryRun, "Entry run should not be null");
            this.entryRuns.add(entryRun);
            return this;
        }

        /**
         * Build ready entry object.
         *
         * @return Entry – constructed entry object
         */
        public Entry build() {
            if (includeAll && CollectionUtils.isNotEmpty(caseIDs)) {
                throw new IllegalArgumentException("If entry includeAll() is true, than entry caseIDs() should be empty");
            } else if (!includeAll && CollectionUtils.isEmpty(caseIDs)) {
                throw new IllegalArgumentException("If entry includeAll() is false, than entry caseIDs() should not be empty");
            }

            Entry entry = new Entry();

            entry.suiteID = suiteID;
            entry.name = name;
            entry.assigneeID = assigneeID;
            entry.description = description;
            entry.includeAll = includeAll;
            entry.refs.addAll(refs);
            entry.entryRuns.addAll(entryRuns);
            entry.caseIDs.addAll(caseIDs);
            entry.configIDs.addAll(configIDs);

            return entry;
        }
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class EntryUpdate extends Entry {
        /**
         * Returns entry object builder for update operations.
         *
         * @return EntryUpdateBuilder – entry update builder object
         */
        public static EntryUpdateBuilder constructUpdate() {
            return new EntryUpdateBuilder();
        }

        @SuppressWarnings("FieldCanBeLocal")
        @NoArgsConstructor(access = AccessLevel.PRIVATE)
        public static class EntryUpdateBuilder {
            private String name;
            private int assigneeID;
            private String description;
            private Boolean includeAll;
            private List<String> refs = new ArrayList<>();
            private List<Integer> caseIDs = new ArrayList<>();

            /**
             * @param name the name of the entry
             * @return EntryUpdateBuilder – entry update builder object
             */
            public EntryUpdateBuilder name(String name) {
                Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
                this.name = name;
                return this;
            }

            /**
             * @param assigneeID the ID of the user the entry should be assigned to
             * @return EntryUpdateBuilder – entry update builder object
             */
            public EntryUpdateBuilder assigneeID(int assigneeID) {
                Preconditions.checkArgument(assigneeID > 0, "Assignee ID should be positive number");
                this.assigneeID = assigneeID;
                return this;
            }

            /**
             * Use empty string for removing description.
             *
             * @param description the description of the test entry
             * @return EntryUpdateBuilder – entry update builder object
             */
            public EntryUpdateBuilder description(String description) {
                Preconditions.checkNotNull(description, "Description should not be null");
                this.description = description;
                return this;
            }

            /**
             * True for including all test cases of the test suite
             * and false for a custom case selection (default: true).
             *
             * @param includeAll test cases included all flag, true by default
             * @return EntryUpdateBuilder – entry update builder object
             */
            public EntryUpdateBuilder includeAll(boolean includeAll) {
                this.includeAll = includeAll;
                return this;
            }

            /**
             * If the case ID's already establish, it will be replaced.
             * Use {@link Collections#emptyList()} for removing case ID's.
             *
             * @param caseIDs list of case ID's for the custom case selection
             * @return EntryUpdateBuilder – entry update builder object
             */
            public EntryUpdateBuilder caseIDs(List<Integer> caseIDs) {
                Preconditions.checkNotNull(caseIDs, "Case ID's should not be null");

                if (CollectionsUtils.isEmptyList(caseIDs)) {
                    this.caseIDs = caseIDs;
                    return this;
                } else if (CollectionsUtils.isEmptyList(this.caseIDs)) {
                    this.caseIDs = new ArrayList<>();
                }

                this.caseIDs.addAll(caseIDs);
                return this;
            }

            /**
             * If the case ID's already establish, it will be replaced.
             *
             * @param caseIDs an array of case ID's for the custom case selection
             * @return EntryUpdateBuilder – entry update builder object
             */
            public EntryUpdateBuilder caseIDs(int... caseIDs) {
                Preconditions.checkArgument(ArrayUtils.isNotEmpty(caseIDs), "Case ID's should be not empty or null");

                if (CollectionsUtils.isEmptyList(this.caseIDs)) {
                    this.caseIDs = new ArrayList<>();
                }

                this.caseIDs.addAll(Ints.asList(caseIDs));
                return this;
            }

            /**
             * Add the list of references/requirements.
             * If the refs already establish, it will be replaced.
             * Use {@link Collections#emptyList()} for removing references/requirements.
             *
             * @param refs list of references/requirements
             * @return EntryUpdateBuilder – entry update builder object
             */
            public EntryUpdateBuilder refs(List<String> refs) {
                Preconditions.checkNotNull(refs, "References should not be null");

                if (CollectionsUtils.isEmptyList(refs)) {
                    this.refs = refs;
                    return this;
                } else if (CollectionsUtils.isEmptyList(this.refs)) {
                    this.refs = new ArrayList<>();
                }

                this.refs.addAll(refs);
                return this;
            }

            /**
             * Parse and add list of comma-separated references/requirements.
             * If the refs already establish, it will be replaced.
             * Use empty string for removing references/requirements.
             *
             * @param refs a comma-separated list of references/requirements
             * @return EntryUpdateBuilder – entry run builder object
             */
            public EntryUpdateBuilder refs(String refs) {
                if (StringUtils.isEmpty(refs)) {
                    if (CollectionsUtils.isNotEmptyList(this.refs)) {
                        this.refs = Collections.emptyList();
                    }

                    return this;
                } else if (CollectionsUtils.isEmptyList(this.refs)) {
                    this.refs = new ArrayList<>();
                }

                this.refs.addAll(new ReferencesDeserializeConverter().convert(refs));
                return this;
            }

            /**
             * Add the array of references/requirements.
             * If the refs already establish, it will be replaced.
             *
             * @param refs an array of references/requirements
             * @return EntryUpdateBuilder – entry update builder object
             */
            public EntryUpdateBuilder refs(String... refs) {
                Preconditions.checkArgument(ArrayUtils.isNotEmpty(refs), "References should not be empty or null");

                if (CollectionsUtils.isEmptyList(this.refs)) {
                    this.refs = new ArrayList<>();
                }

                this.refs.addAll(Arrays.stream(refs)
                        .peek(ref -> Preconditions.checkArgument(StringUtils.isNotEmpty(ref), "Reference should not be empty or null"))
                        .collect(Collectors.toList()));

                return this;
            }

            /**
             * Build ready entry update object.
             *
             * @return EntryUpdate – constructed entry update object
             */
            public EntryUpdate build() {
                if (Objects.isNull(includeAll) && CollectionUtils.isNotEmpty(caseIDs)) {
                    throw new IllegalArgumentException("If entry update caseIDs() is not empty, than provide entry update includeAll() flag");
                } else if (Objects.nonNull(includeAll)) {
                    if (includeAll && CollectionUtils.isNotEmpty(caseIDs)) {
                        throw new IllegalArgumentException("If entry update includeAll() is true, than entry update caseIDs() should be empty");
                    } else if (!includeAll && CollectionUtils.isEmpty(caseIDs)) {
                        throw new IllegalArgumentException("If entry update includeAll() is false, than entry update caseIDs() should not be empty");
                    }
                }

                return BuilderUtils.fillWithBuilderFields(new EntryUpdate(), this);
            }
        }
    }

    @ToString
    @EqualsAndHashCode
    @JsonInclude(Include.NON_DEFAULT)
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Run {
        @JsonView({Run.class, RunUpdate.class})
        @JsonProperty(value = "assignedto_id", access = Access.READ_ONLY)
        private int assigneeID;

        @JsonView({Run.class, RunUpdate.class})
        @JsonProperty(access = Access.READ_ONLY)
        private String description;

        @JsonView({Run.class, RunUpdate.class})
        @JsonProperty(access = Access.READ_ONLY)
        private Boolean includeAll;

        @JsonView({Run.class, RunUpdate.class})
        @JsonProperty(access = Access.READ_ONLY)
        @JsonSerialize(converter = ReferencesSerializeConverter.class)
        @JsonInclude(value = Include.CUSTOM, valueFilter = EmptyListIncludeFilter.class)
        private final List<String> refs = new ArrayList<>();

        @JsonView({Run.class, RunUpdate.class})
        @JsonProperty(access = Access.READ_ONLY)
        @JsonInclude(value = Include.CUSTOM, valueFilter = EmptyListIncludeFilter.class)
        private final List<Integer> caseIDs = new ArrayList<>();

        @JsonView(Run.class)
        @JsonProperty(access = Access.READ_ONLY)
        private final List<Integer> configIDs = new ArrayList<>();

        /**
         * Returns entry run object builder.
         *
         * @param configIDs list of configuration ID's used for the test run of the test plan entry
         * @return RunBuilder – entry run builder object
         */
        public static RunBuilder construct(List<Integer> configIDs) {
            Preconditions.checkArgument(CollectionUtils.isNotEmpty(configIDs), "Config ID's should not be empty or null");
            return new RunBuilder(configIDs);
        }

        /**
         * Returns entry run object builder.
         *
         * @param configIDs an array of configuration ID's used for the test run of the test plan entry
         * @return RunBuilder – entry run builder object
         */
        public static RunBuilder construct(int... configIDs) {
            Preconditions.checkArgument(ArrayUtils.isNotEmpty(configIDs), "Config ID's should not be empty or null");
            return new RunBuilder(Ints.asList(configIDs));
        }

        @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
        public static class RunBuilder {
            private int assigneeID;
            private String description;
            private boolean includeAll = true;
            private final List<String> refs = new ArrayList<>();
            private final List<Integer> caseIDs = new ArrayList<>();
            private final List<Integer> configIDs = new ArrayList<>();

            private RunBuilder(List<Integer> configIDs) {
                this.configIDs.addAll(configIDs);
            }

            /**
             * @param assigneeID the ID of the user the entry run should be assigned to
             * @return RunBuilder – entry run builder object
             */
            public RunBuilder assigneeID(int assigneeID) {
                Preconditions.checkArgument(assigneeID > 0, "Assignee ID should be positive number");
                this.assigneeID = assigneeID;
                return this;
            }

            /**
             * @param description the description of the test run
             * @return RunBuilder – entry run builder object
             */
            public RunBuilder description(String description) {
                Preconditions.checkArgument(StringUtils.isNotEmpty(description), "Description should not be empty or null");
                this.description = description;
                return this;
            }

            /**
             * True for including all test cases of the test suite
             * and false for a custom case selection (default: true).
             *
             * @param includeAll test cases included all flag, true by default
             * @return RunBuilder – entry run builder object
             */
            public RunBuilder includeAll(boolean includeAll) {
                this.includeAll = includeAll;
                return this;
            }

            /**
             * @param caseIDs list of case ID's for the custom case selection
             * @return RunBuilder – entry run builder object
             */
            public RunBuilder caseIDs(List<Integer> caseIDs) {
                Preconditions.checkArgument(CollectionUtils.isNotEmpty(caseIDs), "Case ID's should not be empty or null");
                this.caseIDs.addAll(caseIDs);
                return this;
            }

            /**
             * @param caseIDs an array of case ID's for the custom case selection
             * @return RunBuilder – entry run builder object
             */
            public RunBuilder caseIDs(int... caseIDs) {
                Preconditions.checkArgument(ArrayUtils.isNotEmpty(caseIDs), "Case ID's should not be empty or null");
                this.caseIDs.addAll(Ints.asList(caseIDs));
                return this;
            }

            /**
             * Add the list of references/requirements.
             *
             * @param refs list of references/requirements
             * @return RunBuilder – entry run builder object
             */
            public RunBuilder refs(List<String> refs) {
                Preconditions.checkArgument(CollectionUtils.isNotEmpty(refs), "References should not be empty or null");
                this.refs.addAll(refs);
                return this;
            }

            /**
             * Parse and add list of comma-separated references/requirements.
             *
             * @param refs a comma-separated list of references/requirements
             * @return RunBuilder – entry run builder object
             */
            public RunBuilder refs(String refs) {
                Preconditions.checkArgument(StringUtils.isNotEmpty(refs), "References should not be empty or null");
                this.refs.addAll(new ReferencesDeserializeConverter().convert(refs));
                return this;
            }

            /**
             * Add the array of references/requirements.
             *
             * @param refs an array of references/requirements
             * @return RunBuilder – entry run builder object
             */
            public RunBuilder refs(String... refs) {
                Preconditions.checkArgument(ArrayUtils.isNotEmpty(refs), "References should not be empty or null");

                this.refs.addAll(Arrays.stream(refs)
                        .peek(ref -> Preconditions.checkArgument(StringUtils.isNotEmpty(ref), "Reference should not be empty or null"))
                        .collect(Collectors.toList()));

                return this;
            }

            /**
             * Build ready entry run object.
             *
             * @return Run – constructed entry run object
             */
            public Run build() {
                if (includeAll && CollectionUtils.isNotEmpty(caseIDs)) {
                    throw new IllegalArgumentException("If entry run includeAll() is true, than entry run caseIDs() should be empty");
                } else if (!includeAll && CollectionUtils.isEmpty(caseIDs)) {
                    throw new IllegalArgumentException("If entry run includeAll() is false, than entry run caseIDs() should not be empty");
                }

                Run entryRun = new Run();

                entryRun.assigneeID = assigneeID;
                entryRun.description = description;
                entryRun.includeAll = includeAll;
                entryRun.refs.addAll(refs);
                entryRun.caseIDs.addAll(caseIDs);
                entryRun.configIDs.addAll(configIDs);

                return entryRun;
            }
        }

        @NoArgsConstructor(access = AccessLevel.PRIVATE)
        public static class RunUpdate extends Run {
            /**
             * Returns entry run object builder for update operations.
             *
             * @return RunUpdateBuilder – entry run update builder object
             */
            public static RunUpdateBuilder constructUpdate() {
                return new RunUpdateBuilder();
            }

            @SuppressWarnings("FieldCanBeLocal")
            @NoArgsConstructor(access = AccessLevel.PRIVATE)
            public static class RunUpdateBuilder {
                private int assigneeID;
                private String description;
                private Boolean includeAll;
                private List<String> refs = new ArrayList<>();
                private List<Integer> caseIDs = new ArrayList<>();

                /**
                 * @param assigneeID the ID of the user the entry run should be assigned to
                 * @return RunUpdateBuilder – entry run update builder object
                 */
                public RunUpdateBuilder assigneeID(int assigneeID) {
                    Preconditions.checkArgument(assigneeID > 0, "Assignee ID should be positive number");
                    this.assigneeID = assigneeID;
                    return this;
                }

                /**
                 * Use empty string for removing description.
                 *
                 * @param description the description of the test run
                 * @return RunUpdateBuilder – entry run update builder object
                 */
                public RunUpdateBuilder description(String description) {
                    Preconditions.checkNotNull(description, "Description should not be null");
                    this.description = description;
                    return this;
                }

                /**
                 * True for including all test cases of the test suite
                 * and false for a custom case selection (default: true).
                 *
                 * @param includeAll test cases included all flag, true by default
                 * @return RunUpdateBuilder – entry run update builder object
                 */
                public RunUpdateBuilder includeAll(boolean includeAll) {
                    this.includeAll = includeAll;
                    return this;
                }

                /**
                 * If the case ID's already establish, it will be replaced.
                 * Use {@link Collections#emptyList()} for removing case ID's.
                 *
                 * @param caseIDs list of case ID's for the custom case selection
                 * @return RunUpdateBuilder – entry run update builder object
                 */
                public RunUpdateBuilder caseIDs(List<Integer> caseIDs) {
                    Preconditions.checkNotNull(caseIDs, "Case ID's should not be null");

                    if (CollectionsUtils.isEmptyList(caseIDs)) {
                        this.caseIDs = caseIDs;
                        return this;
                    } else if (CollectionsUtils.isEmptyList(this.caseIDs)) {
                        this.caseIDs = new ArrayList<>();
                    }

                    this.caseIDs.addAll(caseIDs);
                    return this;
                }

                /**
                 * If the case ID's already establish, it will be replaced.
                 *
                 * @param caseIDs an array of case ID's for the custom case selection
                 * @return RunUpdateBuilder – entry run update builder object
                 */
                public RunUpdateBuilder caseIDs(int... caseIDs) {
                    Preconditions.checkArgument(ArrayUtils.isNotEmpty(caseIDs), "Case ID's should be not empty or null");

                    if (CollectionsUtils.isEmptyList(this.caseIDs)) {
                        this.caseIDs = new ArrayList<>();
                    }

                    this.caseIDs.addAll(Ints.asList(caseIDs));
                    return this;
                }

                /**
                 * Add the list of references/requirements.
                 * If the refs already establish, it will be replaced.
                 * Use {@link Collections#emptyList()} for removing references/requirements.
                 *
                 * @param refs list of references/requirements
                 * @return RunUpdateBuilder – entry run update builder object
                 */
                public RunUpdateBuilder refs(List<String> refs) {
                    Preconditions.checkNotNull(refs, "References should not be null");

                    if (CollectionsUtils.isEmptyList(refs)) {
                        this.refs = refs;
                        return this;
                    } else if (CollectionsUtils.isEmptyList(this.refs)) {
                        this.refs = new ArrayList<>();
                    }

                    this.refs.addAll(refs);
                    return this;
                }

                /**
                 * Parse and add list of comma-separated references/requirements.
                 * If the refs already establish, it will be replaced.
                 * Use empty string for removing references/requirements.
                 *
                 * @param refs a comma-separated list of references/requirements
                 * @return RunUpdateBuilder – entry run update builder object
                 */
                public RunUpdateBuilder refs(String refs) {
                    if (StringUtils.isEmpty(refs)) {
                        if (CollectionsUtils.isNotEmptyList(this.refs)) {
                            this.refs = Collections.emptyList();
                        }

                        return this;
                    } else if (CollectionsUtils.isEmptyList(this.refs)) {
                        this.refs = new ArrayList<>();
                    }

                    this.refs.addAll(new ReferencesDeserializeConverter().convert(refs));
                    return this;
                }

                /**
                 * Add the array of references/requirements.
                 * If the refs already establish, it will be replaced.
                 *
                 * @param refs an array of references/requirements
                 * @return RunUpdateBuilder – entry run update builder object
                 */
                public RunUpdateBuilder refs(String... refs) {
                    Preconditions.checkArgument(ArrayUtils.isNotEmpty(refs), "References should not be empty or null");

                    if (CollectionsUtils.isEmptyList(this.refs)) {
                        this.refs = new ArrayList<>();
                    }

                    this.refs.addAll(Arrays.stream(refs)
                            .peek(ref -> Preconditions.checkArgument(StringUtils.isNotEmpty(ref), "Reference should not be empty or null"))
                            .collect(Collectors.toList()));

                    return this;
                }

                /**
                 * Build ready entry run update object.
                 *
                 * @return RunUpdate – constructed entry run update object
                 */
                public RunUpdate build() {
                    if (Objects.isNull(includeAll) && CollectionUtils.isNotEmpty(caseIDs)) {
                        throw new IllegalArgumentException("If entry run update caseIDs() is not empty, than provide entry run update includeAll() flag");
                    } else if (Objects.nonNull(includeAll)) {
                        if (includeAll && CollectionUtils.isNotEmpty(caseIDs)) {
                            throw new IllegalArgumentException("If entry run update includeAll() is true, than entry run update caseIDs() should be empty");
                        } else if (!includeAll && CollectionUtils.isEmpty(caseIDs)) {
                            throw new IllegalArgumentException("If entry run update includeAll() is false, than entry run update caseIDs() should not be empty");
                        }
                    }

                    return BuilderUtils.fillWithBuilderFields(new RunUpdate(), this);
                }
            }
        }
    }
}
