package com.tronstone.client.testrail.api.services;

import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.tronstone.client.inner.annotations.MapName;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.inner.service.ServiceFactory;
import com.tronstone.client.testrail.api.models.beans.fields.Field;
import com.tronstone.client.testrail.api.models.beans.tests.Test;
import com.tronstone.client.testrail.api.models.parsing.modules.CustomFieldModule;
import com.tronstone.client.testrail.api.parameters.TestsParametersProvider;
import com.tronstone.client.testrail.api.services.pagination.ServicePaginator;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Use the following API methods to request details about
 * tests (individual instances of test cases added to specific test runs or test plans).
 */
public class TestsService extends AbstractTestRailService {
    private final boolean cached;
    private final CaseFieldsService caseFieldsService;
    private final LoadingCache<CaseFieldsService, Map<String, Field>> cache;

    public TestsService(TestRailClientConfiguration configuration, boolean cached) {
        super(configuration);

        this.cached = cached;
        caseFieldsService = ServiceFactory.createService(CaseFieldsService.class, configuration);
        cache = CacheBuilder.newBuilder().build(new CacheLoader<CaseFieldsService, Map<String, Field>>() {
            @NonNull
            @Override
            public Map<String, Field> load(@NonNull CaseFieldsService caseFieldsService) {
                return caseFieldsService.getCaseFields().stream().reduce(new HashMap<>(), (caseFields, caseField) -> {
                    caseFields.put(caseField.getSystemName(), caseField);
                    return caseFields;
                }, (caseFields, parallelCaseFieldTypes) -> caseFields);
            }
        });
    }

    /**
     * Returns an existing test.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/tests#gettest">Get Test</a>
     * @param testID the ID of the test
     * @return Test – object representation of the test
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_test/{test_id}")
    public Test getTest(@MapName("test_id") int testID) {
        Preconditions.checkArgument(testID > 0, "Test ID should be positive number");
        return getObjectMapper().registerModule(new CustomFieldModule(getCaseFields())).readValue(getRequester().prepareAndExecuteRequest(), Test.class);
    }

    /**
     * Returns an existing test.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/tests#gettest">Get Test</a>
     * @param testID the ID of the test
     * @param withData the parameter to get data
     * @return Test – object representation of the test
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_test/{test_id}&with_data={with_data}")
    public Test getTest(@MapName("test_id") int testID, @MapName("with_data") String withData) {
        Preconditions.checkArgument(testID > 0, "Test ID should be positive number");
        Preconditions.checkArgument(StringUtils.isNotEmpty(withData), "With data should not be empty or null");

        return getObjectMapper().registerModule(new CustomFieldModule(getCaseFields())).readValue(getRequester().prepareAndExecuteRequest(), Test.class);
    }

    /**
     * Returns a list of tests for a test run.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/tests#gettests">Get Tests</a>
     * @param runID the ID of the test run
     * @return ServicePaginator<Test> – data sets pagination service for tests
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_tests/{run_id}")
    public ServicePaginator<Test> getTests(@MapName("run_id") int runID) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");

        return new ServicePaginator<Test>(getConfiguration(), getRequester().prepareRequest(), Test.class) {{
            getObjectMapper().registerModule(new CustomFieldModule(getCaseFields()));
        }};
    }

    /**
     * Returns a list of filtered tests for a test run.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/tests#gettests">Get Tests</a>
     * @param runID the ID of the test run
     * @param parametersProvider request filter parameters object
     * @return ServicePaginator<Test> – data sets pagination service for tests
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_tests/{run_id}")
    public ServicePaginator<Test> getTests(@MapName("run_id") int runID, TestsParametersProvider parametersProvider) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");
        Preconditions.checkNotNull(parametersProvider, "Tests parameters provider should not be null");

        return new ServicePaginator<Test>(getConfiguration(), getRequester().prepareRequest(), Test.class) {{
            getObjectMapper().registerModule(new CustomFieldModule(getCaseFields()));
        }};
    }

    /**
     * Utility method for refreshing inner cache in a case
     * when custom fields caching is enabled. Useful when the structure of the
     * custom fields changes during the use of the client library.
     */
    public void refreshCustomFields() {
        if (cached) {
            cache.refresh(caseFieldsService);
        }

        throw new IllegalAccessError("Data caching is disabled and therefore not available");
    }

    @SneakyThrows
    private Map<String, Field> getCaseFields() {
        if (cached) {
            return cache.get(caseFieldsService);
        }

        cache.refresh(caseFieldsService);
        return cache.get(caseFieldsService);
    }
}
