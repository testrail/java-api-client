package com.tronstone.client.testrail.api.models.beans.projects;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProjectGroup {
    private int id;
    private int roleID;

    @JsonProperty("role")
    private String roleName;

    public int getID() {
        return id;
    }
}
