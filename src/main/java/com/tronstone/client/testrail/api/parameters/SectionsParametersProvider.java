package com.tronstone.client.testrail.api.parameters;

import com.google.common.base.Preconditions;
import lombok.NoArgsConstructor;

@NoArgsConstructor(staticName = "prepare")
public class SectionsParametersProvider extends PaginationParametersProvider<SectionsParametersProvider> {
    private final StringBuilder parametersBuilder = getParametersBuilder();

    /**
     * The ID of the test suite (optional if the project is operating in single suite mode).
     *
     * @param suiteID the ID of the test suite
     * @return SectionParametersProvider – provider object
     */
    public SectionsParametersProvider suiteID(int suiteID) {
        Preconditions.checkArgument(suiteID > 0, "Suite ID should be positive number");
        parametersBuilder.append("&suite_id=").append(suiteID);
        return this;
    }
}
