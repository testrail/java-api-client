package com.tronstone.client.testrail.api.models.beans.attachments;

import lombok.ToString;

@ToString
public enum EntityType {
    PLAN,
    RUN,
    TEST_CHANGE,
    CASE
}
