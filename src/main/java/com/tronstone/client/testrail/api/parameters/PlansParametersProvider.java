package com.tronstone.client.testrail.api.parameters;

import com.google.common.base.Preconditions;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.concurrent.TimeUnit;

@NoArgsConstructor(staticName = "prepare")
public class PlansParametersProvider extends PaginationParametersProvider<PlansParametersProvider> {
    private final StringBuilder parametersBuilder = getParametersBuilder();

    /**
     * Only return test plans created after this date.
     *
     * @param createdAfter time that will be converted to UNIX timestamp
     * @return PlansParametersProvider – provider object
     */
    public PlansParametersProvider createdAfter(DateTime createdAfter) {
        Preconditions.checkNotNull(createdAfter, "Created after should not be null");
        parametersBuilder.append("&created_after=").append(TimeUnit.MILLISECONDS.toSeconds(createdAfter.getMillis()));
        return this;
    }

    /**
     * Only return test plans created before this date.
     *
     * @param createdBefore time that will be converted to UNIX timestamp
     * @return PlansParametersProvider – provider object
     */
    public PlansParametersProvider createdBefore(DateTime createdBefore) {
        Preconditions.checkNotNull(createdBefore, "Created before should not be null");
        parametersBuilder.append("&created_before=").append(TimeUnit.MILLISECONDS.toSeconds(createdBefore.getMillis()));
        return this;
    }

    /**
     * A comma-separated array of creators (user ID's) to filter by.
     *
     * @param userIDs array of user ID's
     * @return PlansParametersProvider – provider object
     */
    public PlansParametersProvider createdBy(int... userIDs) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(userIDs), "User ID's should not be empty or null");
        parametersBuilder.append("&created_by=").append(StringUtils.join(userIDs, ','));
        return this;
    }

    /**
     * True to return completed test plans only. False to return active test plans only.
     *
     * @param completed is completed flag
     * @return PlansParametersProvider – provider object
     */
    public PlansParametersProvider isCompleted(boolean completed) {
        parametersBuilder.append("&is_completed=").append(BooleanUtils.toInteger(completed));
        return this;
    }

    /**
     * A comma-separated array of milestone ID's to filter by.
     *
     * @param milestoneIDs array of milestone ID's
     * @return PlansParametersProvider – provider object
     */
    public PlansParametersProvider milestoneID(int... milestoneIDs) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(milestoneIDs), "Milestone ID's should not be empty or null");
        parametersBuilder.append("&milestone_id=").append(StringUtils.join(milestoneIDs, ','));
        return this;
    }
}
