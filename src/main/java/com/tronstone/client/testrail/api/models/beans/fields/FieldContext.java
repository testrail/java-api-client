package com.tronstone.client.testrail.api.models.beans.fields;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FieldContext {
    private List<Integer> projectIDs;

    @JsonProperty("is_global")
    private boolean global;
}
