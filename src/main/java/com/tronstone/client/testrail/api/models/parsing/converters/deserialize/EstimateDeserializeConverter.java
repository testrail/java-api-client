package com.tronstone.client.testrail.api.models.parsing.converters.deserialize;

import com.fasterxml.jackson.databind.util.StdConverter;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.Period;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EstimateDeserializeConverter extends StdConverter<String, Period> {
    private final Pattern unitPattern = Pattern.compile("((\\d{1,2})w ?)?((\\d)d ?)?((\\d)h ?)?((\\d{1,2})m ?)?((\\d{1,2})s)?");

    @Override
    public Period convert(String estimate) {
        if (StringUtils.isEmpty(estimate)) {
            return null;
        }

        Preconditions.checkArgument(
                estimate.matches(unitPattern.pattern()),
                "Estimate doesn't match pattern '%s'",
                unitPattern.pattern()
        );

        Matcher unitMatcher = unitPattern.matcher(estimate);
        if (unitMatcher.find()) {
            Period period = new Period();

            String unit = unitMatcher.group(2);
            if (StringUtils.isNotEmpty(unit)) {
                period = period.plusWeeks(Integer.parseInt(unit));
            }

            unit = unitMatcher.group(4);
            if (StringUtils.isNotEmpty(unit)) {
                period = period.plusDays(Integer.parseInt(unit));
            }

            unit = unitMatcher.group(6);
            if (StringUtils.isNotEmpty(unit)) {
                period = period.plusHours(Integer.parseInt(unit));
            }

            unit = unitMatcher.group(8);
            if (StringUtils.isNotEmpty(unit)) {
                period = period.plusMinutes(Integer.parseInt(unit));
            }

            unit = unitMatcher.group(10);
            if (StringUtils.isNotEmpty(unit)) {
                period = period.plusSeconds(Integer.parseInt(unit));
            }

            return period;
        }

        return null;
    }
}
