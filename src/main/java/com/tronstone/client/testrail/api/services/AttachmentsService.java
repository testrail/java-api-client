package com.tronstone.client.testrail.api.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import com.tronstone.client.inner.annotations.MapName;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.testrail.api.models.beans.attachments.Attachment;
import com.tronstone.client.testrail.api.parameters.AttachmentsParametersProvider;
import com.tronstone.client.testrail.api.services.pagination.ServicePaginator;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.entity.ContentType;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.List;

/**
 * Use the following API methods to upload, retrieve and delete attachments.
 */
public class AttachmentsService extends AbstractTestRailService {
    private static final String ATTACHMENT_BOUNDARY = "TestRailAPIAttachment";

    public AttachmentsService(TestRailClientConfiguration configuration) {
        super(configuration);
    }

    /**
     * Adds an attachment to a test plan.
     * The maximum allowable upload size is set to 256MB.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#addattachmenttocase">Add Attachment to Case</a>
     * @param testCaseID the ID of the test case the attachment should be added to
     * @param attachment attachment byte array
     * @param attachmentName name of attachment
     * @return String – the ID of the attachment uploaded to TestRail
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_attachment_to_case/{case_id}")
    public String addAttachmentToCase(@MapName("case_id") int testCaseID, byte[] attachment, String attachmentName) {
        Preconditions.checkArgument(testCaseID > 0, "Test case ID should be positive number");
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(attachment), "Attachment should not be empty or null");
        Preconditions.checkArgument(StringUtils.isNotEmpty(attachmentName), "Attachment name should be not empty or null");

        return getObjectMapper().readTree(
                getRequester().prepareRequest().addHeader(
                        HttpHeaders.CONTENT_TYPE,
                        ContentType.MULTIPART_FORM_DATA.getMimeType() + "; boundary=" + ATTACHMENT_BOUNDARY
                ).bodyByteArray(prepareAttachment(attachment, attachmentName)).execute().returnContent().asString()
        ).get("attachment_id").asText();
    }

    /**
     * Adds an attachment to a test plan.
     * The maximum allowable upload size is set to 256MB.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#addattachmenttocase">Add Attachment to Case</a>
     * @param testCaseID the ID of the test case the attachment should be added to
     * @param attachment attachment file
     * @return String – the ID of the attachment uploaded to TestRail
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_attachment_to_case/{case_id}")
    public String addAttachmentToCase(@MapName("case_id") int testCaseID, File attachment) {
        Preconditions.checkArgument(testCaseID > 0, "Test case ID should be positive number");
        Preconditions.checkNotNull(attachment, "Attachment should not be null");

        return getObjectMapper().readTree(
                getRequester().prepareRequest().addHeader(
                        HttpHeaders.CONTENT_TYPE,
                        ContentType.MULTIPART_FORM_DATA.getMimeType() + "; boundary=" + ATTACHMENT_BOUNDARY
                ).bodyByteArray(prepareAttachment(attachment)).execute().returnContent().asString()
        ).get("attachment_id").asText();
    }

    /**
     * Adds an attachment to a test plan.
     * The maximum allowable upload size is set to 256MB.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#addattachmenttoplan">Add Attachment to Plan</a>
     * @param planID the ID of the test plan the attachment should be added to
     * @param attachment attachment byte array
     * @param attachmentName name of attachment
     * @return String – the ID of the attachment uploaded to TestRail
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_attachment_to_plan/{plan_id}")
    public String addAttachmentToPlan(@MapName("plan_id") int planID, byte[] attachment, String attachmentName) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(attachment), "Attachment should not be empty or null");
        Preconditions.checkArgument(StringUtils.isNotEmpty(attachmentName), "Attachment name should be not empty or null");

        return getObjectMapper().readTree(
                getRequester().prepareRequest().addHeader(
                        HttpHeaders.CONTENT_TYPE,
                        ContentType.MULTIPART_FORM_DATA.getMimeType() + "; boundary=" + ATTACHMENT_BOUNDARY
                ).bodyByteArray(prepareAttachment(attachment, attachmentName)).execute().returnContent().asString()
        ).get("attachment_id").asText();
    }

    /**
     * Adds an attachment to a test plan.
     * The maximum allowable upload size is set to 256MB.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#addattachmenttocase">Add Attachment to Case</a>
     * @param planID the ID of the test plan the attachment should be added to
     * @param attachment attachment file
     * @return String – the ID of the attachment uploaded to TestRail
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_attachment_to_plan/{plan_id}")
    public String addAttachmentToPlan(@MapName("plan_id") int planID, File attachment) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");
        Preconditions.checkNotNull(attachment, "Attachment should not be null");

        return getObjectMapper().readTree(
                getRequester().prepareRequest().addHeader(
                        HttpHeaders.CONTENT_TYPE,
                        ContentType.MULTIPART_FORM_DATA.getMimeType() + "; boundary=" + ATTACHMENT_BOUNDARY
                ).bodyByteArray(prepareAttachment(attachment)).execute().returnContent().asString()
        ).get("attachment_id").asText();
    }

    /**
     * Adds an attachment to a test plan.
     * The maximum allowable upload size is set to 256MB.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#addattachmenttoplanentry">Add Attachment to Entry</a>
     * @param planID the ID of the test plan the attachment should be added to
     * @param entryID the ID of the test plan entry the attachment should be added to
     * @param attachment attachment byte array
     * @param attachmentName name of attachment
     * @return String – the ID of the attachment uploaded to TestRail
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_attachment_to_plan_entry/{plan_id}/{entry_id}")
    public String addAttachmentToEntry(@MapName("plan_id") int planID, @MapName("entry_id") String entryID, byte[] attachment, String attachmentName) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");
        Preconditions.checkArgument(StringUtils.isNotEmpty(entryID), "Entry ID should not be empty or null");
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(attachment), "Attachment should not be empty or null");
        Preconditions.checkArgument(StringUtils.isNotEmpty(attachmentName), "Attachment name should be not empty or null");

        return getObjectMapper().readTree(
                getRequester().prepareRequest().addHeader(
                        HttpHeaders.CONTENT_TYPE,
                        ContentType.MULTIPART_FORM_DATA.getMimeType() + "; boundary=" + ATTACHMENT_BOUNDARY
                ).bodyByteArray(prepareAttachment(attachment, attachmentName)).execute().returnContent().asString()
        ).get("attachment_id").asText();
    }

    /**
     * Adds an attachment to a test plan.
     * The maximum allowable upload size is set to 256MB.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#addattachmenttoplanentry">Add Attachment to Entry</a>
     * @param planID the ID of the test plan the attachment should be added to
     * @param entryID the ID of the test plan entry the attachment should be added to
     * @param attachment attachment file
     * @return String – the ID of the attachment uploaded to TestRail
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_attachment_to_plan_entry/{plan_id}/{entry_id}")
    public String addAttachmentToEntry(@MapName("plan_id") int planID, @MapName("entry_id") String entryID, File attachment) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");
        Preconditions.checkArgument(StringUtils.isNotEmpty(entryID), "Entry ID should not be empty or null");
        Preconditions.checkNotNull(attachment, "Attachment should not be null");

        return getObjectMapper().readTree(
                getRequester().prepareRequest().addHeader(
                        HttpHeaders.CONTENT_TYPE,
                        ContentType.MULTIPART_FORM_DATA.getMimeType() + "; boundary=" + ATTACHMENT_BOUNDARY
                ).bodyByteArray(prepareAttachment(attachment)).execute().returnContent().asString()
        ).get("attachment_id").asText();
    }

    /**
     * Adds an attachment to a result based on the result ID.
     * The maximum allowable upload size is set to 256MB.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#addattachmenttoresult">Add Attachment to Result</a>
     * @param resultID the ID of the test result the attachment should be added to
     * @param attachment attachment byte array
     * @param attachmentName name of attachment
     * @return String – the ID of the attachment uploaded to TestRail
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_attachment_to_result/{result_id}")
    public String addAttachmentToResult(@MapName("result_id") int resultID, byte[] attachment, String attachmentName) {
        Preconditions.checkArgument(resultID > 0, "Result ID should be positive number");
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(attachment), "Attachment should not be empty or null");
        Preconditions.checkArgument(StringUtils.isNotEmpty(attachmentName), "Attachment name should be not empty or null");

        return getObjectMapper().readTree(
                getRequester().prepareRequest().addHeader(
                        HttpHeaders.CONTENT_TYPE,
                        ContentType.MULTIPART_FORM_DATA.getMimeType() + "; boundary=" + ATTACHMENT_BOUNDARY
                ).bodyByteArray(prepareAttachment(attachment, attachmentName)).execute().returnContent().asString()
        ).get("attachment_id").asText();
    }

    /**
     * Adds an attachment to a result based on the result ID.
     * The maximum allowable upload size is set to 256MB.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#addattachmenttoresult">Add Attachment to Result</a>
     * @param resultID the ID of the test result the attachment should be added to
     * @param attachment attachment file
     * @return String – the ID of the attachment uploaded to TestRail
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_attachment_to_result/{result_id}")
    public String addAttachmentToResult(@MapName("result_id") int resultID, File attachment) {
        Preconditions.checkArgument(resultID > 0, "Result ID should be positive number");
        Preconditions.checkNotNull(attachment, "Attachment should not be null");

        return getObjectMapper().readTree(
                getRequester().prepareRequest().addHeader(
                        HttpHeaders.CONTENT_TYPE,
                        ContentType.MULTIPART_FORM_DATA.getMimeType() + "; boundary=" + ATTACHMENT_BOUNDARY
                ).bodyByteArray(prepareAttachment(attachment)).execute().returnContent().asString()
        ).get("attachment_id").asText();
    }

    /**
     * Adds an attachment to test run.
     * The maximum allowable upload size is set to 256MB.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#addattachmenttorun">Add Attachment to Run</a>
     * @param runID the ID of the test run the attachment should be added to
     * @param attachment attachment byte array
     * @param attachmentName name of attachment
     * @return String – the ID of the attachment uploaded to TestRail
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_attachment_to_run/{run_id}")
    public String addAttachmentToRun(@MapName("run_id") int runID, byte[] attachment, String attachmentName) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(attachment), "Attachment should not be empty or null");
        Preconditions.checkArgument(StringUtils.isNotEmpty(attachmentName), "Attachment name should be not empty or null");

        return getObjectMapper().readTree(
                getRequester().prepareRequest().addHeader(
                        HttpHeaders.CONTENT_TYPE,
                        ContentType.MULTIPART_FORM_DATA.getMimeType() + "; boundary=" + ATTACHMENT_BOUNDARY
                ).bodyByteArray(prepareAttachment(attachment, attachmentName)).execute().returnContent().asString()
        ).get("attachment_id").asText();
    }

    /**
     * Adds an attachment to test run.
     * The maximum allowable upload size is set to 256MB.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#addattachmenttorun">Add Attachment to Run</a>
     * @param runID the ID of the test run the attachment should be added to
     * @param attachment attachment file
     * @return String – the ID of the attachment uploaded to TestRail
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_attachment_to_run/{run_id}")
    public String addAttachmentToRun(@MapName("run_id") int runID, File attachment) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");
        Preconditions.checkNotNull(attachment, "Attachment should not be null");

        return getObjectMapper().readTree(
                getRequester().prepareRequest().addHeader(
                        HttpHeaders.CONTENT_TYPE,
                        ContentType.MULTIPART_FORM_DATA.getMimeType() + "; boundary=" + ATTACHMENT_BOUNDARY
                ).bodyByteArray(prepareAttachment(attachment)).execute().returnContent().asString()
        ).get("attachment_id").asText();
    }

    /**
     * Returns a list of attachments for a test case.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#getattachmentsforcase">Get Attachments for Case</a>
     * @param caseID the ID of the test case to retrieve attachments from
     * @return ServicePaginator<Attachment> – data sets pagination service for attachments
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_attachments_for_case/{case_id}")
    public ServicePaginator<Attachment> getAttachmentsForCase(@MapName("case_id") int caseID) {
        Preconditions.checkArgument(caseID > 0, "Case ID should be positive number");
        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Attachment.class);
    }

    /**
     * Returns a list of filtered attachments for a test case.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#getattachmentsforcase">Get Attachments for Case</a>
     * @param caseID the ID of the test case to retrieve attachments from
     * @param parametersProvider request filter parameters object
     * @return ServicePaginator<Attachment> – data sets pagination service for attachments
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_attachments_for_case/{case_id}")
    public ServicePaginator<Attachment> getAttachmentsForCase(@MapName("case_id") int caseID, AttachmentsParametersProvider parametersProvider) {
        Preconditions.checkArgument(caseID > 0, "case ID should be positive number");
        Preconditions.checkNotNull(parametersProvider, "Attachments parameters provider should not be null");

        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Attachment.class);
    }

    /**
     * Returns a list of attachments for a test plan.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#getattachmentsforplan">Get Attachments for Plan</a>
     * @param planID the ID of the test plan to retrieve attachments from
     * @return ServicePaginator<Attachment> – data sets pagination service for attachments
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_attachments_for_plan/{plan_id}")
    public ServicePaginator<Attachment> getAttachmentsForPlan(@MapName("plan_id") int planID) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");
        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Attachment.class);
    }

    /**
     * Returns a list of filtered attachments for a test plan.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#getattachmentsforplan">Get Attachments for Plan</a>
     * @param planID the ID of the test plan to retrieve attachments from
     * @param parametersProvider request filter parameters object
     * @return ServicePaginator<Attachment> – data sets pagination service for attachments
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_attachments_for_plan/{plan_id}")
    public ServicePaginator<Attachment> getAttachmentsForPlan(@MapName("plan_id") int planID, AttachmentsParametersProvider parametersProvider) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");
        Preconditions.checkNotNull(parametersProvider, "Attachments parameters provider should not be null");

        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Attachment.class);
    }

    /**
     * Returns a list of attachments for a test plan entry.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#getattachmentsforplanentry">Get Attachments for Plan Entry</a>
     * @param planID the ID of the test plan to retrieve attachments from
     * @param entryID the ID of the test plan entry to retrieve attachments from
     * @return List<Attachment> – list of attachments
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_attachments_for_plan_entry/{plan_id}/{entry_id}")
    public List<Attachment> getAttachmentsForPlanEntry(@MapName("plan_id") int planID, @MapName("entry_id") String entryID) {
        Preconditions.checkArgument(planID > 0, "Plan ID should be positive number");
        Preconditions.checkArgument(StringUtils.isNotEmpty(entryID), "Entry ID should not be empty or null");

        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), new TypeReference<List<Attachment>>() { });
    }

    /**
     * Returns a list of attachments for a test run.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#getattachmentsforrun">Get Attachments for Run</a>
     * @param runID the ID of the test run to retrieve attachments from
     * @return ServicePaginator<Attachment> – data sets pagination service for attachments
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_attachments_for_run/{run_id}")
    public ServicePaginator<Attachment> getAttachmentsForRun(@MapName("run_id") int runID) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");
        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Attachment.class);
    }

    /**
     * Returns a list of filtered attachments for a test run.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#getattachmentsforrun">Get Attachments for Run</a>
     * @param runID the ID of the test run to retrieve attachments from
     * @param parametersProvider request filter parameters object
     * @return ServicePaginator<Attachment> – data sets pagination service for attachments
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_attachments_for_run/{run_id}")
    public ServicePaginator<Attachment> getAttachmentsForRun(@MapName("run_id") int runID, AttachmentsParametersProvider parametersProvider) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");
        Preconditions.checkNotNull(parametersProvider, "Attachments parameters provider should not be null");

        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Attachment.class);
    }

    /**
     * Returns a list of attachments for a test’s results.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#getattachmentsfortest">Get Attachments for Test</a>
     * @param testID the ID of the test to retrieve attachments from
     * @return List<Attachment> – list of attachments
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_attachments_for_test/{test_id}")
    public List<Attachment> getAttachmentsForTest(@MapName("test_id") int testID) {
        Preconditions.checkArgument(testID > 0, "Test ID should be positive number");
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), new TypeReference<List<Attachment>>() { });
    }

    /**
     * Retrieves the requested file identified by attachment_id.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#getattachment">Get Attachment</a>
     * @param attachmentID the ID of the test to retrieve attachments from
     * @return byte[] – bytes of attachment file
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_attachment/{attachment_id}")
    public byte[] getAttachment(@MapName("attachment_id") String attachmentID) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(attachmentID), "Attachment ID should be not empty or null");
        return getRequester().prepareRequest().execute().returnContent().asBytes();
    }

    /**
     * Retrieves the requested file identified by attachment_id.
     * Attachment file will be created in default temporary files' directory.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#getattachment">Get Attachment</a>
     * @param attachmentID the ID of the test to retrieve attachments from
     * @return File – attachment file
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_attachment/{attachment_id}")
    public File getAttachmentFile(@MapName("attachment_id") String attachmentID) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(attachmentID), "Attachment ID should be not empty or null");

        File tempAttachmentFile = File.createTempFile(Long.toString(System.currentTimeMillis()), ATTACHMENT_BOUNDARY);
        Files.write(getRequester().prepareRequest().execute().returnContent().asBytes(), tempAttachmentFile);
        return tempAttachmentFile;
    }

    /**
     * Deletes the specified attachment identified by attachment_id.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments#deleteattachment">Delete Attachment</a>
     * @param attachmentID the ID of the attachment to delete
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "delete_attachment/{attachment_id}")
    public void deleteAttachment(@MapName("attachment_id") String attachmentID) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(attachmentID), "Attachment ID should be not empty or null");
        getRequester().prepareRequest().addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString()).execute();
    }

    @SneakyThrows
    private byte[] prepareAttachment(byte[] attachment, String attachmentName) {
        @Cleanup ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
        @Cleanup BufferedWriter bodyWriter = new BufferedWriter(new OutputStreamWriter(byteOutputStream));

        bodyWriter.write("\n\n--" + ATTACHMENT_BOUNDARY + "\r\n");
        bodyWriter.write("Content-Disposition: form-data; name=\"attachment\"; filename=\"" + attachmentName + "\"");
        bodyWriter.write("\r\n\r\n");
        bodyWriter.flush();

        byteOutputStream.write(attachment);

        byteOutputStream.flush();
        bodyWriter.write("\r\n--" + ATTACHMENT_BOUNDARY + "--\r\n");
        bodyWriter.flush();

        return byteOutputStream.toByteArray();
    }

    @SneakyThrows
    private byte[] prepareAttachment(File attachment) {
        @Cleanup ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
        @Cleanup InputStream fileInputStream = java.nio.file.Files.newInputStream(attachment.toPath());

        int bytesRead;
        byte[] dataBuffer = new byte[1024];

        while ((bytesRead = fileInputStream.read(dataBuffer)) != -1) {
            byteOutputStream.write(dataBuffer, 0, bytesRead);
        }

        byteOutputStream.flush();
        return prepareAttachment(byteOutputStream.toByteArray(), attachment.getName());
    }
}
