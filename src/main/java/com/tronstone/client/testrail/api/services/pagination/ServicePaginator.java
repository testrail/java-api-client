package com.tronstone.client.testrail.api.services.pagination;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.inner.service.ServiceFactory;
import com.tronstone.client.testrail.api.models.beans.pagination.PaginationData;
import com.tronstone.client.testrail.api.services.AbstractTestRailService;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.fluent.Request;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * The class provides a convenient interface
 * for working with a large data set that uses the TestRail pagination mechanism.
 *
 * @param <T> iterable object type
 */
public class ServicePaginator<T> extends AbstractTestRailService implements Iterable<List<T>> {
    private final Request initialRequest;
    private final Class<T> paginationClass;

    public ServicePaginator(TestRailClientConfiguration configuration, Request initialRequest, Class<T> paginationClass) {
        super(configuration);

        this.initialRequest = initialRequest;
        this.paginationClass = paginationClass;
    }

    @Override
    public Iterator<List<T>> iterator() {
        return ServiceFactory.createService(ServiceIterator.class, getConfiguration(), getObjectMapper(), initialRequest, paginationClass);
    }

    /**
     * Returns first data set for iterable objects.
     *
     * @return List<T> – list of iterable objects
     */
    public List<T> getFirst() {
        return iterator().next();
    }

    /**
     * Collect and provide all data sets for iterable objects.
     *
     * @return List<T> – list of iterable objects
     */
    public List<T> getAll() {
        List<T> allData = new ArrayList<>();

        for (List<T> data : this) {
            allData.addAll(data);
        }

        return allData;
    }

    private static class ServiceIterator<T> extends AbstractTestRailService implements Iterator<List<T>> {
        private boolean isFirst;
        private final JavaType dataType;
        private final Request initialRequest;
        private PaginationData<T> paginationData;

        public ServiceIterator(TestRailClientConfiguration configuration, ObjectMapper objectMapper, Request initialRequest, Class<T> paginationClass) {
            super(configuration, objectMapper);

            this.initialRequest = initialRequest;

            isFirst = true;
            dataType = getObjectMapper().getTypeFactory().constructParametricType(PaginationData.class, paginationClass);
        }

        @Override
        public boolean hasNext() {
            return isFirst || StringUtils.isNotEmpty(paginationData.getNextURL());
        }

        @Override
        @SneakyThrows
        @RequesterMapping(method = HttpMethod.GET)
        public List<T> next() {
            if (isFirst) {
                isFirst = false;
                paginationData = getObjectMapper().readValue(initialRequest.execute().returnContent().asString(), dataType);
                return paginationData.getData();
            }

            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            paginationData = getObjectMapper().readValue(
                    getRequester().prepareRequest(StringUtils.removeStart(paginationData.getNextURL(), SERVICE_PREFIX))
                            .execute()
                            .returnContent()
                            .asString(),
                    dataType
            );

            return paginationData.getData();
        }
    }
}
