package com.tronstone.client.testrail.api.models.beans.cases;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;
import com.tronstone.client.inner.utils.BuilderUtils;
import com.tronstone.client.inner.utils.CollectionsUtils;
import com.tronstone.client.testrail.api.models.beans.fields.CustomField;
import com.tronstone.client.testrail.api.models.beans.fields.FieldType;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.EstimateDeserializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.ReferencesDeserializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.TimeOnDeserializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.serialize.EstimateSerializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.serialize.ReferencesSerializeConverter;
import com.tronstone.client.testrail.api.models.parsing.filters.EmptyListIncludeFilter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Period;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static com.tronstone.client.testrail.api.models.beans.fields.CustomField.CUSTOM_FIELD_SYSTEM_PREFIX;

@Getter
@ToString
@EqualsAndHashCode
@JsonInclude(Include.NON_DEFAULT)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Case {
    @JsonView({Case.class, CaseUpdate.class})
    private String title;

    @JsonView({Case.class, CaseUpdate.class})
    private int templateID;

    @JsonView({Case.class, CaseUpdate.class})
    private int typeID;

    @JsonView({Case.class, CaseUpdate.class})
    private int priorityID;

    @JsonView({Case.class, CaseUpdate.class})
    private int milestoneID = -1;

    @JsonView(CaseUpdate.class)
    private int sectionID;

    @JsonView({Case.class, CaseUpdate.class})
    @JsonSerialize(converter = EstimateSerializeConverter.class)
    @JsonDeserialize(converter = EstimateDeserializeConverter.class)
    private Period estimate = Period.ZERO;

    @JsonView({Case.class, CaseUpdate.class})
    @JsonSerialize(converter = ReferencesSerializeConverter.class)
    @JsonDeserialize(converter = ReferencesDeserializeConverter.class)
    @JsonInclude(value = Include.CUSTOM, valueFilter = EmptyListIncludeFilter.class)
    private final List<String> refs = new ArrayList<>();

    @JsonIgnore
    private final @Getter(AccessLevel.NONE) Map<String, CustomField> customFields = new HashMap<>();

    @JsonProperty(access = Access.WRITE_ONLY)
    private int id;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int suiteID;

    @JsonProperty(value = "created_by", access = Access.WRITE_ONLY)
    private int creatorID;

    @JsonProperty(value = "updated_by", access = Access.WRITE_ONLY)
    private int updaterID;

    @JsonProperty(access = Access.WRITE_ONLY)
    private int displayOrder;

    @JsonDeserialize(converter = TimeOnDeserializeConverter.class)
    @JsonProperty(access = Access.WRITE_ONLY)
    private DateTime createdOn;

    @JsonDeserialize(converter = TimeOnDeserializeConverter.class)
    @JsonProperty(access = Access.WRITE_ONLY)
    private DateTime updatedOn;

    @JsonDeserialize(converter = EstimateDeserializeConverter.class)
    @JsonProperty(access = Access.WRITE_ONLY)
    private Period estimateForecast;

    @JsonProperty(value = "is_deleted", access = Access.WRITE_ONLY)
    private boolean deleted;

    /**
     * Returns case object builder.
     *
     * @param title the title of the test case
     * @return CaseBuilder – case builder object
     */
    public static CaseBuilder construct(String title) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(title), "Title should not be empty or null");
        return new CaseBuilder(title);
    }

    public int getID() {
        return id;
    }

    /**
     * To get the value with correct type use {@link FieldType} mapped types.
     * Warning, label it is not unique value, if two custom field will
     * have the same value, will return first available. For more consistent
     * results use {@link Case#getCustomField(String, FieldType)}
     *
     * @param caseFieldType the custom field type
     * @param label the label of the field as it appears in the user interface
     * @param <T> type of custom field value
     * @return T – the value of the custom field or null if value not exists
     */
    @SuppressWarnings("unchecked")
    public <T> T getCustomField(FieldType<T> caseFieldType, String label) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(label), "Label should not be empty or null");

        CustomField customField = customFields.values()
                .stream()
                .filter(field -> field.getLabel().equalsIgnoreCase(label))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("The label does not match any of the existing custom fields"));

        Preconditions.checkArgument(
                customField.getFieldType().equals(caseFieldType),
                String.format(
                        "Case field type should be the same as type with containing field: expected type '%s' but actual '%s'",
                        caseFieldType.getName(),
                        customField.getFieldType().getName()
                )
        );

        return (T) customField.getFieldValue();
    }

    /**
     * To get the value with correct type use {@link FieldType} mapped types.
     *
     * @param systemName the unique name of custom case field in the database without 'custom_' prefix
     * @param caseFieldType the custom field type
     * @param <T> – type of custom field value
     * @return T – the value of the custom field or null if value not exists
     */
    @SuppressWarnings("unchecked")
    public <T> T getCustomField(String systemName, FieldType<T> caseFieldType) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(systemName), "System name should not be empty or null");
        Preconditions.checkArgument(
                !systemName.startsWith(CUSTOM_FIELD_SYSTEM_PREFIX),
                "System name should not have '%s' prefix",
                CUSTOM_FIELD_SYSTEM_PREFIX
        );

        CustomField customField = customFields.get(systemName);
        Preconditions.checkArgument(
                customField.getFieldType().equals(caseFieldType),
                String.format(
                        "Case field type should be the same as type with containing field: expected type '%s' but actual '%s'",
                        caseFieldType.getName(),
                        customField.getFieldType().getName()
                )
        );

        return (T) customField.getFieldValue();
    }

    @JsonAnyGetter
    @JsonView({Case.class, CaseUpdate.class})
    private Map<String, Object> serializeCustomFields() {
        return customFields.values().stream().collect(
                HashMap::new,
                (map, customField) -> map.put(customField.getPrefixedSystemName(), customField.getFieldValue()),
                HashMap::putAll
        );
    }

    @JsonAnySetter
    private void deserializeCustomField(String fieldSystemName, CustomField customField) {
        if (fieldSystemName.startsWith(CUSTOM_FIELD_SYSTEM_PREFIX)) {
            customFields.put(fieldSystemName.replaceFirst(CUSTOM_FIELD_SYSTEM_PREFIX, StringUtils.EMPTY), customField);
        } else {
            throw new UnsupportedOperationException("Unrecognized json property: " + fieldSystemName);
        }
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public static class CaseBuilder {
        private @NonNull String title;
        private int templateID;
        private int typeID;
        private int priorityID;
        private int milestoneID = -1;
        private Period estimate = Period.ZERO;
        private final List<String> refs = new ArrayList<>();
        private final Map<String, CustomField> customFields = new HashMap<>();

        /**
         * @param templateID the ID of the template (field layout)
         * @return CaseBuilder – case builder object
         */
        public CaseBuilder templateID(int templateID) {
            Preconditions.checkArgument(templateID > 0, "Template ID should be positive number");
            this.templateID = templateID;
            return this;
        }

        /**
         * @param typeID the ID of the case type
         * @return CaseBuilder – case builder object
         */
        public CaseBuilder typeID(int typeID) {
            Preconditions.checkArgument(typeID > 0, "Type ID should be positive number");
            this.typeID = typeID;
            return this;
        }

        /**
         * @param priorityID the ID of the case priority
         * @return CaseBuilder – case builder object
         */
        public CaseBuilder priorityID(int priorityID) {
            Preconditions.checkArgument(priorityID > 0, "Priority ID should be positive number");
            this.priorityID = priorityID;
            return this;
        }

        /**
         * @param milestoneID the ID of the milestone to link to the test case
         * @return CaseBuilder – case builder object
         */
        public CaseBuilder milestoneID(int milestoneID) {
            Preconditions.checkArgument(milestoneID > 0, "Milestone ID should be positive number");
            this.milestoneID = milestoneID;
            return this;
        }

        /**
         * Set the estimate period.
         *
         * @param estimate the estimate of the case testing
         * @return CaseBuilder – case builder object
         */
        public CaseBuilder estimate(Period estimate) {
            Preconditions.checkNotNull(estimate, "Estimate should not be null");
            Preconditions.checkArgument(!estimate.equals(Period.ZERO), "Estimate should not be zero");

            this.estimate = estimate;
            return this;
        }

        /**
         * Parse and set estimate time.
         *
         * @param estimate the estimate of the case testing, e.g. “30s” or “1m 45s”
         * @return CaseBuilder – case builder object
         */
        public CaseBuilder estimate(String estimate) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(estimate), "Estimate should not be empty or null");
            this.estimate = new EstimateDeserializeConverter().convert(estimate);
            return this;
        }

        /**
         * Add the list of references/requirements.
         *
         * @param refs list of references/requirements
         * @return CaseBuilder – case builder object
         */
        public CaseBuilder refs(List<String> refs) {
            Preconditions.checkArgument(CollectionUtils.isNotEmpty(refs), "References should not be empty or null");
            this.refs.addAll(refs);
            return this;
        }

        /**
         * Parse and add list of comma-separated references/requirements.
         *
         * @param refs a comma-separated list of references/requirements
         * @return CaseBuilder – case builder object
         */
        public CaseBuilder refs(String refs) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(refs), "References should not be empty or null");
            this.refs.addAll(new ReferencesDeserializeConverter().convert(refs));
            return this;
        }

        /**
         * Add the array of references/requirements.
         *
         * @param refs array of references/requirements
         * @return CaseBuilder – case builder object
         */
        public CaseBuilder refs(String... refs) {
            Preconditions.checkArgument(ArrayUtils.isNotEmpty(refs), "References should not be empty or null");

            this.refs.addAll(Arrays.stream(refs)
                    .peek(ref -> Preconditions.checkArgument(StringUtils.isNotEmpty(ref), "Reference should not be empty or null"))
                    .collect(Collectors.toList()));

            return this;
        }

        /**
         * Add custom field to test case.
         *
         * @param systemName the unique name of custom case field in the database without 'custom_' prefix
         * @param caseFieldType the custom field type
         * @param value the custom field value data
         * @param <T> type of custom field value
         * @return CaseBuilder – case builder object
         */
        public <T> CaseBuilder addCustomField(String systemName, FieldType<T> caseFieldType, T value) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(systemName), "System name should not be empty or null");
            Preconditions.checkNotNull(caseFieldType, "Case field type should not be null");
            Preconditions.checkNotNull(value, "Value should not be null");
            Preconditions.checkArgument(!systemName.startsWith(CUSTOM_FIELD_SYSTEM_PREFIX), "System name should not have 'custom_' prefix");
            Preconditions.checkArgument(!caseFieldType.equals(FieldType.STEP_RESULTS), "'Steps Results' is not supporting for add custom field");

            customFields.put(systemName, new CustomField(CUSTOM_FIELD_SYSTEM_PREFIX + systemName, caseFieldType, value));
            return this;
        }

        /**
         * Build ready case object.
         *
         * @return Case – constructed case object
         */
        public Case build() {
            Case testCase = new Case();

            testCase.title = title;
            testCase.templateID = templateID;
            testCase.typeID = typeID;
            testCase.priorityID = priorityID;
            testCase.milestoneID = milestoneID;
            testCase.estimate = estimate;
            testCase.refs.addAll(refs);
            testCase.customFields.putAll(customFields);

            return testCase;
        }
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class CaseUpdate extends Case {
        /**
         * Returns case object builder for update operations.
         *
         * @return CaseUpdateBuilder – case update builder object
         */
        public static CaseUpdateBuilder constructUpdate() {
            return new CaseUpdateBuilder();
        }

        /**
         * Due to references and custom fields are completely overwritten,
         * it will be useful to have a complete collection of these elements.
         * When updating, these elements will already be present when building the query.
         *
         * @param testCase pre-filled test case
         * @return CaseUpdateBuilder – case update builder object
         */
        public static CaseUpdateBuilder constructUpdate(Case testCase) {
            return new CaseUpdateBuilder(testCase.refs, testCase.customFields);
        }

        @SuppressWarnings("FieldCanBeLocal")
        @NoArgsConstructor(access = AccessLevel.PRIVATE)
        public static class CaseUpdateBuilder {
            private String title;
            private int sectionID;
            private int templateID;
            private int typeID;
            private int priorityID;
            private int milestoneID = -1;
            private Period estimate = Period.ZERO;
            private List<String> refs = new ArrayList<>();
            private Map<String, CustomField> customFields = new HashMap<>();

            private CaseUpdateBuilder(List<String> refs, Map<String, CustomField> customFields) {
                this.refs = refs;
                this.customFields = customFields;
            }

            /**
             * @param title the title of the test case
             * @return CaseUpdateBuilder – case builder object
             */
            public CaseUpdateBuilder title(String title) {
                Preconditions.checkArgument(StringUtils.isNotEmpty(title), "Title should not be empty or null");
                this.title = title;
                return this;
            }

            /**
             * @param sectionID the ID of the section the test case should be updated to
             * @return CaseUpdateBuilder – case builder object
             */
            public CaseUpdateBuilder sectionID(int sectionID) {
                Preconditions.checkArgument(sectionID > 0, "Section ID should be positive number");
                this.sectionID = sectionID;
                return this;
            }

            /**
             * @param templateID the ID of the template (field layout)
             * @return CaseUpdateBuilder – case builder object
             */
            public CaseUpdateBuilder templateID(int templateID) {
                Preconditions.checkArgument(templateID > 0, "Template ID should be positive number");
                this.templateID = templateID;
                return this;
            }

            /**
             * Use 0 for removing milestone.
             *
             * @param milestoneID the ID of the milestone to link to the test case
             * @return CaseUpdateBuilder – case builder object
             */
            public CaseUpdateBuilder milestoneID(int milestoneID) {
                Preconditions.checkArgument(templateID > -1, "Milestone ID should be zero or positive number");
                this.milestoneID = milestoneID;
                return this;
            }

            /**
             * @param typeID the ID of the case type
             * @return CaseUpdateBuilder – case builder object
             */
            public CaseUpdateBuilder typeID(int typeID) {
                Preconditions.checkArgument(typeID > 0, "Type ID should be positive number");
                this.typeID = typeID;
                return this;
            }

            /**
             * @param priorityID the ID of the case priority
             * @return CaseUpdateBuilder – case builder object
             */
            public CaseUpdateBuilder priorityID(int priorityID) {
                Preconditions.checkArgument(priorityID > 0, "Priority ID should be positive number");
                this.priorityID = priorityID;
                return this;
            }

            /**
             * Update the estimate period.
             * If the estimate already establish, it will be replaced.
             * Use {@link Period#ZERO} for removing estimate.
             *
             * @param estimate the estimate of the case testing
             * @return CaseUpdateBuilder – case builder object
             */
            public CaseUpdateBuilder estimate(Period estimate) {
                if (estimate.equals(Period.ZERO)) {
                    this.estimate = null;
                } else {
                    this.estimate = estimate;
                }

                return this;
            }

            /**
             * Parse and update estimate time.
             * If the estimate already establish, it will be replaced.
             * Use empty string for removing estimate.
             *
             * @param estimate the estimate of the case testing, e.g. “30s” or “1m 45s”
             * @return CaseUpdateBuilder – case builder object
             */
            public CaseUpdateBuilder estimate(String estimate) {
                this.estimate = new EstimateDeserializeConverter().convert(estimate);
                return this;
            }

            /**
             * Update the list of references/requirements.
             * If the refs already establish, it will be replaced.
             * Use {@link Collections#emptyList()} for removing references/requirements.
             *
             * @param refs list of references/requirements
             * @return CaseUpdateBuilder – case builder object
             */
            public CaseUpdateBuilder refs(List<String> refs) {
                Preconditions.checkNotNull(refs, "References should not be null");

                if (CollectionsUtils.isEmptyList(refs)) {
                    this.refs = refs;
                    return this;
                } else if (CollectionsUtils.isEmptyList(this.refs)) {
                    this.refs = new ArrayList<>();
                }

                this.refs.addAll(refs);
                return this;
            }

            /**
             * Parse and update list of comma-separated references/requirements.
             * If the refs already establish, it will be replaced.
             * Use empty string for removing references/requirements.
             *
             * @param refs a comma-separated list of references/requirements
             * @return CaseUpdateBuilder – case builder object
             */
            public CaseUpdateBuilder refs(String refs) {
                if (StringUtils.isEmpty(refs)) {
                    if (CollectionsUtils.isNotEmptyList(this.refs)) {
                        this.refs = Collections.emptyList();
                    }

                    return this;
                } else if (CollectionsUtils.isEmptyList(this.refs)) {
                    this.refs = new ArrayList<>();
                }

                this.refs.addAll(new ReferencesDeserializeConverter().convert(refs));
                return this;
            }

            /**
             * Update the array of references/requirements.
             * If the refs already establish, it will be replaced.
             *
             * @param refs array of references/requirements
             * @return CaseUpdateBuilder – case builder object
             */
            public CaseUpdateBuilder refs(String... refs) {
                Preconditions.checkArgument(ArrayUtils.isNotEmpty(refs), "References should not be empty or null");

                if (CollectionsUtils.isEmptyList(this.refs)) {
                    this.refs = new ArrayList<>();
                }

                this.refs.addAll(Arrays.stream(refs)
                        .peek(ref -> Preconditions.checkArgument(StringUtils.isNotEmpty(ref), "Reference should not be empty or null"))
                        .collect(Collectors.toList()));

                return this;
            }

            /**
             * Add custom field to test case.
             * In the case of updating an existing custom field,
             * the old value will be completely replaced by the new one.
             * Use null for removing custom field.
             *
             * @param systemName the unique name of custom case field in the database without 'custom_' prefix
             * @param caseFieldType the custom field type
             * @param value the custom field value data
             * @param <T> type of custom field value
             * @return CaseUpdateBuilder – case builder object
             */
            public <T> CaseUpdateBuilder addCustomField(String systemName, FieldType<T> caseFieldType, T value) {
                Preconditions.checkArgument(StringUtils.isNotEmpty(systemName), "System name should not be empty or null");
                Preconditions.checkNotNull(caseFieldType, "Case field type should not be null");
                Preconditions.checkArgument(!systemName.startsWith(CUSTOM_FIELD_SYSTEM_PREFIX), "System name should not have 'custom_' prefix");
                Preconditions.checkArgument(!caseFieldType.equals(FieldType.STEP_RESULTS), "'Steps Results' is not supporting for add custom field");

                customFields.put(systemName, new CustomField(CUSTOM_FIELD_SYSTEM_PREFIX + systemName, caseFieldType, value));
                return this;
            }

            /**
             * Build ready case update object.
             *
             * @return CaseUpdate – constructed case update object
             */
            @SneakyThrows
            public CaseUpdate build() {
                return BuilderUtils.fillWithBuilderFields(new CaseUpdate(), this);
            }
        }
    }
 }
