package com.tronstone.client.testrail.api.services;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Preconditions;
import com.tronstone.client.inner.annotations.MapName;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.testrail.api.models.beans.groups.Group;
import com.tronstone.client.testrail.api.services.pagination.ServicePaginator;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.SneakyThrows;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.entity.ContentType;

import java.util.Collections;
import java.util.Objects;

/**
 * Use the following API methods to request details about groups.
 */
public class GroupsService extends AbstractTestRailService {
    public GroupsService(TestRailClientConfiguration configuration) {
        super(configuration);
    }

    /**
     * Returns an existing group.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/groups#getgroup">Get Group</a>
     * @param groupID the ID of the group
     * @return Group – object representation of the group
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_group/{group_id}")
    public Group getGroup(@MapName("group_id") int groupID) {
        Preconditions.checkArgument(groupID > 0, "Group ID should be positive number");
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), Group.class);
    }

    /**
     * Returns the list of available groups.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/groups#getgroups">Get Groups</a>
     * @return ServicePaginator<Group> – data sets pagination service for groups
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_groups")
    public ServicePaginator<Group> getGroups() {
        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Group.class);
    }

    /**
     * Creates a new group.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/groups#addgroup">Add Group</a>
     * @param name the name of the section group
     * @param userIDs array of user ID's
     * @return Group – created group
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_group")
    public Group addGroup(String name, int... userIDs) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(userIDs), "User ID's should not be empty or null");

        ArrayNode userIDsNode = getObjectMapper().createArrayNode();
        for (int id : userIDs) {
            userIDsNode.add(id);
        }

        ObjectNode addGroupNode = getObjectMapper().createObjectNode();
        addGroupNode.put("name", name);
        addGroupNode.putArray("user_ids").addAll(userIDsNode);

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writeValueAsBytes(addGroupNode), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Group.class
        );
    }

    /**
     * Using update will set the group’s members to match the userIDs array provided.
     * It is not possible to add or remove users. The userIDs array submitted should always be
     * the full list of users in the group.
     * You can update only group name. Use null for removing user ID's.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/groups#updategroup">Update Group</a>
     * @param groupID the ID of the group
     * @param name the name of the section group
     * @param userIDs array of user ID's, can be empty or null
     * @return Group – updated group
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "update_group/{group_id}")
    public Group updateGroup(@MapName("group_id") int groupID, String name, int... userIDs) {
        Preconditions.checkArgument(groupID > 0, "Group ID should be positive number");
        Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");

        ObjectNode updateGroupNode = getObjectMapper().createObjectNode();
        updateGroupNode.put("group_id", groupID);
        updateGroupNode.put("name", name);

        if (Objects.isNull(userIDs)) {
            updateGroupNode.putArray("user_ids").addAll(Collections.emptyList());
        } else if (ArrayUtils.isNotEmpty(userIDs)) {
            ArrayNode userIDsNode = getObjectMapper().createArrayNode();

            for (int id : userIDs) {
                userIDsNode.add(id);
            }

            updateGroupNode.putArray("user_ids").addAll(userIDsNode);
        }

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writeValueAsBytes(updateGroupNode), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Group.class
        );
    }

    /**
     * Deletes an existing group.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/groups#deletegroup">Delete Group</a>
     * @param groupID the ID of the group
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "delete_group/{group_id}")
    public void deleteGroup(@MapName("group_id") int groupID) {
        Preconditions.checkArgument(groupID > 0, "Group ID should be positive number");
        getRequester().prepareRequest().addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType()).execute();
    }
}
