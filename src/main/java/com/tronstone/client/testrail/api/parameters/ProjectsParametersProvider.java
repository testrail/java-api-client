package com.tronstone.client.testrail.api.parameters;

import lombok.NoArgsConstructor;
import org.apache.commons.lang3.BooleanUtils;

@NoArgsConstructor(staticName = "prepare")
public class ProjectsParametersProvider extends PaginationParametersProvider<ProjectsParametersProvider> {
    private final StringBuilder parametersBuilder = getParametersBuilder();

    /**
     * True to return completed projects only. False to return active projects only.
     *
     * @param completed is completed flag
     * @return ProjectsParametersProvider – provider object
     */
    public ProjectsParametersProvider isCompleted(boolean completed) {
        parametersBuilder.append("&is_completed=").append(BooleanUtils.toInteger(completed));
        return this;
    }
}
