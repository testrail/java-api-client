package com.tronstone.client.testrail.api.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.tronstone.client.inner.annotations.MapName;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.inner.service.ServiceFactory;
import com.tronstone.client.testrail.api.models.beans.cases.Case;
import com.tronstone.client.testrail.api.models.beans.cases.Case.CaseUpdate;
import com.tronstone.client.testrail.api.models.beans.cases.history.CaseHistory;
import com.tronstone.client.testrail.api.models.beans.deletion.DeletionData;
import com.tronstone.client.testrail.api.models.beans.fields.Field;
import com.tronstone.client.testrail.api.models.parsing.modules.CustomFieldModule;
import com.tronstone.client.testrail.api.parameters.CaseHistoryParametersProvider;
import com.tronstone.client.testrail.api.parameters.CasesParametersProvider;
import com.tronstone.client.testrail.api.services.pagination.ServicePaginator;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.http.entity.ContentType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Use the following API methods to retrieve data about test cases-like title,
 * case fields, and history-and to create, modify, or delete test cases.
 */
public class CasesService extends AbstractTestRailService {
    private final boolean cached;
    private final CaseFieldsService caseFieldsService;
    private final LoadingCache<CaseFieldsService, Map<String, Field>> cache;

    public CasesService(TestRailClientConfiguration configuration, boolean cached) {
        super(configuration);

        this.cached = cached;
        caseFieldsService = ServiceFactory.createService(CaseFieldsService.class, configuration);
        cache = CacheBuilder.newBuilder().build(new CacheLoader<CaseFieldsService, Map<String, Field>>() {
            @NonNull
            @Override
            public Map<String, Field> load(@NonNull CaseFieldsService caseFieldsService) {
                return caseFieldsService.getCaseFields().stream().reduce(new HashMap<>(), (caseFields, caseField) -> {
                    caseFields.put(caseField.getSystemName(), caseField);
                    return caseFields;
                }, (caseFields, parallelCaseFieldTypes) -> caseFields);
            }
        });
    }

    /**
     * Returns an existing test case.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases#getcase">Get Case</a>
     * @param testCaseID the ID of the test case
     * @return Case – object representation of the test case
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_case/{case_id}")
    public Case getCase(@MapName("case_id") int testCaseID) {
        Preconditions.checkArgument(testCaseID > 0, "Test case ID should be positive number");
        return getObjectMapper().registerModule(new CustomFieldModule(getCaseFields())).readValue(getRequester().prepareAndExecuteRequest(), Case.class);
    }

    /**
     * Returns a list of test cases for a project in single suite mode.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases#getcases">Get Cases</a>
     * @param projectID the ID of the project
     * @return ServicePaginator<Case> – data sets pagination service for cases
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_cases/{project_id}")
    public ServicePaginator<Case> getCases(@MapName("project_id") int projectID) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");

        return new ServicePaginator<Case>(getConfiguration(), getRequester().prepareRequest(), Case.class) {{
            getObjectMapper().registerModule(new CustomFieldModule(getCaseFields()));
        }};
    }

    /**
     * Returns a list of test cases for a project or
     * specific test suite (if the project has multiple suites enabled).
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases#getcases">Get Cases</a>
     * @param projectID the ID of the project
     * @param suiteID the ID of the test suite
     * @return ServicePaginator<Case> – data sets pagination service for cases
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_cases/{project_id}&suite_id={suite_id}")
    public ServicePaginator<Case> getCases(@MapName("project_id") int projectID, @MapName("suite_id") int suiteID) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        Preconditions.checkArgument(suiteID > 0, "Suite ID should be positive number");

        return new ServicePaginator<Case>(getConfiguration(), getRequester().prepareRequest(), Case.class) {{
            getObjectMapper().registerModule(new CustomFieldModule(getCaseFields()));
        }};
    }

    /**
     * Returns a list of filtered test cases for a project.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases#getcases">Get Cases</a>
     * @param projectID the ID of the project
     * @param parametersProvider request filter parameters object
     * @return ServicePaginator<Case> – data sets pagination service for cases
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_cases/{project_id}")
    public ServicePaginator<Case> getCases(@MapName("project_id") int projectID, CasesParametersProvider parametersProvider) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        Preconditions.checkNotNull(parametersProvider, "Cases parameters provider should not be null");

        return new ServicePaginator<Case>(getConfiguration(), getRequester().prepareRequest(), Case.class) {{
            getObjectMapper().registerModule(new CustomFieldModule(getCaseFields()));
        }};
    }

    /**
     * Returns the edit history for a test.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases#gethistoryforcase">Get History for Case</a>
     * @param testCaseID the ID of the test case
     * @return ServicePaginator<CaseHistory> – data sets pagination service for case history
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_history_for_case/{case_id}")
    public ServicePaginator<CaseHistory> getHistoryForCase(@MapName("case_id") int testCaseID) {
        Preconditions.checkArgument(testCaseID > 0, "Test case ID should be positive number");
        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), CaseHistory.class);
    }

    /**
     * Returns the filtered edit history for a test.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases#gethistoryforcase">Get History for Case</a>
     * @param testCaseID the ID of the test case
     * @param parametersProvider request filter parameters object
     * @return ServicePaginator<CaseHistory> – data sets pagination service for case history
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_history_for_case/{case_id}")
    public ServicePaginator<CaseHistory> getHistoryForCase(@MapName("case_id") int testCaseID, CaseHistoryParametersProvider parametersProvider) {
        Preconditions.checkArgument(testCaseID > 0, "Test case ID should be positive number");
        Preconditions.checkNotNull(parametersProvider, "Case history parameters provider should not be null");

        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), CaseHistory.class);
    }

    /**
     * Creates a new test case.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases#addcase">Add Case</a>
     * @param sectionID the ID of the section the test case should be added to
     * @param testCase new test case
     * @return Case – created test case
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_case/{section_id}")
    public Case addCase(@MapName("section_id") int sectionID, Case testCase) {
        Preconditions.checkArgument(sectionID > 0, "Section ID should be positive number");
        Preconditions.checkNotNull(testCase, "Case should not be null");

        return getObjectMapper().registerModule(new CustomFieldModule(getCaseFields())).readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writerWithView(Case.class).writeValueAsBytes(testCase), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Case.class
        );
    }

    /**
     * Copies the list of cases to another suite/section.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases#copycasestosection">Copy Cases to Section</a>
     * @param sectionID the ID of the section the test case should be copied to
     * @param testCaseIDs array of test case ID's
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "copy_cases_to_section/{section_id}")
    public void copyCasesToSection(@MapName("section_id") int sectionID, int... testCaseIDs) {
        Preconditions.checkArgument(sectionID > 0, "Section ID should be positive number");
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(testCaseIDs), "Test case ID's should not be empty or null");

        ArrayNode testCaseIDsNode = getObjectMapper().createArrayNode();
        for (int id : testCaseIDs) {
            testCaseIDsNode.add(id);
        }

        ObjectNode copyCasesNode = getObjectMapper().createObjectNode();
        copyCasesNode.putArray("case_ids").addAll(testCaseIDsNode);

        getRequester().prepareRequest()
                .bodyByteArray(getObjectMapper().writeValueAsBytes(copyCasesNode), ContentType.APPLICATION_JSON)
                .execute();
    }

    /**
     * Updates an existing test case (partial updates are supported,
     * i.e. you can submit and update specific fields only).
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases#updatecase">Update Case</a>
     * @param testCaseID the ID of the test case
     * @param caseUpdate test case that to be updated
     * @return Case – updated test case
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "update_case/{case_id}")
    public Case updateCase(@MapName("case_id") int testCaseID, CaseUpdate caseUpdate) {
        Preconditions.checkArgument(testCaseID > 0, "Case ID should be positive number");
        Preconditions.checkNotNull(caseUpdate, "Case update should not be null");

        return getObjectMapper().registerModule(new CustomFieldModule(getCaseFields())).readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writerWithView(CaseUpdate.class).writeValueAsBytes(caseUpdate), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Case.class
        );
    }

    /**
     * Updates multiple test cases with the same values,
     * like setting a set of test cases to “High” priority.
     * This does not support updating multiple test cases
     * with different values per test case.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases#updatecases">Update Cases</a>
     * @param suiteID the ID of the test suite
     * @param caseUpdate test case fields set that to be updated
     * @param testCaseIDs the ID of the cases that need to be updated
     * @return List<Case> – list of updated test cases
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "update_cases/{suite_id}")
    public List<Case> updateCases(@MapName("suite_id") int suiteID, CaseUpdate caseUpdate, int... testCaseIDs) {
        Preconditions.checkArgument(suiteID > 0, "Suite ID should be positive number");
        Preconditions.checkNotNull(caseUpdate, "Case update should not be null");
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(testCaseIDs), "Test case ID's should not be empty or null");

        ArrayNode testCaseIDsNode = getObjectMapper().createArrayNode();
        for (int id : testCaseIDs) {
            testCaseIDsNode.add(id);
        }

        ObjectNode caseUpdateNode = (ObjectNode) getObjectMapper().readTree(getObjectMapper().writerWithView(CaseUpdate.class).writeValueAsBytes(caseUpdate));
        caseUpdateNode.putArray("case_ids").addAll(testCaseIDsNode);

        return getObjectMapper().registerModule(new CustomFieldModule(getCaseFields())).treeToValue(
                getObjectMapper().readTree(getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writeValueAsBytes(caseUpdateNode), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString()
                ).get("updated_cases"),
                TypeFactory.defaultInstance().constructType(new TypeReference<List<Case>>() { })
        );
    }

    /**
     * Moves cases to another suite or section.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases#movecasestosection">Move Cases to Section</a>
     * @param suiteID the ID of the suite the case will be moved to
     * @param sectionID the ID of the section the case will be moved to
     * @param testCaseIDs the ID of the cases that need to be moved
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "move_cases_to_section/{section_id}")
    public void moveCasesToSection(@MapName("suite_id") int suiteID, @MapName("section_id") int sectionID, int... testCaseIDs) {
        Preconditions.checkArgument(suiteID > 0, "Suite ID should be positive number");
        Preconditions.checkArgument(sectionID > 0, "Section ID should be positive number");
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(testCaseIDs), "Test case ID's should not be empty or null");

        ArrayNode testCaseIDsNode = getObjectMapper().createArrayNode();
        for (int id : testCaseIDs) {
            testCaseIDsNode.add(id);
        }

        ObjectNode moveCasesNode = getObjectMapper().createObjectNode();
        moveCasesNode.put("suite_id", suiteID);
        moveCasesNode.putArray("case_ids").addAll(testCaseIDsNode);

        getRequester().prepareRequest()
                .bodyByteArray(getObjectMapper().writeValueAsBytes(moveCasesNode), ContentType.APPLICATION_JSON)
                .execute();
    }

    /**
     * Deletes an existing test case.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases#deletecase">Delete Case</a>
     * @param testCaseID the ID of the test case to be deleted
     * @param soft when true will return information about the data which will be deleted but will not proceed with the deletion
     * @return DeletionData – data on the number of affected set when soft is true, in deletion case return null
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "delete_case/{case_id}")
    public DeletionData deleteCase(@MapName("case_id") int testCaseID, boolean soft) {
        Preconditions.checkArgument(testCaseID > 0, "Test case ID should be positive number");

        ObjectNode deleteCaseNode = getObjectMapper().createObjectNode();
        deleteCaseNode.put("soft", BooleanUtils.toInteger(soft));

        JsonNode responseNode = getObjectMapper().readTree(getRequester().prepareRequest()
                .bodyByteArray(getObjectMapper().writeValueAsBytes(deleteCaseNode), ContentType.APPLICATION_JSON)
                .execute()
                .returnContent()
                .asString()
        );

        if (responseNode.isEmpty()) {
            return null;
        }

        return getObjectMapper().treeToValue(responseNode, DeletionData.class);
    }

    /**
     * Deletes an existing test case.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases#deletecase">Delete Case</a>
     * @param testCaseID the ID of the test case to be deleted
     */
    public void deleteCase(int testCaseID) {
        deleteCase(testCaseID, false);
    }

    /**
     * Deletes multiple test cases from a project or test suite.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases#deletecases">Delete Cases</a>
     * @param projectID the ID of the project
     * @param suiteID the ID of the suite
     * @param soft when true will return information about the data which will be deleted but will not proceed with the deletion
     * @param testCaseIDs an array of the IDs of the test cases you would like to delete
     * @return DeletionData – data on the number of affected set when soft is true, in deletion case return null
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "delete_cases/{suite_id}")
    public DeletionData deleteCases(@MapName("project_id") int projectID, @MapName("suite_id") int suiteID, boolean soft, int... testCaseIDs) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        Preconditions.checkArgument(suiteID > 0, "Suite ID should be positive number");
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(testCaseIDs), "Test case ID's should not be empty or null");

        ArrayNode testCaseIDsNode = getObjectMapper().createArrayNode();
        for (int id : testCaseIDs) {
            testCaseIDsNode.add(id);
        }

        ObjectNode deleteCasesNode = getObjectMapper().createObjectNode();
        deleteCasesNode.putArray("case_ids").addAll(testCaseIDsNode);
        deleteCasesNode.put("project_id", projectID);
        deleteCasesNode.put("soft", BooleanUtils.toInteger(soft));

        JsonNode responseNode = getObjectMapper().readTree(getRequester().prepareRequest()
                .bodyByteArray(getObjectMapper().writeValueAsBytes(deleteCasesNode), ContentType.APPLICATION_JSON)
                .execute()
                .returnContent()
                .asString()
        );

        if (responseNode.isEmpty()) {
            return null;
        }

        return getObjectMapper().treeToValue(responseNode, DeletionData.class);
    }

    /**
     * Deletes multiple test cases from a project or test suite.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases#deletecases">Delete Cases</a>
     * @param projectID the ID of the project
     * @param suiteID the ID of the suite
     * @param testCaseIDs an array of the IDs of the test cases you would like to delete
     */
    public void deleteCases(int projectID, int suiteID, int... testCaseIDs) {
        deleteCases(projectID, suiteID, false, testCaseIDs);
    }

    /**
     * Utility method for refreshing inner cache in a case
     * when custom fields caching is enabled. Useful when the structure of the
     * custom fields changes during the use of the client library.
     */
    public void refreshCustomFields() {
        if (cached) {
            cache.refresh(caseFieldsService);
        }

        throw new IllegalAccessError("Data caching is disabled and therefore not available");
    }

    @SneakyThrows
    private Map<String, Field> getCaseFields() {
        if (cached) {
            return cache.get(caseFieldsService);
        }

        cache.refresh(caseFieldsService);
        return cache.get(caseFieldsService);
    }
}
