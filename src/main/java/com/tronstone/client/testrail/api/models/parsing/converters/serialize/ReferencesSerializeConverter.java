package com.tronstone.client.testrail.api.models.parsing.converters.serialize;

import com.fasterxml.jackson.databind.util.StdConverter;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class ReferencesSerializeConverter extends StdConverter<List<String>, String> {
    @Override
    public String convert(List<String> refs) {
        return StringUtils.join(refs, ',');
    }
}
