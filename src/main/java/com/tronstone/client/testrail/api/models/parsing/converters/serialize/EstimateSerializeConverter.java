package com.tronstone.client.testrail.api.models.parsing.converters.serialize;

import com.fasterxml.jackson.databind.util.StdConverter;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatterBuilder;

public class EstimateSerializeConverter extends StdConverter<Period, String> {
    @Override
    public String convert(Period period) {
        return period.toString(
                new PeriodFormatterBuilder().appendWeeks().appendSuffix("w").appendSeparator(StringUtils.SPACE)
                        .appendDays().appendSuffix("d").appendSeparator(StringUtils.SPACE)
                        .appendHours().appendSuffix("h").appendSeparator(StringUtils.SPACE)
                        .appendMinutes().appendSuffix("m").appendSeparator(StringUtils.SPACE)
                        .appendSeconds().appendSuffix("s").appendSeparator(StringUtils.SPACE)
                        .toFormatter()
        );
    }
}
