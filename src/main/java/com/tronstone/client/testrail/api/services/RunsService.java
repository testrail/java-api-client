package com.tronstone.client.testrail.api.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Preconditions;
import com.tronstone.client.inner.annotations.MapName;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.testrail.api.models.beans.deletion.DeletionData;
import com.tronstone.client.testrail.api.models.beans.runs.Run;
import com.tronstone.client.testrail.api.models.beans.runs.Run.RunUpdate;
import com.tronstone.client.testrail.api.parameters.RunsParametersProvider;
import com.tronstone.client.testrail.api.services.pagination.ServicePaginator;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.SneakyThrows;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.entity.ContentType;

/**
 * Use the following API methods to request details about test runs and to create or modify test runs.
 */
public class RunsService extends AbstractTestRailService {
    public RunsService(TestRailClientConfiguration configuration) {
        super(configuration);
    }

    /**
     * Returns an existing test run.
     * Please see {@link TestsService#getTests(int)} for the list of included tests in this run.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/runs#getrun">Get Run</a>
     * @param runID the ID of the test run
     * @return Run – object representation of the test run
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_run/{run_id}")
    public Run getRun(@MapName("run_id") int runID) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), Run.class);
    }

    /**
     * Returns a list of test runs for a project.
     * Only returns those test runs that are not part of a test plan,
     * please see {@link PlansService#getPlan(int)} and {@link PlansService#getPlans(int)} for this.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/runs#getruns">Get Runs</a>
     * @param projectID the ID of the project
     * @return ServicePaginator<Run> – data sets pagination service for runs
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_runs/{project_id}")
    public ServicePaginator<Run> getRuns(@MapName("project_id") int projectID) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Run.class);
    }

    /**
     * Returns a list of filtered test runs for a project.
     * Only returns those test runs that are not part of a test plan,
     * please see {@link PlansService#getPlan(int)} and {@link PlansService#getPlans(int)} for this.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/runs#getruns">Get Runs</a>
     * @param projectID the ID of the project
     * @param parametersProvider request filter parameters object
     * @return ServicePaginator<Run> – data sets pagination service for runs
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_runs/{project_id}")
    public ServicePaginator<Run> getRuns(@MapName("project_id") int projectID, RunsParametersProvider parametersProvider) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        Preconditions.checkNotNull(parametersProvider, "Runs parameters provider should not be null");

        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Run.class);
    }

    /**
     * Creates a new test run.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/runs#addrun">Add Run</a>
     * @param projectID the ID of the project the test run should be added to
     * @param run new test run
     * @return Run – created test run
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_run/{project_id}")
    public Run addRun(@MapName("project_id") int projectID, Run run) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        Preconditions.checkNotNull(run, "Run should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writerWithView(Run.class).writeValueAsBytes(run), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Run.class
        );
    }

    /**
     * Updates an existing test run (partial updates are supported,
     * i.e. you can submit and update specific fields only).
     * To update test runs inside test plans, please see {@link PlansService}.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/runs#updaterun">Update Run</a>
     * @param runID the ID of the test run
     * @param runUpdate run that to be updated
     * @return Run – updated test run
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "update_run/{run_id}")
    public Run updateRun(@MapName("run_id") int runID, RunUpdate runUpdate) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");
        Preconditions.checkNotNull(runUpdate, "Run update should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writerWithView(RunUpdate.class).writeValueAsBytes(runUpdate), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Run.class
        );
    }

    /**
     * Closes an existing test run and archives its tests & results.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/runs#closerun">Close Run</a>
     * @param runID the ID of the test run
     * @return Run – closed test run
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "close_run/{run_id}")
    public Run closeRun(@MapName("run_id") int runID) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString())
                        .execute()
                        .returnContent()
                        .asString(),
                Run.class
        );
    }

    /**
     * Deletes an existing test run.
     * To delete a test run within a test plan, please see {@link PlansService}.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/runs#deleterun">Delete Run</a>
     * @param runID the ID of the test run
     * @param soft when true will return information about the data which will be deleted but will not proceed with the deletion
     * @return DeletionData – data on the number of affected set when soft is true, in deletion case return null
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "delete_run/{run_id}")
    public DeletionData deleteRun(@MapName("run_id") int runID, boolean soft) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");

        ObjectNode deleteRunNode = getObjectMapper().createObjectNode();
        deleteRunNode.put("soft", BooleanUtils.toInteger(soft));

        JsonNode responseNode = getObjectMapper().readTree(getRequester().prepareRequest()
                .bodyByteArray(getObjectMapper().writeValueAsBytes(deleteRunNode), ContentType.APPLICATION_JSON)
                .execute()
                .returnContent()
                .asString()
        );

        if (responseNode.isEmpty()) {
            return null;
        }

        return getObjectMapper().treeToValue(responseNode, DeletionData.class);
    }

    /**
     * Deletes an existing test run.
     * To delete a test run within a test plan, please see {@link PlansService}.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/runs#deleterun">Delete Run</a>
     * @param runID the ID of the test run
     */
    public void deleteRun(int runID) {
        deleteRun(runID, false);
    }
}
