package com.tronstone.client.testrail.api.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Preconditions;
import com.tronstone.client.inner.annotations.MapName;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.testrail.api.models.beans.configurations.ConfigurationGroup;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.entity.ContentType;

import java.util.List;

/**
 * Use the following API methods to request details about test plan configurations
 * and to create or modify configurations used to generate test plans.
 */
public class ConfigurationsService extends AbstractTestRailService {
    public ConfigurationsService(TestRailClientConfiguration configuration) {
        super(configuration);
    }

    /**
     * Returns a list of available configurations, grouped by configuration groups.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/configurations#getconfigs">Get Configs</a>
     * @param projectID the ID of the project
     * @return List<ConfigurationGroup> – list of configuration groups
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_configs/{project_id}")
    public List<ConfigurationGroup> getConfigs(@MapName("project_id") int projectID) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), new TypeReference<List<ConfigurationGroup>>() { });
    }

    /**
     * Creates a new configuration group.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/configurations#addconfiggroup">Add Config Group</a>
     * @param projectID the ID of the project the configuration group should be added to
     * @param name the name of the configuration group
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_config_group/{project_id}")
    public void addConfigGroup(@MapName("project_id") int projectID, String name) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");

        ObjectNode addConfigGroupNode = getObjectMapper().createObjectNode();
        addConfigGroupNode.put("name", name);

        getRequester().prepareRequest()
                .bodyByteArray(getObjectMapper().writeValueAsBytes(addConfigGroupNode), ContentType.APPLICATION_JSON)
                .execute();
    }

    /**
     * Creates a new configuration.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/configurations#addconfig">Add Config</a>
     * @param configGroupID the ID of the configuration group the configuration should be added to
     * @param name the name of the configuration
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_config/{config_group_id}")
    public void addConfig(@MapName("config_group_id") int configGroupID, String name) {
        Preconditions.checkArgument(configGroupID > 0, "Configuration group ID should be positive number");
        Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");

        ObjectNode addConfigNode = getObjectMapper().createObjectNode();
        addConfigNode.put("name", name);

        getRequester().prepareRequest()
                .bodyByteArray(getObjectMapper().writeValueAsBytes(addConfigNode), ContentType.APPLICATION_JSON)
                .execute();
    }

    /**
     * Updates an existing configuration group.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/configurations#updateconfiggroup">Update Config Group</a>
     * @param configGroupID the ID of the configuration group
     * @param name the name of the configuration group
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "update_config_group/{config_group_id}")
    public void updateConfigGroup(@MapName("config_group_id") int configGroupID, String name) {
        Preconditions.checkArgument(configGroupID > 0, "Configuration group ID should be positive number");
        Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");

        ObjectNode updateConfigGroupNode = getObjectMapper().createObjectNode();
        updateConfigGroupNode.put("name", name);

        getRequester().prepareRequest()
                .bodyByteArray(getObjectMapper().writeValueAsBytes(updateConfigGroupNode), ContentType.APPLICATION_JSON)
                .execute();
    }

    /**
     * Updates an existing configuration.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/configurations#updateconfig">Update Config</a>
     * @param configID the ID of the configuration
     * @param name the name of the configuration
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "update_config/{config_id}")
    public void updateConfig(@MapName("config_id") int configID, String name) {
        Preconditions.checkArgument(configID > 0, "Configuration ID should be positive number");
        Preconditions.checkArgument(StringUtils.isNotEmpty(name), "Name should not be empty or null");

        ObjectNode updateConfigNode = getObjectMapper().createObjectNode();
        updateConfigNode.put("name", name);

        getRequester().prepareRequest()
                .bodyByteArray(getObjectMapper().writeValueAsBytes(updateConfigNode), ContentType.APPLICATION_JSON)
                .execute();
    }

    /**
     * Deletes an existing configuration group and its configurations.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/configurations#deleteconfiggroup">Delete Config Group</a>
     * @param configGroupID the ID of the configuration group
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "delete_config_group/{config_group_id}")
    public void deleteConfigGroup(@MapName("config_group_id") int configGroupID) {
        Preconditions.checkArgument(configGroupID > 0, "Configuration group ID should be positive number");
        getRequester().prepareRequest().addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType()).execute();
    }

    /**
     * Deletes an existing configuration.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/configurations#deleteconfig">Delete Config</a>
     * @param configID the ID of the configuration
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "delete_config/{config_id}")
    public void deleteConfig(@MapName("config_id") int configID) {
        Preconditions.checkArgument(configID > 0, "Configuration ID should be positive number");
        getRequester().prepareRequest().addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType()).execute();
    }
}
