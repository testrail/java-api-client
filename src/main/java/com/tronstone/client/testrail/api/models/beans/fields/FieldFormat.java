package com.tronstone.client.testrail.api.models.beans.fields;

public enum FieldFormat {
    PLAIN,
    MARKDOWN;

    @Override
    public String toString() {
        return name().toLowerCase();
    }

    public static FieldFormat valueOfFieldFormat(String fieldFormat) {
        return FieldFormat.valueOf(fieldFormat.toUpperCase());
    }
}
