package com.tronstone.client.testrail.api.models.beans.statuses;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.util.StdConverter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.awt.Color;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Status {
    private int id;
    private String name;
    private String label;

    @JsonProperty("is_system")
    private boolean system;

    @JsonProperty("is_untested")
    private boolean untested;

    @JsonProperty("is_final")
    private @Getter(AccessLevel.NONE) boolean statusFinal;

    @JsonDeserialize(converter = ColorDeserializeConverter.class)
    private Color colorDark;

    @JsonDeserialize(converter = ColorDeserializeConverter.class)
    private Color colorMedium;

    @JsonDeserialize(converter = ColorDeserializeConverter.class)
    private Color colorBright;

    public int getID() {
        return id;
    }

    public boolean isFinal() {
        return statusFinal;
    }

    private static class ColorDeserializeConverter extends StdConverter<Integer, Color> {
        @Override
        public Color convert(Integer integer) {
            return new Color(integer);
        }
    }
}
