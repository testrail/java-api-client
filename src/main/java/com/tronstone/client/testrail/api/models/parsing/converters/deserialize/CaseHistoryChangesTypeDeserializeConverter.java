package com.tronstone.client.testrail.api.models.parsing.converters.deserialize;

import com.fasterxml.jackson.databind.util.StdConverter;
import com.tronstone.client.testrail.api.models.beans.cases.history.CaseHistoryChangesType;

public class CaseHistoryChangesTypeDeserializeConverter extends StdConverter<Integer, CaseHistoryChangesType> {
    @Override
    public CaseHistoryChangesType convert(Integer changesTypeID) {
        return CaseHistoryChangesType.valueOfChangesType(changesTypeID);
    }
}
