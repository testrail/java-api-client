package com.tronstone.client.testrail.api.models.beans.cases.history;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.tronstone.client.testrail.api.models.beans.fields.FieldOptions;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.CaseHistoryChangesTypeDeserializeConverter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CaseHistoryChanges {
    private String label;
    private String field;
    private String oldText;
    private String newText;
    private Object oldValue;
    private Object newValue;
    private FieldOptions options;

    @JsonProperty("type_id")
    @JsonDeserialize(converter = CaseHistoryChangesTypeDeserializeConverter.class)
    private CaseHistoryChangesType type;
}
