package com.tronstone.client.testrail.api.models.beans.steps;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;
import com.tronstone.client.testrail.api.models.parsing.converters.deserialize.ReferencesDeserializeConverter;
import com.tronstone.client.testrail.api.models.parsing.converters.serialize.ReferencesSerializeConverter;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ToString
@Setter @Getter
@EqualsAndHashCode
@JsonInclude(Include.NON_DEFAULT)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Step {
	private String content;
	private String additionalInfo;
	private String expected;

	@JsonSerialize(converter = ReferencesSerializeConverter.class)
	@JsonDeserialize(converter = ReferencesDeserializeConverter.class)
	private final @Setter(AccessLevel.NONE) List<String> refs = new ArrayList<>();

	@JsonProperty(access = Access.WRITE_ONLY)
	private @Setter(AccessLevel.NONE) int sharedStepID;

	/**
	 * Returns step object builder.
	 *
	 * @return StepBuilder – step builder object
	 */
	public static StepBuilder construct() {
		return new StepBuilder();
	}

	/**
	 * Returns list of steps.
	 *
	 * @param steps steps array
	 * @return List<Step> – list of steps
	 */
	public static List<Step> createSteps(Step... steps) {
		Preconditions.checkArgument(ArrayUtils.isNotEmpty(steps), "Steps should not be empty or null");
		return Arrays.asList(steps);
	}

	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static class StepBuilder {
		private String content;
		private String additionalInfo;
		private String expected;
		private final List<String> refs = new ArrayList<>();

		/**
		 * The text contents of the “Step” field.
		 *
		 * @param content text value
		 * @return StepBuilder – step builder object
		 */
		public StepBuilder content(String content) {
			Preconditions.checkArgument(StringUtils.isNotEmpty(content), "Content should not be empty or null");
			this.content = content;
			return this;
		}

		/**
		 * The text contents of the “Additional Info” field.
		 *
		 * @param additionalInfo text value
		 * @return StepBuilder – step builder object
		 */
		public StepBuilder additionalInfo(String additionalInfo) {
			Preconditions.checkArgument(StringUtils.isNotEmpty(additionalInfo), "Additional info should not be empty or null");
			this.additionalInfo = additionalInfo;
			return this;
		}

		/**
		 * The text contents of the “Expected Result” field.
		 *
		 * @param expected text value
		 * @return StepBuilder – step builder object
		 */
		public StepBuilder expected(String expected) {
			Preconditions.checkArgument(StringUtils.isNotEmpty(expected), "Expected should not be empty or null");
			this.expected = expected;
			return this;
		}

		/**
		 * Add the list of references/requirements.
		 *
		 * @param refs list of references/requirements
		 * @return StepBuilder – step builder object
		 */
		public StepBuilder refs(List<String> refs) {
			Preconditions.checkArgument(CollectionUtils.isNotEmpty(refs), "References should not be empty or null");
			this.refs.addAll(refs);
			return this;
		}

		/**
		 * Parse and add list of comma-separated references/requirements.
		 *
		 * @param refs a comma-separated list of references/requirements
		 * @return StepBuilder – step builder object
		 */
		public StepBuilder refs(String refs) {
			Preconditions.checkArgument(StringUtils.isNotEmpty(refs), "References should not be empty or null");
			this.refs.addAll(new ReferencesDeserializeConverter().convert(refs));
			return this;
		}

		/**
		 * Add the array of references/requirements.
		 *
		 * @param refs array of references/requirements
		 * @return StepBuilder – step builder object
		 */
		public StepBuilder refs(String... refs) {
			Preconditions.checkArgument(ArrayUtils.isNotEmpty(refs), "References should not be empty or null");

			this.refs.addAll(Arrays.stream(refs)
					.peek(ref -> Preconditions.checkArgument(StringUtils.isNotEmpty(ref), "Reference should not be empty or null"))
					.collect(Collectors.toList()));

			return this;
		}

		/**
		 * Build ready step object.
		 *
		 * @return Step – constructed step object
		 */
		public Step build() {
			Step step = new Step();

			step.content = content;
			step.additionalInfo = additionalInfo;
			step.expected = expected;
			step.refs.addAll(refs);

			return step;
		}
	}
}