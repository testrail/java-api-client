package com.tronstone.client.testrail.api.parameters;

import com.google.common.base.Preconditions;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

@NoArgsConstructor(staticName = "prepare")
public class ResultsParametersProvider extends PaginationParametersProvider<ResultsParametersProvider> {
    private final StringBuilder parametersBuilder = getParametersBuilder();

    /**
     * A single Defect ID (e.g. TR-1, 4291, etc).
     *
     * @param defect single defect
     * @return ResultsParametersProvider – provider object
     */
    public ResultsParametersProvider defectsFilter(String defect) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(defect), "Defect should not be empty or null");
        parametersBuilder.append("&defects_filter=").append(defect);
        return this;
    }

    /**
     * A comma-separated list of status IDs to filter by.
     *
     * @param statusIDs array of status ID's
     * @return ResultsParametersProvider – provider object
     */
    public ResultsParametersProvider statusID(int... statusIDs) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(statusIDs), "Status ID's should not be empty or null");
        parametersBuilder.append("&status_id=").append(StringUtils.join(statusIDs, ','));
        return this;
    }
}
