package com.tronstone.client.testrail.api.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Preconditions;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.testrail.api.models.beans.fields.Field;
import com.tronstone.client.testrail.api.models.beans.fields.FieldType;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.SneakyThrows;
import org.apache.http.entity.ContentType;

import java.util.List;

/**
 * Use the following API methods to request details about custom fields for test cases.
 */
public class CaseFieldsService extends AbstractTestRailService {
    public CaseFieldsService(TestRailClientConfiguration configuration) {
        super(configuration);
    }

    /**
     * Returns a list of available test case custom fields.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/case-fields#getcasefields">Get Case Fields</a>
     * @return List<CaseField> – list of case fields
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_case_fields")
    public List<Field> getCaseFields() {
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), new TypeReference<List<Field>>() { });
    }

    /**
     * Creates a new test case custom field.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/case-fields#addcasefield">Add Case Field</a>
     * @param caseField new case field
     * @return CaseField – created case field
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_case_field")
    public Field addCaseField(Field caseField) {
        Preconditions.checkNotNull(caseField, "Case field should not be null");
        Preconditions.checkArgument(
                !caseField.getFieldType().equals(FieldType.STEP_RESULTS),
                "For Steps Results case field type custom field is not supporting"
        );

        ObjectNode caseFieldNode = (ObjectNode) getObjectMapper().readTree(getRequester().prepareRequest()
                .bodyByteArray(getObjectMapper().writeValueAsBytes(caseField), ContentType.APPLICATION_JSON)
                .execute()
                .returnContent()
                .asString()
        );

        caseFieldNode.replace("configs", getObjectMapper().readTree(caseFieldNode.get("configs").asText()));
        return getObjectMapper().treeToValue(caseFieldNode, Field.class);
    }
}
