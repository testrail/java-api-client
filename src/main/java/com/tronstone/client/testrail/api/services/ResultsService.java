package com.tronstone.client.testrail.api.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.tronstone.client.inner.annotations.MapName;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.inner.service.ServiceFactory;
import com.tronstone.client.testrail.api.models.beans.fields.Field;
import com.tronstone.client.testrail.api.models.beans.results.Result;
import com.tronstone.client.testrail.api.models.parsing.modules.CustomFieldModule;
import com.tronstone.client.testrail.api.parameters.ResultsParametersProvider;
import com.tronstone.client.testrail.api.parameters.RunResultsParametersProvider;
import com.tronstone.client.testrail.api.services.pagination.ServicePaginator;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.http.entity.ContentType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Use the following API methods to request details about test results and to add new test results.
 */
public class ResultsService extends AbstractTestRailService {
    private final boolean cached;
    private final ResultFieldsService resultFieldsService;
    private final LoadingCache<ResultFieldsService, Map<String, Field>> cache;

    public ResultsService(TestRailClientConfiguration configuration, boolean cached) {
        super(configuration);

        this.cached = cached;
        resultFieldsService = ServiceFactory.createService(ResultFieldsService.class, configuration);
        cache = CacheBuilder.newBuilder().build(new CacheLoader<ResultFieldsService, Map<String, Field>>() {
            @NonNull
            @Override
            public Map<String, Field> load(@NonNull ResultFieldsService resultFieldsService) {
                return resultFieldsService.getResultFields().stream().reduce(new HashMap<>(), (resultFields, resultField) -> {
                    resultFields.put(resultField.getSystemName(), resultField);
                    return resultFields;
                }, (resultFields, parallelResultFieldTypes) -> resultFields);
            }
        });
    }

    /**
     * Returns a list of test results for a test.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/results#getresults">Get Results</a>
     * @param testID the ID of the test
     * @return ServicePaginator<Result> – data sets pagination service for results
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_results/{test_id}")
    public ServicePaginator<Result> getResults(@MapName("test_id") int testID) {
        Preconditions.checkArgument(testID > 0, "Test ID should be positive number");

        return new ServicePaginator<Result>(getConfiguration(), getRequester().prepareRequest(), Result.class) {{
            getObjectMapper().registerModule(new CustomFieldModule(getResultFields()));
        }};
    }

    /**
     * Returns a list of filtered test results for a test.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/results#getresults">Get Results</a>
     * @param testID the ID of the test
     * @param parametersProvider request filter parameters object
     * @return ServicePaginator<Result> – data sets pagination service for results
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_results/{test_id}")
    public ServicePaginator<Result> getResults(@MapName("test_id") int testID, ResultsParametersProvider parametersProvider) {
        Preconditions.checkArgument(testID > 0, "Test ID should be positive number");
        Preconditions.checkNotNull(parametersProvider, "Results parameters provider should not be null");

        return new ServicePaginator<Result>(getConfiguration(), getRequester().prepareRequest(), Result.class) {{
            getObjectMapper().registerModule(new CustomFieldModule(getResultFields()));
        }};
    }

    /**
     * Returns a list of test results for a test run and case combination.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/results#getresultsforcase">Get Results for Case</a>
     * @param runID the ID of the test run
     * @param caseID the ID of the test case
     * @return ServicePaginator<Result> – data sets pagination service for results
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_results_for_case/{run_id}/{case_id}")
    public ServicePaginator<Result> getResultsForCase(@MapName("run_id") int runID, @MapName("case_id") int caseID) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");
        Preconditions.checkArgument(caseID > 0, "Case ID should be positive number");

        return new ServicePaginator<Result>(getConfiguration(), getRequester().prepareRequest(), Result.class) {{
            getObjectMapper().registerModule(new CustomFieldModule(getResultFields()));
        }};
    }

    /**
     * Returns a list of filtered test results for a test run and case combination.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/results#getresultsforcase">Get Results for Case</a>
     * @param runID the ID of the test run
     * @param caseID the ID of the test case
     * @param parametersProvider request filter parameters object
     * @return ServicePaginator<Result> – data sets pagination service for results
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_results_for_case/{run_id}/{case_id}")
    public ServicePaginator<Result> getResultsForCase(@MapName("run_id") int runID, @MapName("case_id") int caseID, ResultsParametersProvider parametersProvider) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");
        Preconditions.checkArgument(caseID > 0, "Case ID should be positive number");
        Preconditions.checkNotNull(parametersProvider, "Results parameters provider should not be null");

        return new ServicePaginator<Result>(getConfiguration(), getRequester().prepareRequest(), Result.class) {{
            getObjectMapper().registerModule(new CustomFieldModule(getResultFields()));
        }};
    }

    /**
     * Returns a list of test results for a test run.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/results#getresultsforrun">Get Results for Run</a>
     * @param runID the ID of the test run
     * @return ServicePaginator<Result> – data sets pagination service for results
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_results_for_run/{run_id}")
    public ServicePaginator<Result> getResultsForRun(@MapName("run_id") int runID) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");

        return new ServicePaginator<Result>(getConfiguration(), getRequester().prepareRequest(), Result.class) {{
            getObjectMapper().registerModule(new CustomFieldModule(getResultFields()));
        }};
    }

    /**
     * Returns a list of filtered test results for a test run.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/results#getresultsforrun">Get Results for Run</a>
     * @param runID the ID of the test run
     * @param parametersProvider request filter parameters object
     * @return ServicePaginator<Result> – data sets pagination service for results
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_results_for_run/{run_id}")
    public ServicePaginator<Result> getResultsForRun(@MapName("run_id") int runID, RunResultsParametersProvider parametersProvider) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");
        Preconditions.checkNotNull(parametersProvider, "Run results parameters provider should not be null");

        return new ServicePaginator<Result>(getConfiguration(), getRequester().prepareRequest(), Result.class) {{
            getObjectMapper().registerModule(new CustomFieldModule(getResultFields()));
        }};
    }

    /**
     * Adds a new test result, comment or assigns a test.
     * It’s recommended to use {@link ResultsService#addResults(int, Result...)} instead
     * if you plan to add results for multiple tests.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/results#addresult">Add Result</a>
     * @param testID the ID of the test the result should be added to
     * @param result new result
     * @return Result – created result
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_result/{test_id}")
    public Result addResult(@MapName("test_id") int testID, Result result) {
        Preconditions.checkArgument(testID > 0, "Test ID should be positive number");
        Preconditions.checkNotNull(result, "Result should not be null");

        return getObjectMapper().registerModule(new CustomFieldModule(getResultFields())).readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writeValueAsBytes(result), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Result.class
        );
    }

    /**
     * Adds a new test result, comment or assigns a test (for a test run and case combination).
     * It’s recommended to use {@link ResultsService#addResultsForCases(int, Result...)} instead
     * if you plan to add results for multiple test cases.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/results#addresultforcase">Add Result for Case</a>
     * @param runID the ID of the test run
     * @param caseID the ID of the test case
     * @param result new result
     * @return Result – created result
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_result_for_case/{run_id}/{case_id}")
    public Result addResultForCase(@MapName("run_id") int runID, @MapName("case_id") int caseID, Result result) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");
        Preconditions.checkArgument(caseID > 0, "Case ID should be positive number");
        Preconditions.checkNotNull(result, "Result should not be null");

        return getObjectMapper().registerModule(new CustomFieldModule(getResultFields())).readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writeValueAsBytes(result), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Result.class
        );
    }

    /**
     * Adds one or more new test results, comments, or assigns one or more tests.
     * Ideal for test automation to bulk-add multiple test results in one step.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/results#addresults">Add Results</a>
     * @param runID the ID of the test run the results should be added to
     * @param results array of results
     * @return List<Result> – list of added results
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_results/{run_id}")
    public List<Result> addResults(@MapName("run_id") int runID, Result... results) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(results), "Results should not be empty or null");

        ArrayNode resultsListNode = getObjectMapper().createArrayNode();
        for (Result result : results) {
            resultsListNode.add(getObjectMapper().valueToTree(result));
        }

        ObjectNode resultsNode = getObjectMapper().createObjectNode();
        resultsNode.putArray("results").addAll(resultsListNode);

        return getObjectMapper().registerModule(new CustomFieldModule(getResultFields())).readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writeValueAsBytes(resultsNode), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                new TypeReference<List<Result>>() { }
        );
    }

    /**
     * Adds one or more new test results, comments, or assigns one or more tests.
     * Ideal for test automation to bulk-add multiple test results in one step.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/results#addresultsforcases">Add Results for Cases</a>
     * @param runID the ID of the test run the results should be added to
     * @param results array of results
     * @return List<Result> – list of added results
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_results_for_cases/{run_id}")
    public List<Result> addResultsForCases(@MapName("run_id") int runID, Result... results) {
        Preconditions.checkArgument(runID > 0, "Run ID should be positive number");
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(results), "Results should not be empty or null");

        ArrayNode resultsListNode = getObjectMapper().createArrayNode();
        for (Result result : results) {
            resultsListNode.add(getObjectMapper().valueToTree(result));
        }

        ObjectNode resultsNode = getObjectMapper().createObjectNode();
        resultsNode.putArray("results").addAll(resultsListNode);

        return getObjectMapper().registerModule(new CustomFieldModule(getResultFields())).readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writeValueAsBytes(resultsNode), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                new TypeReference<List<Result>>() { }
        );
    }

    /**
     * Utility method for refreshing inner cache in a case
     * when custom fields caching is enabled. Useful when the structure of the
     * custom fields changes during the use of the client library.
     */
    public void refreshCustomFields() {
        if (cached) {
            cache.refresh(resultFieldsService);
        }

        throw new IllegalAccessError("Data caching is disabled and therefore not available");
    }

    @SneakyThrows
    private Map<String, Field> getResultFields() {
        if (cached) {
            return cache.get(resultFieldsService);
        }

        cache.refresh(resultFieldsService);
        return cache.get(resultFieldsService);
    }
}
