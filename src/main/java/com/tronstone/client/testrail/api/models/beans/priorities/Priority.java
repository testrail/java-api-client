package com.tronstone.client.testrail.api.models.beans.priorities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Priority {
	private int id;
	private int priority;
	private String name;
	private String shortName;

	@JsonProperty("is_default")
	private @Getter(AccessLevel.NONE) boolean priorityDefault;

	public int getID() {
		return id;
	}

	public boolean isDefault() {
		return priorityDefault;
	}
}