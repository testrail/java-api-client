package com.tronstone.client.testrail.api.models.beans.pagination;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PaginationData<T> {
    private int offset;
    private int limit;
    private int size;
    private String nextURL;
    private String previousURL;
    private List<T> data;

    @JsonSetter("_links")
    private void setLinks(Map<String, String> links) {
        nextURL = links.get("next");
        previousURL = links.get("prev");
    }

    @JsonAnySetter
    private void setData(String key, List<T> data) {
        this.data = data;
    }
}
