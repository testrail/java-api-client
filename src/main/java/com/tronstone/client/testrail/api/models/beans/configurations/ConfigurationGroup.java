package com.tronstone.client.testrail.api.models.beans.configurations;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConfigurationGroup {
    private int id;
    private String name;
    private int projectID;
    private List<Configuration> configs;

    public int getID() {
        return id;
    }
}
