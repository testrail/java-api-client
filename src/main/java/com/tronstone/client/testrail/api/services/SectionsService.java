package com.tronstone.client.testrail.api.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Preconditions;
import com.tronstone.client.inner.annotations.MapName;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.testrail.api.models.beans.deletion.DeletionData;
import com.tronstone.client.testrail.api.models.beans.sections.Section;
import com.tronstone.client.testrail.api.models.beans.sections.Section.SectionUpdate;
import com.tronstone.client.testrail.api.parameters.SectionsParametersProvider;
import com.tronstone.client.testrail.api.services.pagination.ServicePaginator;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.SneakyThrows;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.http.entity.ContentType;

/**
 * Use the following API methods to request details about sections and to create or modify sections.
 * Sections are used to group and organize test cases in test suites.
 */
public class SectionsService extends AbstractTestRailService {
    public SectionsService(TestRailClientConfiguration configuration) {
        super(configuration);
    }

    /**
     * Returns an existing section.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/sections#getsection">Get Section</a>
     * @param sectionID the ID of the section
     * @return Section – object representation of the section
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_section/{section_id}")
    public Section getSection(@MapName("section_id") int sectionID) {
        Preconditions.checkArgument(sectionID > 0, "Section ID should be positive number");
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), Section.class);
    }

    /**
     * Returns a list of sections for a project and test suite.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/sections#getsections">Get Sections</a>
     * @param projectID the ID of the project
     * @return ServicePaginator<Section> – data sets pagination service for sections
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_sections/{project_id}")
    public ServicePaginator<Section> getSections(@MapName("project_id") int projectID) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Section.class);
    }

    /**
     * Returns a list of sections for a project and
     * test suite (if the project has multiple suites enabled).
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/sections#getsections">Get Sections</a>
     * @param projectID the ID of the project
     * @param suiteID the ID of the test suite
     * @return ServicePaginator<Section> – data sets pagination service for sections
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_sections/{project_id}&suite_id={suite_id}")
    public ServicePaginator<Section> getSections(@MapName("project_id") int projectID, @MapName("suite_id") int suiteID) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        Preconditions.checkArgument(suiteID > 0, "Suite ID should be positive number");

        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Section.class);
    }

    /**
     * Returns a list of filtered sections for a project
     * and test suite (if the project has multiple suites enabled).
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/sections#getsections">Get Sections</a>
     * @param projectID the ID of the project
     * @param parametersProvider request filter parameters object
     * @return ServicePaginator<Section> – data sets pagination service for sections
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_sections/{project_id}")
    public ServicePaginator<Section> getSections(@MapName("project_id") int projectID, SectionsParametersProvider parametersProvider) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        Preconditions.checkNotNull(parametersProvider, "Sections parameters provider should not be null");

        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Section.class);
    }

    /**
     * Creates a new section.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/sections#addsection">Add Section</a>
     * @param projectID the ID of the project
     * @param section new section
     * @return Section – created section
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_section/{project_id}")
    public Section addSection(@MapName("project_id") int projectID, Section section) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        Preconditions.checkNotNull(section, "Section should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writeValueAsBytes(section), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Section.class
        );
    }

    /**
     * Moves a section to another suite or section.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/sections#movesection">Move Section</a>
     * @param sectionID the ID of the section
     * @param parentID the ID of the parent section (it can be 0 if it should be
     *                 moved to the root). Must be in the same project and suite.
     *                 May not be a direct child of the section being moved
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "move_section/{section_id}")
    public void moveSectionParent(@MapName("section_id") int sectionID, @MapName("parent_id") int parentID) {
        Preconditions.checkArgument(sectionID > 0, "Section ID should be positive number");
        Preconditions.checkArgument(parentID > -1, "Parent ID should be zero or positive number");

        ObjectNode moveSectionNode = getObjectMapper().createObjectNode();
        moveSectionNode.put("parent_id", parentID == 0 ? null : parentID);

        getRequester().prepareRequest()
                .bodyByteArray(getObjectMapper().writeValueAsBytes(moveSectionNode), ContentType.APPLICATION_JSON)
                .execute();
    }

    /**
     * Moves a section to another suite or section.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/sections#movesection">Move Section</a>
     * @param sectionID the ID of the section
     * @param afterID the section ID after which the section should be put (can be 0)
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "move_section/{section_id}")
    public void moveSectionAfter(@MapName("section_id") int sectionID, @MapName("after_id") int afterID) {
        Preconditions.checkArgument(sectionID > 0, "Section ID should be positive number");
        Preconditions.checkArgument(afterID > -1, "After ID should be zero or positive number");

        ObjectNode moveSectionNode = getObjectMapper().createObjectNode();
        moveSectionNode.put("after_id", afterID == 0 ? null : afterID);

        getRequester().prepareRequest()
                .bodyByteArray(getObjectMapper().writeValueAsBytes(moveSectionNode), ContentType.APPLICATION_JSON)
                .execute();
    }

    /**
     * Moves a section to another suite or section.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/sections#movesection">Move Section</a>
     * @param sectionID the ID of the section
     * @param afterID the section ID after which the section should be put (can be 0)
     * @param parentID the ID of the parent section (it can be 0 if it should be
     *                 moved to the root). Must be in the same project and suite.
     *                 May not be a direct child of the section being moved
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "move_section/{section_id}")
    public void moveSection(@MapName("section_id") int sectionID, @MapName("parent_id") int parentID, @MapName("after_id") int afterID) {
        Preconditions.checkArgument(sectionID > 0, "Section ID should be positive number");
        Preconditions.checkArgument(parentID > -1, "Parent ID should be zero or positive number");
        Preconditions.checkArgument(afterID > -1, "After ID should be zero or positive number");

        ObjectNode moveSectionNode = getObjectMapper().createObjectNode();
        moveSectionNode.put("parent_id", parentID == 0 ? null : parentID);
        moveSectionNode.put("after_id", afterID == 0 ? null : afterID);

        getRequester().prepareRequest()
                .bodyByteArray(getObjectMapper().writeValueAsBytes(moveSectionNode), ContentType.APPLICATION_JSON)
                .execute();
    }

    /**
     * Updates an existing section (partial updates are supported,
     * i.e. you can submit and update specific fields only).
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/sections#updatesection">Update Section</a>
     * @param sectionID the ID of the section
     * @param sectionUpdate section that to be updated
     * @return Section – updated section
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "update_section/{section_id}")
    public Section updateSection(@MapName("section_id") int sectionID, SectionUpdate sectionUpdate) {
        Preconditions.checkArgument(sectionID > 0, "Section ID should be positive number");
        Preconditions.checkNotNull(sectionUpdate, "Section update should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writeValueAsBytes(sectionUpdate), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Section.class
        );
    }

    /**
     * Deletes an existing section.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/sections#deletesection">Delete Section</a>
     * @param sectionID the ID of the section
     * @param soft when true will return information about the data which will be deleted but will not proceed with the deletion
     * @return DeletionData – data on the number of affected set when soft is true, in deletion case return null
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "delete_section/{section_id}")
    public DeletionData deleteSection(@MapName("section_id") int sectionID, boolean soft) {
        Preconditions.checkArgument(sectionID > 0, "Section ID should be positive number");

        ObjectNode deleteSectionNode = getObjectMapper().createObjectNode();
        deleteSectionNode.put("soft", BooleanUtils.toInteger(soft));

        JsonNode responseNode = getObjectMapper().readTree(getRequester().prepareRequest()
                .bodyByteArray(getObjectMapper().writeValueAsBytes(deleteSectionNode), ContentType.APPLICATION_JSON)
                .execute()
                .returnContent()
                .asString()
        );

        if (responseNode.isEmpty()) {
            return null;
        }

        return getObjectMapper().treeToValue(responseNode, DeletionData.class);
    }

    /**
     * Deletes an existing section.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/sections#deletesection">Delete Section</a>
     * @param sectionID the ID of the section
     */
    public void deleteSection(int sectionID) {
        deleteSection(sectionID, false);
    }
}
