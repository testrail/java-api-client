package com.tronstone.client.testrail.api.models.beans.cases.types;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CaseType {
    private int id;
    private String name;

    @JsonProperty("is_default")
    private @Getter(AccessLevel.NONE) boolean typeDefault;

    public int getID() {
        return id;
    }

    public boolean isDefault() {
        return typeDefault;
    }
}
