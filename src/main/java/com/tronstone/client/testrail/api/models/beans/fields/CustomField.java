package com.tronstone.client.testrail.api.models.beans.fields;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class CustomField {
    public static final String CUSTOM_FIELD_SYSTEM_PREFIX = "custom_";

    private String prefixedSystemName;
    private String label;
    private FieldType<?> fieldType;
    private Object fieldValue;

    public CustomField(String prefixedSystemName, FieldType<?> fieldType, Object fieldValue) {
        this.prefixedSystemName = prefixedSystemName;
        this.fieldType = fieldType;
        this.fieldValue = fieldValue;
    }
}
