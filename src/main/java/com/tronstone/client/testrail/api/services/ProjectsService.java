package com.tronstone.client.testrail.api.services;

import com.google.common.base.Preconditions;
import com.tronstone.client.inner.annotations.MapName;
import com.tronstone.client.inner.annotations.RequesterMapping;
import com.tronstone.client.inner.service.HttpMethod;
import com.tronstone.client.testrail.api.models.beans.projects.Project;
import com.tronstone.client.testrail.api.models.beans.projects.Project.ProjectUpdate;
import com.tronstone.client.testrail.api.parameters.ProjectsParametersProvider;
import com.tronstone.client.testrail.api.services.pagination.ServicePaginator;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import lombok.SneakyThrows;
import org.apache.http.HttpHeaders;
import org.apache.http.entity.ContentType;

/**
 * Use the following API methods to request details about projects and to create or modify projects.
 */
public class ProjectsService extends AbstractTestRailService {
    public ProjectsService(TestRailClientConfiguration configuration) {
        super(configuration);
    }

    /**
     * Returns an existing project.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/projects#getproject">Get Project</a>
     * @param projectID the ID of the project
     * @return Project – object representation of the project
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.GET, path = "get_project/{project_id}")
    public Project getProject(@MapName("project_id") int projectID) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        return getObjectMapper().readValue(getRequester().prepareAndExecuteRequest(), Project.class);
    }

    /**
     * Returns the list of available projects.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/projects#getprojects">Get Projects</a>
     * @return ServicePaginator<Project> – data sets pagination service for projects
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_projects")
    public ServicePaginator<Project> getProjects() {
        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Project.class);
    }

    /**
     * Returns the list of filtered available projects.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/projects#getprojects">Get Projects</a>
     * @param parametersProvider request filter parameters object
     * @return ServicePaginator<Project> – data sets pagination service for projects
     */
    @RequesterMapping(method = HttpMethod.GET, path = "get_projects")
    public ServicePaginator<Project> getProjects(ProjectsParametersProvider parametersProvider) {
        Preconditions.checkNotNull(parametersProvider, "Projects parameters provider should not be null");
        return new ServicePaginator<>(getConfiguration(), getRequester().prepareRequest(), Project.class);
    }

    /**
     * Creates a new project (admin status required).
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/projects#addproject">Add Project</a>
     * @param project new project
     * @return Project – created project
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "add_project")
    public Project addProject(Project project) {
        Preconditions.checkNotNull(project, "Project should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writeValueAsBytes(project), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Project.class
        );
    }

    /**
     * Updates an existing project (admin status required; partial updates are supported,
     * i.e. you can submit and update specific fields only).
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/projects#updateproject">Update Project</a>
     * @param projectID the ID of the project
     * @param projectUpdate project that to be updated
     * @return Project – updated project
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "update_project/{project_id}")
    public Project updateProject(@MapName("project_id") int projectID, ProjectUpdate projectUpdate) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        Preconditions.checkNotNull(projectUpdate, "Project update should not be null");

        return getObjectMapper().readValue(
                getRequester().prepareRequest()
                        .bodyByteArray(getObjectMapper().writeValueAsBytes(projectUpdate), ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent()
                        .asString(),
                Project.class
        );
    }

    /**
     * Deletes an existing project (admin status required).
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/projects#deleteproject">Delete Project</a>
     * @param projectID the ID of the project
     */
    @SneakyThrows
    @RequesterMapping(method = HttpMethod.POST, path = "delete_project/{project_id}")
    public void deleteProject(@MapName("project_id") int projectID) {
        Preconditions.checkArgument(projectID > 0, "Project ID should be positive number");
        getRequester().prepareRequest().addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString()).execute();
    }
}
