package com.tronstone.client.testrail.api.parameters;

import lombok.NoArgsConstructor;

@NoArgsConstructor(staticName = "prepare")
public class AttachmentsParametersProvider extends PaginationParametersProvider<AttachmentsParametersProvider>{ }
