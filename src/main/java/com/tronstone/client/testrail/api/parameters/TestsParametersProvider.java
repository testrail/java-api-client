package com.tronstone.client.testrail.api.parameters;

import com.google.common.base.Preconditions;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

@NoArgsConstructor(staticName = "prepare")
public class TestsParametersProvider extends PaginationParametersProvider<TestsParametersProvider> {
    private final StringBuilder parametersBuilder = getParametersBuilder();

    /**
     * A comma-separated array of status ID's to filter by.
     *
     * @param statusIDs array of status ID's
     * @return TestsParametersProvider – provider object
     */
    public TestsParametersProvider statusID(int... statusIDs) {
        Preconditions.checkArgument(ArrayUtils.isNotEmpty(statusIDs), "Status ID's should not be empty or null");
        parametersBuilder.append("&status_id=").append(StringUtils.join(statusIDs, ','));
        return this;
    }
}
