package com.tronstone.client.testrail;

import com.tronstone.client.inner.authentication.APIKeyAuthentication;
import com.tronstone.client.inner.authentication.Authentication;
import com.tronstone.client.inner.authentication.BasicAuthentication;
import com.tronstone.client.inner.service.BasicService;
import com.tronstone.client.inner.service.ServiceFactory;
import com.tronstone.client.testrail.api.services.AttachmentsService;
import com.tronstone.client.testrail.api.services.CaseFieldsService;
import com.tronstone.client.testrail.api.services.CaseTypesService;
import com.tronstone.client.testrail.api.services.CasesService;
import com.tronstone.client.testrail.api.services.ConfigurationsService;
import com.tronstone.client.testrail.api.services.GroupsService;
import com.tronstone.client.testrail.api.services.PlansService;
import com.tronstone.client.testrail.api.services.PrioritiesService;
import com.tronstone.client.testrail.api.services.ProjectsService;
import com.tronstone.client.testrail.api.services.ResultFieldsService;
import com.tronstone.client.testrail.api.services.ResultsService;
import com.tronstone.client.testrail.api.services.RolesService;
import com.tronstone.client.testrail.api.services.RunsService;
import com.tronstone.client.testrail.api.services.SectionsService;
import com.tronstone.client.testrail.api.services.StatusesService;
import com.tronstone.client.testrail.api.services.SuitesService;
import com.tronstone.client.testrail.api.services.TestsService;
import com.tronstone.client.testrail.api.services.UsersService;
import com.tronstone.client.testrail.configuration.TestRailClientConfiguration;
import com.tronstone.client.testrail.configuration.TestRailConfigurationProperties;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIUtils;

import java.net.URI;

/**
 * TestRail API Java Client for test case management system.
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class TestRailClient {
    private TestRailClientConfiguration configuration;
    private @Getter TestRailConfigurationProperties configurationProperties;

    /**
     * Manual configuration.
     *
     * @param instanceURL testrail instance url
     * @param username testrail instance username (which is usually your email address)
     * @return TestRailClientManualConfigurator – manual configurator instance
     */
    public static TestRailClientManualConfigurator configure(String instanceURL, String username) {
        return new TestRailClientManualConfigurator(instanceURL, username);
    }

    /**
     * Property configuration.
     *
     * @param configurationPropertiesClass properties interface class
     * @return TestRailClientFactory – client factory instance
     */
    public static TestRailClientFactory configure(Class<? extends TestRailConfigurationProperties> configurationPropertiesClass) {
        return new TestRailClientPropertyConfigurator().configure(configurationPropertiesClass);
    }

    /**
     * Property configuration.
     *
     * @param configurationProperties properties object
     * @return TestRailClientFactory – client factory instance
     */
    public static TestRailClientFactory configure(TestRailConfigurationProperties configurationProperties) {
        return new TestRailClientPropertyConfigurator().configure(configurationProperties);
    }

    /**
     * Creates TestRail API Client instance.
     * Configurable with default classpath properties file location.
     *
     * @return TestRailClient – client instance
     */
    public static TestRailClient create() {
        return new TestRailClientPropertyConfigurator().configure().create();
    }

    /**
     * Use the following API methods to upload, retrieve and delete attachments.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/attachments">Attachments</a>
     * @return AttachmentsService – attachments service API wrapper
     */
    public AttachmentsService attachments() {
        return instantiateService(AttachmentsService.class);
    }

    /**
     * Use the following API methods to retrieve data about test cases-like title,
     * case fields, and history-and to create, modify, or delete test cases.<br><br>
     * To correctly serialize and deserialize custom fields in test cases,
     * test cases must be aware of all existing types of custom fields.
     * This custom fields values will be updated on each request.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases">Cases</a>
     * @return CasesService – cases service API wrapper
     */
    public CasesService cases() {
        return ServiceFactory.createService(CasesService.class, configuration, false);
    }

    /**
     * Use the following API methods to retrieve data about test cases-like title,
     * case fields, and history-and to create, modify, or delete test cases.<br><br>
     * To correctly serialize and deserialize custom fields in test cases,
     * test cases must be aware of all existing types of custom fields.
     * This custom fields values will be updated on each request. Since the request
     * for information about the types of custom fields is an additional call to the web service,
     * the system provides for caching this operation.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/cases">Cases</a>
     * @param cached if flag true, then internal case fields will be cached
     * @return CasesService – cases service API wrapper
     */
    public CasesService cases(boolean cached) {
        return ServiceFactory.createService(CasesService.class, configuration, cached);
    }

    /**
     * Use the following API methods to request details about custom fields for test cases.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/case-fields">Case Fields</a>
     * @return CaseFieldsService – case fields service API wrapper
     */
    public CaseFieldsService caseFields() {
        return instantiateService(CaseFieldsService.class);
    }

    /**
     * Use the following API methods to request details about case type.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/case-types">Case Types</a>
     * @return CaseTypesService – case types service API wrapper
     */
    public CaseTypesService caseTypes() {
        return instantiateService(CaseTypesService.class);
    }

    /**
     * Use the following API methods to request details about test plan configurations
     * and to create or modify configurations used to generate test plans.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/configurations">Configurations</a>
     * @return ConfigurationsService – configurations service API wrapper
     */
    public ConfigurationsService configurations() {
        return instantiateService(ConfigurationsService.class);
    }

    /**
     * Use the following API methods to request details about groups.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/groups">Groups</a>
     * @return GroupsService – groups service API wrapper
     */
    public GroupsService groups() {
        return instantiateService(GroupsService.class);
    }

    /**
     * Use the following API methods to request details about test plans and to create or modify test plans.
     * In TestRail, test plans allow you to group multiple test runs together
     * and automatically generate test runs for various browser, OS,
     * or other configurations you set without having to add each test run individually.
     *
     * @return PlansService – plans service API wrapper
     */
    public PlansService plans() {
        return instantiateService(PlansService.class);
    }

    /**
     * Use the following API methods to request details about the priorities field on test cases.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/priorities">Priorities</a>
     * @return PrioritiesService – priorities service API wrapper
     */
    public PrioritiesService priorities() {
        return instantiateService(PrioritiesService.class);
    }

    /**
     * Use the following API methods to request details about projects and to create or modify projects.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/projects">Projects</a>
     * @return ProjectsService – projects service API wrapper
     */
    public ProjectsService projects() {
        return instantiateService(ProjectsService.class);
    }

    /**
     * Use the following API methods to request details about test results and to add new test results.<br><br>
     * To correctly serialize and deserialize custom fields in results,
     * results must be aware of all existing types of custom fields.
     * This custom fields values will be updated on each request.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/results">Results</a>
     * @return ResultsService – results service API wrapper
     */
    public ResultsService results() {
        return ServiceFactory.createService(ResultsService.class, configuration, false);
    }

    /**
     * Use the following API methods to request details about test results and to add new test results.<br><br>
     * To correctly serialize and deserialize custom fields in results,
     * results must be aware of all existing types of custom fields.
     * This custom fields values will be updated on each request. Since the request
     * for information about the types of custom fields is an additional call to the web service,
     * the system provides for caching this operation.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/results">Results</a>
     * @param cached if flag true, then internal result fields will be cached
     * @return ResultsService – results service API wrapper
     */
    public ResultsService results(boolean cached) {
        return ServiceFactory.createService(ResultsService.class, configuration, cached);
    }

    /**
     * Use the following API methods to request details about custom fields for test results.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/result-fields">Result Fields</a>
     * @return ResultsService – result fields service API wrapper
     */
    public ResultFieldsService resultFields() {
        return instantiateService(ResultFieldsService.class);
    }

    /**
     * Use the following API methods to request details about roles.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/roles">Roles</a>
     * @return RolesService – roles service API wrapper
     */
    public RolesService roles() {
        return instantiateService(RolesService.class);
    }

    /**
     * Use the following API methods to request details about test runs and to create or modify test runs.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/runs">Runs</a>
     * @return RunsService – runs service API wrapper
     */
    public RunsService runs() {
        return instantiateService(RunsService.class);
    }

    /**
     * Use the following API methods to request details about sections and to create or modify sections.
     * Sections are used to group and organize test cases in test suites.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/sections">Sections</a>
     * @return SectionsService – sections service API wrapper
     */
    public SectionsService sections() {
        return instantiateService(SectionsService.class);
    }

    /**
     * Use the following API methods to request details about test statuses.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/statuses">Statuses</a>
     * @return StatusesService – statuses service API wrapper
     */
    public StatusesService statuses() {
        return instantiateService(StatusesService.class);
    }

    /**
     * Use the following API methods to request details about test suites and to create or modify test suites.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/suites">Suites</a>
     * @return SuitesService – suites service API wrapper
     */
    public SuitesService suites() {
        return instantiateService(SuitesService.class);
    }

    /**
     * Use the following API methods to request details about
     * tests (individual instances of test cases added to specific test runs or test plans).<br><br>
     * To correctly serialize and deserialize custom fields in tests,
     * tests must be aware of all existing types of custom fields.
     * This custom fields values will be updated on each request.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/tests">Tests</a>
     * @return TestsService – tests service API wrapper
     */
    public TestsService tests() {
        return ServiceFactory.createService(TestsService.class, configuration, false);
    }

    /**
     * Use the following API methods to request details about
     * tests (individual instances of test cases added to specific test runs or test plans).<br><br>
     * To correctly serialize and deserialize custom fields in tests,
     * tests must be aware of all existing types of custom fields.
     * This custom fields values will be updated on each request. Since the request
     * for information about the types of custom fields is an additional call to the web service,
     * the system provides for caching this operation.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/tests">Tests</a>
     * @param cached if flag true, then internal case fields will be cached
     * @return TestsService – tests service API wrapper
     */
    public TestsService tests(boolean cached) {
        return ServiceFactory.createService(TestsService.class, configuration, cached);
    }

    /**
     * Use the following API methods to request details about users.
     * Any user can retrieve his/her own account information.
     * Retrieving information for any other use requires administrator access.
     *
     * @see <a href="https://www.gurock.com/testrail/docs/api/reference/users">Users</a>
     * @return UsersService – users service API wrapper
     */
    public UsersService users() {
        return instantiateService(UsersService.class);
    }

    private <T> T instantiateService(Class<? extends BasicService> serviceClass) {
        return ServiceFactory.createService(serviceClass, configuration);
    }

    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class TestRailClientManualConfigurator {
        private String instanceURI;
        private String username;

        @SneakyThrows
        public TestRailClientFactory password(String password) {
            return new TestRailClientFactory(TestRailClientConfiguration.create(
                    URIUtils.normalizeSyntax(URI.create(instanceURI)),
                    BasicAuthentication.credentials(username, password)
            ), null);
        }

        @SneakyThrows
        public TestRailClientFactory apiKey(String apiKey) {
            return new TestRailClientFactory(TestRailClientConfiguration.create(
                    URIUtils.normalizeSyntax(URI.create(instanceURI)),
                    APIKeyAuthentication.credentials(username, apiKey)
            ), null);
        }
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    private static class TestRailClientPropertyConfigurator {
        public TestRailClientFactory configure(TestRailConfigurationProperties configurationProperties) {
            return createClientFactory(configurationProperties);
        }

        public TestRailClientFactory configure(Class<? extends TestRailConfigurationProperties> configurationPropertiesClass) {
            return createClientFactory(ConfigFactory.create(configurationPropertiesClass));
        }

        public TestRailClientFactory configure() {
            return configure(TestRailConfigurationProperties.class);
        }

        private Authentication chooseAuthentication(TestRailConfigurationProperties configurationProperties) {
            if (StringUtils.isNotEmpty(configurationProperties.getAPIKey())) {
                return APIKeyAuthentication.credentials(configurationProperties.getUsername(), configurationProperties.getAPIKey());
            } else if (StringUtils.isNotEmpty(configurationProperties.getPassword())) {
                return BasicAuthentication.credentials(configurationProperties.getUsername(), configurationProperties.getPassword());
            }

            throw new IllegalArgumentException("Authentication password or API key not provided");
        }

        private TestRailClientFactory createClientFactory(TestRailConfigurationProperties configurationProperties) {
            return new TestRailClientFactory(TestRailClientConfiguration.create(
                    configurationProperties.getInstanceURL(),
                    chooseAuthentication(configurationProperties)
            ), configurationProperties);
        }
    }

    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class TestRailClientFactory {
        private TestRailClientConfiguration configuration;
        private TestRailConfigurationProperties configurationProperties;

        public TestRailClient create() {
            return new TestRailClient(configuration, configurationProperties);
        }
    }
}
