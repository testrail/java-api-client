package com.tronstone.client.testrail.processing;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TestRailException extends RuntimeException {
    @JsonCreator
    public TestRailException(@JacksonInject int errorCode, @JsonProperty("error") String message) {
        super("Response code: " + errorCode + " - " + message);
    }
}
