package com.tronstone.client.testrail.processing;

import com.fasterxml.jackson.databind.InjectableValues;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Cleanup;
import net.sf.cglib.core.ReflectUtils;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Response;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Objects;

import static java.net.HttpURLConnection.HTTP_OK;

public class ResponseErrorHandler implements MethodInterceptor {
    @Override
    public Object intercept(Object requestObject, Method requestMethod, Object[] methodArguments, MethodProxy proxy) throws Throwable {
        if (requestMethod.getName().equals("execute")) {
            Response response = (Response) proxy.invokeSuper(requestObject, methodArguments);
            HttpResponse httpResponse = response.returnResponse();
            int statusCode = httpResponse.getStatusLine().getStatusCode();

            if (statusCode != HTTP_OK) {
                @Cleanup InputStream errorStream = httpResponse.getEntity().getContent();

                if (Objects.isNull(errorStream)) {
                    throw new TestRailException(statusCode, "Server didn't' send any error message");
                }

                throw (TestRailException) new ObjectMapper().reader(new InjectableValues.Std().addValue(int.class, statusCode))
                        .forType(TestRailException.class)
                        .readValue(new BufferedInputStream(errorStream));
            }

            return ReflectUtils.newInstance(Response.class, new Class[] {HttpResponse.class}, new Object[] {httpResponse});
        }

        return proxy.invokeSuper(requestObject, methodArguments);
    }
}
