# TestRail Java API Client

[![Maven Central](https://img.shields.io/maven-central/v/com.tronstone/testrail-java-client)](https://search.maven.org/artifact/com.tronstone/testrail-java-client)
[![Wiki Documentation](https://img.shields.io/badge/documentation-wiki-blue)](https://gitlab.com/testrail/java-api-client/-/wikis/home)
[![Java](https://img.shields.io/badge/java-1.8-red)]()
[![GitLab](https://img.shields.io/gitlab/license/38379023)](https://gitlab.com/testrail/java-api-client/-/blob/master/LICENSE)
[![Free](https://img.shields.io/badge/free-open--source-lightgray)]()<br>
[![TestRail](https://img.shields.io/badge/TestRail-v.7.5-00bdd9)](https://blog.gurock.com/tag/testrail-release/)
[![TestRail](https://img.shields.io/badge/TestRail%20API-v.2-00bdd9)](http://docs.gurock.com/testrail-api2/start)

Here is a **Java** client library, which is a convenient interface for working with the [TestRail API](http://docs.gurock.com/testrail-api2/start).
The library provides a wrapper over REST API web services for quick integration of **TestRail** into your applications.
Wrapper services provide the necessary methods for manipulating test cases, test runs, sections, and more.

## How to start?
### For Maven users:
Add these lines to file `pom.xml`:
```xml
<dependency>
    <groupId>com.tronstone</groupId>
    <artifactId>testrail-java-client</artifactId>
    <version>${latest-version}</version>
</dependency>
```
### For Gradle users:
Add these lines to file `build.gradle`:
```groovy
dependencies {
    implementation 'com.tronstone:testrail-java-client:${latest-version}'
}
```

## Example Usage
> Look [Wiki](https://gitlab.com/testrail/java-api-client/-/wikis/home) for more examples.
### Create a TestRail API Client instance with manual configuration
```java
public class InstanceCreationExample {
    public static void main(String[] args) {
        TestRailClient client = TestRailClient.configure("https://example.testrail.io/", "username").password("password").create();
    }
}
```

### Get data from test case
```java
public class GetTestCase {
    public static void main(String[] args) {
        Case testCase = client.cases().getCase(1234);

        String title = testCase.getTitle();
        DateTime updatedOn = testCase.getUpdatedOn();
        Period estimate = testCase.getEstimate();
        List<Step> steps = testCase.getCustomField("steps_field", CaseFieldType.STEPS);
    }
}
```

### Get several test cases with priority and type request filters
```java
public class GetTestCases {
    public static void main(String[] args) {
        ServicePaginator<Case> casesPaginator = client.cases().getCases(1, CaseParametersProvider.prepare().priorityID(1).typeID(2));

        for (List<Case> cases : casesPaginator) {
            cases.forEach(testCase -> System.out.println(testCase.getTitle()));
        }

        List<Case> cases = casesPagination.getAll();
    }
}
```

### Add new test case
```java
public class AddTestCase {
    public static void main(String[] args) {
        client.cases().addCase(1234, Case.construct("New Test Case")
                .refs("TC-1", "TC-2")
                .estimate("1h")
                .addCustomField("steps_field", CaseFieldType.STEPS, Step.createSteps(
                        Step.construct().content("Example Step Description").expected("Example Expected").build(),
                        Step.construct().additionalInfo("Example Additional Info").build()
                )).build()
        );
    }
}
```

### Delete one or several test cases
```java
public class DeleteTestCases {
    public static void main(String[] args) {
        client.cases().deleteCase(1233);
        client.cases().deleteCases(1, 1, false, 1234, 1235);
    }
}
```

## Contributing
Any contributions to **TestRail Java API Client** are both welcomed and appreciated.
Feel free to clone, build, run tests and contribute pull requests.

### How can you help?
It doesn't necessarily have to be code. There are many ways you can help **TestRail Java API Client**:
- Answer questions in `chats/forums/stackoverflow/etc`
- Write in your `blog/twitter/medium` about **TestRail Java API Client** or it's usages in specific situations
- Discuss ideas in [our brainstorms](https://gitlab.com/testrail/java-api-client/-/issues?label_name%5B%5D=brainstorm%3A%3Adiscussions)
- Give your feedback on [feature requests](https://gitlab.com/testrail/java-api-client/-/issues?label_name%5B%5D=feature%3A%3Adiscussions)
- Help to reproduce an issue that we [cannot reproduce](https://gitlab.com/testrail/java-api-client/-/issues/?label_name%5B%5D=can%27t%20reproduce%3A%3Aissues)
- Make code review for any of [merge requests](https://gitlab.com/testrail/java-api-client/-/merge_requests)
- And finally, code!
    1. Find any [issue with label "help wanted"](https://gitlab.com/testrail/java-api-client/-/issues/?label_name%5B%5D=help%20wanted%3A%3Adiscussions)
    2. Add a comment like "I am working on it" (provide some details what's your plan)
    3. Send a merge request

If you are a **brave code writer**, we need your help by first with:
- Writing unit tests (especially)
- Documentation writing (javadocs)
- Finding bugs in an existing services implementation and documentation

## License
**TestRail Java API Client** is open-source project, and distributed under the [MIT](http://choosealicense.com/licenses/mit/) license.